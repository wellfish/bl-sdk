using Bl_sdk.Connector;
using Bl_sdk.Service;

namespace Bl_sdk.Tests;

public sealed class BlTests : IAsyncLifetime
{
    private string _token;
    private Guid _guid;

    public async Task InitializeAsync()
    {
        var response = await BlAuthService.GetAuth(Environment.GetEnvironmentVariable("CLIENT_ID"),
            Environment.GetEnvironmentVariable("CLIENT_SECRET"));
        _token = response.AccessToken;
        _guid = Guid.Parse(Environment.GetEnvironmentVariable("USER_KEY"));
    }

    public Task DisposeAsync() => Task.CompletedTask;  // Add cleanup logic if needed

    [Fact]
    public void EnvironmentVariablesAreSet()
    {
        Assert.False(string.IsNullOrEmpty(Environment.GetEnvironmentVariable("CLIENT_ID")), "CLIENT_ID not set.");
        Assert.False(string.IsNullOrEmpty(Environment.GetEnvironmentVariable("CLIENT_SECRET")), "CLIENT_SECRET not set.");
        Assert.False(string.IsNullOrEmpty(Environment.GetEnvironmentVariable("USER_KEY")), "USER_KEY not set.");
    }

    [Fact]
    public void GetAuthToken_ReturnsValidToken()
    {
        Assert.NotNull(_token);
    }

    [Fact]
    public async Task GetDetails_ReturnsData()
    {
        var connector = new BlConnector(_token, _guid);
        var details = await BlDetailsService.GetDetails(connector);
        Assert.NotNull(details);
    }

    [Fact]
    public async Task GetSieData_ReturnsValidSie()
    {
        var connector = new BlConnector(_token, _guid);
        var start = new DateTime(2023, 1, 1);
        var end = new DateTime(2023, 1, 31);
        var details = await BlSieService.GetSieData(connector, start, end); 
        
        Assert.NotNull(details);
    }
}