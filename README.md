# Unoffical Björn Lundén .NET SDK
This is an unofficial .NET SDK for Björn Lundén's [API](https://developer.bjornlunden.se/api-documentation/).

Maintained by [Wellfish](https://wellfish.se).

## State
This is a **work in progress** and not all endpoints are implemented. If you need a specific endpoint implemented please open an issue.

All models are complete and are generated from the [OpenAPI](https://developer.bjornlunden.se/api-documentation/) spec.

### Latest version
[NuGet](https://gitlab.com/wellfish/bl-sdk/-/packages)

## Installation
```bash
dotnet nuget add source "https://gitlab.com/api/v4/projects/44016273/packages/nuget/index.json" --name gitlab
```


## Usage
### Auth
```csharp
public async Task<AuthResponse> Auth()
{
    var authClient = new BlApiAuthClient();
    var authResponse = await authClient.GetToken(_bjornLundenClientId, _bjornLundenClientSecret);
    return authResponse;
}
```

### Fetch Accounts
```csharp
public async Task<Details> GetCompanyInformation(string userKey)
{
    var auth = await Auth();
    var connector = new BlConnector(auth.AccessToken, userKey);
    
    var companyInformation = await BlDetailsService.GetDetails(connector);
    return companyInformation;
}
```

### For more examples see the service directory




## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

### Releases
To make a new release just push a tag to the repo and a new package will be published to the gitlab nuget feed.

## License
[MIT](https://choosealicense.com/licenses/mit/)

