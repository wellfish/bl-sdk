namespace Bl_sdk.Connector;

public sealed class BlConnector
{
    public readonly string AccessToken;
    public readonly Guid UserKey;

    public BlConnector(string accessToken, Guid userKey)
    {
        AccessToken = accessToken;
        UserKey = userKey;
    }
}