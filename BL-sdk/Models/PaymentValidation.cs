using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class PaymentValidation
{
    /// <summary>
    ///     Gets or Sets InvoiceEntityId
    /// </summary>
    [DataMember(Name = "invoiceEntityId", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceEntityId")]
    public long? InvoiceEntityId { get; set; }

    /// <summary>
    ///     Gets or Sets Message
    /// </summary>
    [DataMember(Name = "message", EmitDefaultValue = false)]
    [JsonPropertyName("message")]
    public string Message { get; set; }

    /// <summary>
    ///     Gets or Sets PaymentEntityId
    /// </summary>
    [DataMember(Name = "paymentEntityId", EmitDefaultValue = false)]
    [JsonPropertyName("paymentEntityId")]
    public long? PaymentEntityId { get; set; }

    /// <summary>
    ///     Gets or Sets SupplierPayment
    /// </summary>
    [DataMember(Name = "supplierPayment", EmitDefaultValue = false)]
    [JsonPropertyName("supplierPayment")]
    public SupplierPayment SupplierPayment { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class PaymentValidation {\n");
        sb.Append("  InvoiceEntityId: ").Append(InvoiceEntityId).Append("\n");
        sb.Append("  Message: ").Append(Message).Append("\n");
        sb.Append("  PaymentEntityId: ").Append(PaymentEntityId).Append("\n");
        sb.Append("  SupplierPayment: ").Append(SupplierPayment).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}