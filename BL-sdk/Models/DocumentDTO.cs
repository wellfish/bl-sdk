using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class DocumentDTO
{
    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public long? Id { get; set; }

    /// <summary>
    ///     Gets or Sets Name
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     Gets or Sets Type
    /// </summary>
    [DataMember(Name = "type", EmitDefaultValue = false)]
    [JsonPropertyName("type")]
    public string Type { get; set; }

    /// <summary>
    ///     Gets or Sets TextId
    /// </summary>
    [DataMember(Name = "textId", EmitDefaultValue = false)]
    [JsonPropertyName("textId")]
    public string TextId { get; set; }

    /// <summary>
    ///     Gets or Sets NumberId
    /// </summary>
    [DataMember(Name = "numberId", EmitDefaultValue = false)]
    [JsonPropertyName("numberId")]
    public int? NumberId { get; set; }

    /// <summary>
    ///     Gets or Sets OwningType
    /// </summary>
    [DataMember(Name = "owningType", EmitDefaultValue = false)]
    [JsonPropertyName("owningType")]
    public string OwningType { get; set; }

    /// <summary>
    ///     Gets or Sets Created
    /// </summary>
    [DataMember(Name = "created", EmitDefaultValue = false)]
    [JsonPropertyName("created")]
    public DateTime? Created { get; set; }

    /// <summary>
    ///     Gets or Sets Reference
    /// </summary>
    [DataMember(Name = "reference", EmitDefaultValue = false)]
    [JsonPropertyName("reference")]
    public string Reference { get; set; }

    /// <summary>
    ///     Gets or Sets Comment
    /// </summary>
    [DataMember(Name = "comment", EmitDefaultValue = false)]
    [JsonPropertyName("comment")]
    public string Comment { get; set; }

    /// <summary>
    ///     Gets or Sets FileExt
    /// </summary>
    [DataMember(Name = "fileExt", EmitDefaultValue = false)]
    [JsonPropertyName("fileExt")]
    public string FileExt { get; set; }

    /// <summary>
    ///     Gets or Sets ContentType
    /// </summary>
    [DataMember(Name = "contentType", EmitDefaultValue = false)]
    [JsonPropertyName("contentType")]
    public string ContentType { get; set; }

    /// <summary>
    ///     Gets or Sets Properties
    /// </summary>
    [DataMember(Name = "properties", EmitDefaultValue = false)]
    [JsonPropertyName("properties")]
    public List<DocumentPropertyDTO> Properties { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class DocumentDTO {\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  Type: ").Append(Type).Append("\n");
        sb.Append("  TextId: ").Append(TextId).Append("\n");
        sb.Append("  NumberId: ").Append(NumberId).Append("\n");
        sb.Append("  OwningType: ").Append(OwningType).Append("\n");
        sb.Append("  Created: ").Append(Created).Append("\n");
        sb.Append("  Reference: ").Append(Reference).Append("\n");
        sb.Append("  Comment: ").Append(Comment).Append("\n");
        sb.Append("  FileExt: ").Append(FileExt).Append("\n");
        sb.Append("  ContentType: ").Append(ContentType).Append("\n");
        sb.Append("  Properties: ").Append(Properties).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}