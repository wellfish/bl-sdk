using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class AspspDetailsResult
{
    /// <summary>
    ///     Gets or Sets City
    /// </summary>
    [DataMember(Name = "city", EmitDefaultValue = false)]
    [JsonPropertyName("city")]
    public string City { get; set; }

    /// <summary>
    ///     Gets or Sets Country
    /// </summary>
    [DataMember(Name = "country", EmitDefaultValue = false)]
    [JsonPropertyName("country")]
    public string Country { get; set; }

    /// <summary>
    ///     Gets or Sets PostalCode
    /// </summary>
    [DataMember(Name = "postalCode", EmitDefaultValue = false)]
    [JsonPropertyName("postalCode")]
    public string PostalCode { get; set; }

    /// <summary>
    ///     Gets or Sets StreetAddress
    /// </summary>
    [DataMember(Name = "streetAddress", EmitDefaultValue = false)]
    [JsonPropertyName("streetAddress")]
    public string StreetAddress { get; set; }

    /// <summary>
    ///     Gets or Sets ConpanyNumber
    /// </summary>
    [DataMember(Name = "conpanyNumber", EmitDefaultValue = false)]
    [JsonPropertyName("conpanyNumber")]
    public string ConpanyNumber { get; set; }

    /// <summary>
    ///     Gets or Sets Phone
    /// </summary>
    [DataMember(Name = "phone", EmitDefaultValue = false)]
    [JsonPropertyName("phone")]
    public string Phone { get; set; }

    /// <summary>
    ///     Gets or Sets WebsiteUrl
    /// </summary>
    [DataMember(Name = "websiteUrl", EmitDefaultValue = false)]
    [JsonPropertyName("websiteUrl")]
    public string WebsiteUrl { get; set; }

    /// <summary>
    ///     Gets or Sets GlobalPaymentProducts
    /// </summary>
    [DataMember(Name = "globalPaymentProducts", EmitDefaultValue = false)]
    [JsonPropertyName("globalPaymentProducts")]
    public List<string> GlobalPaymentProducts { get; set; }

    /// <summary>
    ///     Gets or Sets SupportedAuthorizationMethods
    /// </summary>
    [DataMember(Name = "supportedAuthorizationMethods", EmitDefaultValue = false)]
    [JsonPropertyName("supportedAuthorizationMethods")]
    public List<AuthorizationMethod> SupportedAuthorizationMethods { get; set; }

    /// <summary>
    ///     Gets or Sets AffiliatedAspsps
    /// </summary>
    [DataMember(Name = "affiliatedAspsps", EmitDefaultValue = false)]
    [JsonPropertyName("affiliatedAspsps")]
    public List<AffiliatedAspsp> AffiliatedAspsps { get; set; }

    /// <summary>
    ///     Gets or Sets BicFi
    /// </summary>
    [DataMember(Name = "bicFi", EmitDefaultValue = false)]
    [JsonPropertyName("bicFi")]
    public string BicFi { get; set; }

    /// <summary>
    ///     Gets or Sets Name
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     Gets or Sets LogoUrl
    /// </summary>
    [DataMember(Name = "logoUrl", EmitDefaultValue = false)]
    [JsonPropertyName("logoUrl")]
    public string LogoUrl { get; set; }

    /// <summary>
    ///     Gets or Sets ExtendedData
    /// </summary>
    [DataMember(Name = "extendedData", EmitDefaultValue = false)]
    [JsonPropertyName("extendedData")]
    public ClientAdaptedBankInfo ExtendedData { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class AspspDetailsResult {\n");
        sb.Append("  City: ").Append(City).Append("\n");
        sb.Append("  Country: ").Append(Country).Append("\n");
        sb.Append("  PostalCode: ").Append(PostalCode).Append("\n");
        sb.Append("  StreetAddress: ").Append(StreetAddress).Append("\n");
        sb.Append("  ConpanyNumber: ").Append(ConpanyNumber).Append("\n");
        sb.Append("  Phone: ").Append(Phone).Append("\n");
        sb.Append("  WebsiteUrl: ").Append(WebsiteUrl).Append("\n");
        sb.Append("  GlobalPaymentProducts: ").Append(GlobalPaymentProducts).Append("\n");
        sb.Append("  SupportedAuthorizationMethods: ").Append(SupportedAuthorizationMethods).Append("\n");
        sb.Append("  AffiliatedAspsps: ").Append(AffiliatedAspsps).Append("\n");
        sb.Append("  BicFi: ").Append(BicFi).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  LogoUrl: ").Append(LogoUrl).Append("\n");
        sb.Append("  ExtendedData: ").Append(ExtendedData).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}