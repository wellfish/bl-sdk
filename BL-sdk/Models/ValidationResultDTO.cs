using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class ValidationResultDTO
{
    /// <summary>
    ///     Gets or Sets Document
    /// </summary>
    [DataMember(Name = "document", EmitDefaultValue = false)]
    [JsonPropertyName("document")]
    public DocumentDTO Document { get; set; }

    /// <summary>
    ///     Gets or Sets Logs
    /// </summary>
    [DataMember(Name = "logs", EmitDefaultValue = false)]
    [JsonPropertyName("logs")]
    public List<SieLogDTO> Logs { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class ValidationResultDTO {\n");
        sb.Append("  Document: ").Append(Document).Append("\n");
        sb.Append("  Logs: ").Append(Logs).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}