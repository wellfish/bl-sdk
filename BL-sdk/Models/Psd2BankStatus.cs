using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Psd2BankStatus
{
    /// <summary>
    ///     Gets or Sets AccountBalance
    /// </summary>
    [DataMember(Name = "accountBalance", EmitDefaultValue = false)]
    [JsonPropertyName("accountBalance")]
    public decimal? AccountBalance { get; set; }

    /// <summary>
    ///     Gets or Sets AccountBalanceDate
    /// </summary>
    [DataMember(Name = "accountBalanceDate", EmitDefaultValue = false)]
    [JsonPropertyName("accountBalanceDate")]
    public DateTime? AccountBalanceDate { get; set; }

    /// <summary>
    ///     Gets or Sets Affiliate
    /// </summary>
    [DataMember(Name = "affiliate", EmitDefaultValue = false)]
    [JsonPropertyName("affiliate")]
    public string Affiliate { get; set; }

    /// <summary>
    ///     Gets or Sets AgreementExpirationDate
    /// </summary>
    [DataMember(Name = "agreementExpirationDate", EmitDefaultValue = false)]
    [JsonPropertyName("agreementExpirationDate")]
    public DateTime? AgreementExpirationDate { get; set; }

    /// <summary>
    ///     Gets or Sets ErrorCode
    /// </summary>
    [DataMember(Name = "errorCode", EmitDefaultValue = false)]
    [JsonPropertyName("errorCode")]
    public string ErrorCode { get; set; }

    /// <summary>
    ///     Gets or Sets ErrorText
    /// </summary>
    [DataMember(Name = "errorText", EmitDefaultValue = false)]
    [JsonPropertyName("errorText")]
    public string ErrorText { get; set; }

    /// <summary>
    ///     Gets or Sets Iban
    /// </summary>
    [DataMember(Name = "iban", EmitDefaultValue = false)]
    [JsonPropertyName("iban")]
    public string Iban { get; set; }

    /// <summary>
    ///     Gets or Sets Context
    /// </summary>
    [DataMember(Name = "context", EmitDefaultValue = false)]
    [JsonPropertyName("context")]
    public string Context { get; set; }

    /// <summary>
    ///     Gets or Sets Active
    /// </summary>
    [DataMember(Name = "active", EmitDefaultValue = false)]
    [JsonPropertyName("active")]
    public bool? Active { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Psd2BankStatus {\n");
        sb.Append("  AccountBalance: ").Append(AccountBalance).Append("\n");
        sb.Append("  AccountBalanceDate: ").Append(AccountBalanceDate).Append("\n");
        sb.Append("  Affiliate: ").Append(Affiliate).Append("\n");
        sb.Append("  AgreementExpirationDate: ").Append(AgreementExpirationDate).Append("\n");
        sb.Append("  ErrorCode: ").Append(ErrorCode).Append("\n");
        sb.Append("  ErrorText: ").Append(ErrorText).Append("\n");
        sb.Append("  Iban: ").Append(Iban).Append("\n");
        sb.Append("  Context: ").Append(Context).Append("\n");
        sb.Append("  Active: ").Append(Active).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}