using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class TransactionDetailsResult
{
    /// <summary>
    ///     Gets or Sets TransactionsDetails
    /// </summary>
    [DataMember(Name = "transactionsDetails", EmitDefaultValue = false)]
    [JsonPropertyName("transactionsDetails")]
    public TransactionsDetails TransactionsDetails { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class TransactionDetailsResult {\n");
        sb.Append("  TransactionsDetails: ").Append(TransactionsDetails).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}