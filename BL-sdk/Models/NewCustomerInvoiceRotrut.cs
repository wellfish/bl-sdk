using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
///     New customer invoice rut/rot
/// </summary>
[DataContract]
public sealed class NewCustomerInvoiceRotrut
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "type", EmitDefaultValue = false)]
    [JsonPropertyName("type")]
    public int? Type { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "invoiceNumber", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceNumber")]
    public long? InvoiceNumber { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "orderNumber", EmitDefaultValue = false)]
    [JsonPropertyName("orderNumber")]
    public long? OrderNumber { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "customerId", EmitDefaultValue = false)]
    [JsonPropertyName("customerId")]
    public string CustomerId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "customerName", EmitDefaultValue = false)]
    [JsonPropertyName("customerName")]
    public string CustomerName { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "customerOrgNumber", EmitDefaultValue = false)]
    [JsonPropertyName("customerOrgNumber")]
    public string CustomerOrgNumber { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "socialSecurityNumber1", EmitDefaultValue = false)]
    [JsonPropertyName("socialSecurityNumber1")]
    public string SocialSecurityNumber1 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "socialSecurityNumber2", EmitDefaultValue = false)]
    [JsonPropertyName("socialSecurityNumber2")]
    public string SocialSecurityNumber2 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "propertyId", EmitDefaultValue = false)]
    [JsonPropertyName("propertyId")]
    public string PropertyId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "apartmentId", EmitDefaultValue = false)]
    [JsonPropertyName("apartmentId")]
    public string ApartmentId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "address", EmitDefaultValue = false)]
    [JsonPropertyName("address")]
    public string Address { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "zip", EmitDefaultValue = false)]
    [JsonPropertyName("zip")]
    public string Zip { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "city", EmitDefaultValue = false)]
    [JsonPropertyName("city")]
    public string City { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "amountPerson1", EmitDefaultValue = false)]
    [JsonPropertyName("amountPerson1")]
    public double? AmountPerson1 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "amountPerson2", EmitDefaultValue = false)]
    [JsonPropertyName("amountPerson2")]
    public double? AmountPerson2 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "manualCount", EmitDefaultValue = false)]
    [JsonPropertyName("manualCount")]
    public bool? ManualCount { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "forward", EmitDefaultValue = false)]
    [JsonPropertyName("forward")]
    public bool? Forward { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "paymentDate", EmitDefaultValue = false)]
    [JsonPropertyName("paymentDate")]
    public DateTime? PaymentDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "createDate", EmitDefaultValue = false)]
    [JsonPropertyName("createDate")]
    public DateTime? CreateDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "forwardDate", EmitDefaultValue = false)]
    [JsonPropertyName("forwardDate")]
    public DateTime? ForwardDate { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class NewCustomerInvoiceRotrut {\n");
        sb.Append("  Type: ").Append(Type).Append("\n");
        sb.Append("  InvoiceNumber: ").Append(InvoiceNumber).Append("\n");
        sb.Append("  OrderNumber: ").Append(OrderNumber).Append("\n");
        sb.Append("  CustomerId: ").Append(CustomerId).Append("\n");
        sb.Append("  CustomerName: ").Append(CustomerName).Append("\n");
        sb.Append("  CustomerOrgNumber: ").Append(CustomerOrgNumber).Append("\n");
        sb.Append("  SocialSecurityNumber1: ").Append(SocialSecurityNumber1).Append("\n");
        sb.Append("  SocialSecurityNumber2: ").Append(SocialSecurityNumber2).Append("\n");
        sb.Append("  PropertyId: ").Append(PropertyId).Append("\n");
        sb.Append("  ApartmentId: ").Append(ApartmentId).Append("\n");
        sb.Append("  Address: ").Append(Address).Append("\n");
        sb.Append("  Zip: ").Append(Zip).Append("\n");
        sb.Append("  City: ").Append(City).Append("\n");
        sb.Append("  AmountPerson1: ").Append(AmountPerson1).Append("\n");
        sb.Append("  AmountPerson2: ").Append(AmountPerson2).Append("\n");
        sb.Append("  ManualCount: ").Append(ManualCount).Append("\n");
        sb.Append("  Forward: ").Append(Forward).Append("\n");
        sb.Append("  PaymentDate: ").Append(PaymentDate).Append("\n");
        sb.Append("  CreateDate: ").Append(CreateDate).Append("\n");
        sb.Append("  ForwardDate: ").Append(ForwardDate).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}