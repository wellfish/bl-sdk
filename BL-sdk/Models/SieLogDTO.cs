using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class SieLogDTO
{
    /// <summary>
    ///     Gets or Sets Level
    /// </summary>
    [DataMember(Name = "level", EmitDefaultValue = false)]
    [JsonPropertyName("level")]
    public string Level { get; set; }

    /// <summary>
    ///     Gets or Sets Message
    /// </summary>
    [DataMember(Name = "message", EmitDefaultValue = false)]
    [JsonPropertyName("message")]
    public string Message { get; set; }

    /// <summary>
    ///     Gets or Sets Tag
    /// </summary>
    [DataMember(Name = "tag", EmitDefaultValue = false)]
    [JsonPropertyName("tag")]
    public string Tag { get; set; }

    /// <summary>
    ///     Gets or Sets Origin
    /// </summary>
    [DataMember(Name = "origin", EmitDefaultValue = false)]
    [JsonPropertyName("origin")]
    public string Origin { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class SieLogDTO {\n");
        sb.Append("  Level: ").Append(Level).Append("\n");
        sb.Append("  Message: ").Append(Message).Append("\n");
        sb.Append("  Tag: ").Append(Tag).Append("\n");
        sb.Append("  Origin: ").Append(Origin).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}