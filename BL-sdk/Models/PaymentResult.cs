using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class PaymentResult
{
    /// <summary>
    ///     Gets or Sets InvalidPayments
    /// </summary>
    [DataMember(Name = "invalidPayments", EmitDefaultValue = false)]
    [JsonPropertyName("invalidPayments")]
    public List<PaymentValidation> InvalidPayments { get; set; }

    /// <summary>
    ///     Gets or Sets AuthenticationResult
    /// </summary>
    [DataMember(Name = "authenticationResult", EmitDefaultValue = false)]
    [JsonPropertyName("authenticationResult")]
    public AuthenticationResult AuthenticationResult { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class PaymentResult {\n");
        sb.Append("  InvalidPayments: ").Append(InvalidPayments).Append("\n");
        sb.Append("  AuthenticationResult: ").Append(AuthenticationResult).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}