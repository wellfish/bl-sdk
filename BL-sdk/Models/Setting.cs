using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Setting
{
    /// <summary>
    ///     Gets or Sets EntityId
    /// </summary>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets Key
    /// </summary>
    [DataMember(Name = "key", EmitDefaultValue = false)]
    [JsonPropertyName("key")]
    public string Key { get; set; }

    /// <summary>
    ///     Gets or Sets TextValue
    /// </summary>
    [DataMember(Name = "textValue", EmitDefaultValue = false)]
    [JsonPropertyName("textValue")]
    public string TextValue { get; set; }

    /// <summary>
    ///     Gets or Sets TimeValue
    /// </summary>
    [DataMember(Name = "timeValue", EmitDefaultValue = false)]
    [JsonPropertyName("timeValue")]
    public DateTime? TimeValue { get; set; }

    /// <summary>
    ///     Gets or Sets TimeValue2
    /// </summary>
    [DataMember(Name = "timeValue2", EmitDefaultValue = false)]
    [JsonPropertyName("timeValue2")]
    public DateTime? TimeValue2 { get; set; }

    /// <summary>
    ///     Gets or Sets DoubleValue
    /// </summary>
    [DataMember(Name = "doubleValue", EmitDefaultValue = false)]
    [JsonPropertyName("doubleValue")]
    public double? DoubleValue { get; set; }

    /// <summary>
    ///     Gets or Sets DoubleValue2
    /// </summary>
    [DataMember(Name = "doubleValue2", EmitDefaultValue = false)]
    [JsonPropertyName("doubleValue2")]
    public double? DoubleValue2 { get; set; }

    /// <summary>
    ///     Gets or Sets LongValue
    /// </summary>
    [DataMember(Name = "longValue", EmitDefaultValue = false)]
    [JsonPropertyName("longValue")]
    public long? LongValue { get; set; }

    /// <summary>
    ///     Gets or Sets LongValue2
    /// </summary>
    [DataMember(Name = "longValue2", EmitDefaultValue = false)]
    [JsonPropertyName("longValue2")]
    public long? LongValue2 { get; set; }

    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    /// <summary>
    ///     Gets or Sets Type
    /// </summary>
    [DataMember(Name = "type", EmitDefaultValue = false)]
    [JsonPropertyName("type")]
    public string Type { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Setting {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Key: ").Append(Key).Append("\n");
        sb.Append("  TextValue: ").Append(TextValue).Append("\n");
        sb.Append("  TimeValue: ").Append(TimeValue).Append("\n");
        sb.Append("  TimeValue2: ").Append(TimeValue2).Append("\n");
        sb.Append("  DoubleValue: ").Append(DoubleValue).Append("\n");
        sb.Append("  DoubleValue2: ").Append(DoubleValue2).Append("\n");
        sb.Append("  LongValue: ").Append(LongValue).Append("\n");
        sb.Append("  LongValue2: ").Append(LongValue2).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Type: ").Append(Type).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}