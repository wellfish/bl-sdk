using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class ServiceScope
{
    /// <summary>
    ///     Gets or Sets ServiceName
    /// </summary>
    [DataMember(Name = "serviceName", EmitDefaultValue = false)]
    [JsonPropertyName("serviceName")]
    public string ServiceName { get; set; }

    /// <summary>
    ///     Gets or Sets AccessTypes
    /// </summary>
    [DataMember(Name = "accessTypes", EmitDefaultValue = false)]
    [JsonPropertyName("accessTypes")]
    public List<string> AccessTypes { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class ServiceScope {\n");
        sb.Append("  ServiceName: ").Append(ServiceName).Append("\n");
        sb.Append("  AccessTypes: ").Append(AccessTypes).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}