using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class UserBindingData
{
    /// <summary>
    ///     Gets or Sets Name
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     Gets or Sets PublicKey
    /// </summary>
    [DataMember(Name = "publicKey", EmitDefaultValue = false)]
    [JsonPropertyName("publicKey")]
    public string PublicKey { get; set; }

    /// <summary>
    ///     Gets or Sets Scopes
    /// </summary>
    [DataMember(Name = "scopes", EmitDefaultValue = false)]
    [JsonPropertyName("scopes")]
    public List<ServiceScope> Scopes { get; set; }

    /// <summary>
    ///     Gets or Sets Email
    /// </summary>
    [DataMember(Name = "email", EmitDefaultValue = false)]
    [JsonPropertyName("email")]
    public string Email { get; set; }

    /// <summary>
    ///     Gets or Sets ExternalCustomerId
    /// </summary>
    [DataMember(Name = "externalCustomerId", EmitDefaultValue = false)]
    [JsonPropertyName("externalCustomerId")]
    public string ExternalCustomerId { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class UserBindingData {\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  PublicKey: ").Append(PublicKey).Append("\n");
        sb.Append("  Scopes: ").Append(Scopes).Append("\n");
        sb.Append("  Email: ").Append(Email).Append("\n");
        sb.Append("  ExternalCustomerId: ").Append(ExternalCustomerId).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}