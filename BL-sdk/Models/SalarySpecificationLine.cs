using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class SalarySpecificationLine
{
    /// <summary>
    ///     Gets or Sets RowNumber
    /// </summary>
    [DataMember(Name = "rowNumber", EmitDefaultValue = false)]
    [JsonPropertyName("rowNumber")]
    public int? RowNumber { get; set; }

    /// <summary>
    ///     Gets or Sets Text
    /// </summary>
    [DataMember(Name = "text", EmitDefaultValue = false)]
    [JsonPropertyName("text")]
    public string Text { get; set; }

    /// <summary>
    ///     Gets or Sets Account
    /// </summary>
    [DataMember(Name = "account", EmitDefaultValue = false)]
    [JsonPropertyName("account")]
    public string Account { get; set; }

    /// <summary>
    ///     Gets or Sets CostCenterId
    /// </summary>
    [DataMember(Name = "costCenterId", EmitDefaultValue = false)]
    [JsonPropertyName("costCenterId")]
    public string CostCenterId { get; set; }

    /// <summary>
    ///     Gets or Sets CostBearerId
    /// </summary>
    [DataMember(Name = "costBearerId", EmitDefaultValue = false)]
    [JsonPropertyName("costBearerId")]
    public string CostBearerId { get; set; }

    /// <summary>
    ///     Gets or Sets ProjectId
    /// </summary>
    [DataMember(Name = "projectId", EmitDefaultValue = false)]
    [JsonPropertyName("projectId")]
    public string ProjectId { get; set; }

    /// <summary>
    ///     Gets or Sets Quantity
    /// </summary>
    [DataMember(Name = "quantity", EmitDefaultValue = false)]
    [JsonPropertyName("quantity")]
    public double? Quantity { get; set; }

    /// <summary>
    ///     Gets or Sets PricePerUnit
    /// </summary>
    [DataMember(Name = "pricePerUnit", EmitDefaultValue = false)]
    [JsonPropertyName("pricePerUnit")]
    public decimal? PricePerUnit { get; set; }

    /// <summary>
    ///     Gets or Sets Amount
    /// </summary>
    [DataMember(Name = "amount", EmitDefaultValue = false)]
    [JsonPropertyName("amount")]
    public decimal? Amount { get; set; }

    /// <summary>
    ///     Gets or Sets FromDate
    /// </summary>
    [DataMember(Name = "fromDate", EmitDefaultValue = false)]
    [JsonPropertyName("fromDate")]
    public DateTime? FromDate { get; set; }

    /// <summary>
    ///     Gets or Sets ToDate
    /// </summary>
    [DataMember(Name = "toDate", EmitDefaultValue = false)]
    [JsonPropertyName("toDate")]
    public DateTime? ToDate { get; set; }

    /// <summary>
    ///     Gets or Sets Comment
    /// </summary>
    [DataMember(Name = "comment", EmitDefaultValue = false)]
    [JsonPropertyName("comment")]
    public string Comment { get; set; }

    /// <summary>
    ///     Gets or Sets Unit
    /// </summary>
    [DataMember(Name = "unit", EmitDefaultValue = false)]
    [JsonPropertyName("unit")]
    public string Unit { get; set; }

    /// <summary>
    ///     Gets or Sets EntityId
    /// </summary>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class SalarySpecificationLine {\n");
        sb.Append("  RowNumber: ").Append(RowNumber).Append("\n");
        sb.Append("  Text: ").Append(Text).Append("\n");
        sb.Append("  Account: ").Append(Account).Append("\n");
        sb.Append("  CostCenterId: ").Append(CostCenterId).Append("\n");
        sb.Append("  CostBearerId: ").Append(CostBearerId).Append("\n");
        sb.Append("  ProjectId: ").Append(ProjectId).Append("\n");
        sb.Append("  Quantity: ").Append(Quantity).Append("\n");
        sb.Append("  PricePerUnit: ").Append(PricePerUnit).Append("\n");
        sb.Append("  Amount: ").Append(Amount).Append("\n");
        sb.Append("  FromDate: ").Append(FromDate).Append("\n");
        sb.Append("  ToDate: ").Append(ToDate).Append("\n");
        sb.Append("  Comment: ").Append(Comment).Append("\n");
        sb.Append("  Unit: ").Append(Unit).Append("\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}