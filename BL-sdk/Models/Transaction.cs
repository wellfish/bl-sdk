using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Transaction
{
    /// <summary>
    ///     Gets or Sets TransactionId
    /// </summary>
    [DataMember(Name = "transactionId", EmitDefaultValue = false)]
    [JsonPropertyName("transactionId")]
    public string TransactionId { get; set; }

    /// <summary>
    ///     Gets or Sets CreditorName
    /// </summary>
    [DataMember(Name = "creditorName", EmitDefaultValue = false)]
    [JsonPropertyName("creditorName")]
    public string CreditorName { get; set; }

    /// <summary>
    ///     Gets or Sets DebtorName
    /// </summary>
    [DataMember(Name = "debtorName", EmitDefaultValue = false)]
    [JsonPropertyName("debtorName")]
    public string DebtorName { get; set; }

    /// <summary>
    ///     Gets or Sets CreditorAccount
    /// </summary>
    [DataMember(Name = "creditorAccount", EmitDefaultValue = false)]
    [JsonPropertyName("creditorAccount")]
    public BankAccount CreditorAccount { get; set; }

    /// <summary>
    ///     Gets or Sets DebtorAccount
    /// </summary>
    [DataMember(Name = "debtorAccount", EmitDefaultValue = false)]
    [JsonPropertyName("debtorAccount")]
    public BankAccount DebtorAccount { get; set; }

    /// <summary>
    ///     Gets or Sets TransactionAmount
    /// </summary>
    [DataMember(Name = "transactionAmount", EmitDefaultValue = false)]
    [JsonPropertyName("transactionAmount")]
    public Amount TransactionAmount { get; set; }

    /// <summary>
    ///     Gets or Sets BookingDate
    /// </summary>
    [DataMember(Name = "bookingDate", EmitDefaultValue = false)]
    [JsonPropertyName("bookingDate")]
    public string BookingDate { get; set; }

    /// <summary>
    ///     Gets or Sets ValueDate
    /// </summary>
    [DataMember(Name = "valueDate", EmitDefaultValue = false)]
    [JsonPropertyName("valueDate")]
    public string ValueDate { get; set; }

    /// <summary>
    ///     Gets or Sets RemittanceInformationUnstructured
    /// </summary>
    [DataMember(Name = "remittanceInformationUnstructured", EmitDefaultValue = false)]
    [JsonPropertyName("remittanceInformationUnstructured")]
    public string RemittanceInformationUnstructured { get; set; }

    /// <summary>
    ///     Gets or Sets RemittanceInformationStructured
    /// </summary>
    [DataMember(Name = "remittanceInformationStructured", EmitDefaultValue = false)]
    [JsonPropertyName("remittanceInformationStructured")]
    public string RemittanceInformationStructured { get; set; }

    /// <summary>
    ///     Gets or Sets TransactionListId
    /// </summary>
    [DataMember(Name = "transactionListId", EmitDefaultValue = false)]
    [JsonPropertyName("transactionListId")]
    public string TransactionListId { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Transaction {\n");
        sb.Append("  TransactionId: ").Append(TransactionId).Append("\n");
        sb.Append("  CreditorName: ").Append(CreditorName).Append("\n");
        sb.Append("  DebtorName: ").Append(DebtorName).Append("\n");
        sb.Append("  CreditorAccount: ").Append(CreditorAccount).Append("\n");
        sb.Append("  DebtorAccount: ").Append(DebtorAccount).Append("\n");
        sb.Append("  TransactionAmount: ").Append(TransactionAmount).Append("\n");
        sb.Append("  BookingDate: ").Append(BookingDate).Append("\n");
        sb.Append("  ValueDate: ").Append(ValueDate).Append("\n");
        sb.Append("  RemittanceInformationUnstructured: ").Append(RemittanceInformationUnstructured).Append("\n");
        sb.Append("  RemittanceInformationStructured: ").Append(RemittanceInformationStructured).Append("\n");
        sb.Append("  TransactionListId: ").Append(TransactionListId).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}