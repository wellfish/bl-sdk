using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Amount
{
    /// <summary>
    ///     Gets or Sets Currency
    /// </summary>
    [DataMember(Name = "currency", EmitDefaultValue = false)]
    [JsonPropertyName("currency")]
    public string Currency { get; set; }

    /// <summary>
    ///     Gets or Sets _Amount
    /// </summary>
    [DataMember(Name = "amount", EmitDefaultValue = false)]
    [JsonPropertyName("amount")]
    public string _Amount { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Amount {\n");
        sb.Append("  Currency: ").Append(Currency).Append("\n");
        sb.Append("  _Amount: ").Append(_Amount).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}