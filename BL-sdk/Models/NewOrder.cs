using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
///     New order
/// </summary>
[DataContract]
public sealed class NewOrder
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "box", EmitDefaultValue = false)]
    [JsonPropertyName("box")]
    public string Box { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "city", EmitDefaultValue = false)]
    [JsonPropertyName("city")]
    public string City { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "comment", EmitDefaultValue = false)]
    [JsonPropertyName("comment")]
    public string Comment { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "country", EmitDefaultValue = false)]
    [JsonPropertyName("country")]
    public string Country { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "costBearerId", EmitDefaultValue = false)]
    [JsonPropertyName("costBearerId")]
    public string CostBearerId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "costCenterId", EmitDefaultValue = false)]
    [JsonPropertyName("costCenterId")]
    public string CostCenterId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "currency", EmitDefaultValue = false)]
    [JsonPropertyName("currency")]
    public string Currency { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "customerId", EmitDefaultValue = false)]
    [JsonPropertyName("customerId")]
    public string CustomerId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "customerOrderNumber", EmitDefaultValue = false)]
    [JsonPropertyName("customerOrderNumber")]
    public string CustomerOrderNumber { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "deliveryBox", EmitDefaultValue = false)]
    [JsonPropertyName("deliveryBox")]
    public string DeliveryBox { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "deliveryBy", EmitDefaultValue = false)]
    [JsonPropertyName("deliveryBy")]
    public string DeliveryBy { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "deliveryCity", EmitDefaultValue = false)]
    [JsonPropertyName("deliveryCity")]
    public string DeliveryCity { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "deliveryCountry", EmitDefaultValue = false)]
    [JsonPropertyName("deliveryCountry")]
    public string DeliveryCountry { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "deliveryDate", EmitDefaultValue = false)]
    [JsonPropertyName("deliveryDate")]
    public DateTime? DeliveryDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "deliveryMethodText", EmitDefaultValue = false)]
    [JsonPropertyName("deliveryMethodText")]
    public string DeliveryMethodText { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "deliveryName", EmitDefaultValue = false)]
    [JsonPropertyName("deliveryName")]
    public string DeliveryName { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "deliveryReference", EmitDefaultValue = false)]
    [JsonPropertyName("deliveryReference")]
    public string DeliveryReference { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "deliveryStreet", EmitDefaultValue = false)]
    [JsonPropertyName("deliveryStreet")]
    public string DeliveryStreet { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "deliveryTermsText", EmitDefaultValue = false)]
    [JsonPropertyName("deliveryTermsText")]
    public string DeliveryTermsText { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "deliveryZip", EmitDefaultValue = false)]
    [JsonPropertyName("deliveryZip")]
    public string DeliveryZip { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "department", EmitDefaultValue = false)]
    [JsonPropertyName("department")]
    public string Department { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "discountAmount", EmitDefaultValue = false)]
    [JsonPropertyName("discountAmount")]
    public decimal? DiscountAmount { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "discountPercentage", EmitDefaultValue = false)]
    [JsonPropertyName("discountPercentage")]
    public double? DiscountPercentage { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "description", EmitDefaultValue = false)]
    [JsonPropertyName("description")]
    public string Description { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "documentHeader", EmitDefaultValue = false)]
    [JsonPropertyName("documentHeader")]
    public string DocumentHeader { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "email", EmitDefaultValue = false)]
    [JsonPropertyName("email")]
    public string Email { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "freight", EmitDefaultValue = false)]
    [JsonPropertyName("freight")]
    public decimal? Freight { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "freightGross", EmitDefaultValue = false)]
    [JsonPropertyName("freightGross")]
    public decimal? FreightGross { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "freightVatPercentage", EmitDefaultValue = false)]
    [JsonPropertyName("freightVatPercentage")]
    public double? FreightVatPercentage { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "invoiceDate", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceDate")]
    public DateTime? InvoiceDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "invoiceDueDate", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceDueDate")]
    public DateTime? InvoiceDueDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "invoiceNumber", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceNumber")]
    public long? InvoiceNumber { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "invoicePrinted", EmitDefaultValue = false)]
    [JsonPropertyName("invoicePrinted")]
    public bool? InvoicePrinted { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "invoicingBlocked", EmitDefaultValue = false)]
    [JsonPropertyName("invoicingBlocked")]
    public bool? InvoicingBlocked { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "lines", EmitDefaultValue = false)]
    [JsonPropertyName("lines")]
    public List<OrderLine> Lines { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "noVat", EmitDefaultValue = false)]
    [JsonPropertyName("noVat")]
    public bool? NoVat { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "ocrCheck", EmitDefaultValue = false)]
    [JsonPropertyName("ocrCheck")]
    public string OcrCheck { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "ocrRef", EmitDefaultValue = false)]
    [JsonPropertyName("ocrRef")]
    public string OcrRef { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "ourReference", EmitDefaultValue = false)]
    [JsonPropertyName("ourReference")]
    public string OurReference { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "orderRegistrationDate", EmitDefaultValue = false)]
    [JsonPropertyName("orderRegistrationDate")]
    public DateTime? OrderRegistrationDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "orderType", EmitDefaultValue = false)]
    [JsonPropertyName("orderType")]
    public string OrderType { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "paymentTerms", EmitDefaultValue = false)]
    [JsonPropertyName("paymentTerms")]
    public string PaymentTerms { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "paymentTermsText", EmitDefaultValue = false)]
    [JsonPropertyName("paymentTermsText")]
    public string PaymentTermsText { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "preliminary", EmitDefaultValue = false)]
    [JsonPropertyName("preliminary")]
    public bool? Preliminary { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "projectId", EmitDefaultValue = false)]
    [JsonPropertyName("projectId")]
    public string ProjectId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "registeredByUser", EmitDefaultValue = false)]
    [JsonPropertyName("registeredByUser")]
    public string RegisteredByUser { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "requisitionNumber", EmitDefaultValue = false)]
    [JsonPropertyName("requisitionNumber")]
    public string RequisitionNumber { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "rounding", EmitDefaultValue = false)]
    [JsonPropertyName("rounding")]
    public decimal? Rounding { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "serviceFee", EmitDefaultValue = false)]
    [JsonPropertyName("serviceFee")]
    public decimal? ServiceFee { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "serviceFeeGross", EmitDefaultValue = false)]
    [JsonPropertyName("serviceFeeGross")]
    public decimal? ServiceFeeGross { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "serviceFeeVatPercentage", EmitDefaultValue = false)]
    [JsonPropertyName("serviceFeeVatPercentage")]
    public double? ServiceFeeVatPercentage { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "street", EmitDefaultValue = false)]
    [JsonPropertyName("street")]
    public string Street { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "telephone", EmitDefaultValue = false)]
    [JsonPropertyName("telephone")]
    public string Telephone { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "volume", EmitDefaultValue = false)]
    [JsonPropertyName("volume")]
    public double? Volume { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "weight", EmitDefaultValue = false)]
    [JsonPropertyName("weight")]
    public double? Weight { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "yourReference", EmitDefaultValue = false)]
    [JsonPropertyName("yourReference")]
    public string YourReference { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "zip", EmitDefaultValue = false)]
    [JsonPropertyName("zip")]
    public string Zip { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class NewOrder {\n");
        sb.Append("  Box: ").Append(Box).Append("\n");
        sb.Append("  City: ").Append(City).Append("\n");
        sb.Append("  Comment: ").Append(Comment).Append("\n");
        sb.Append("  Country: ").Append(Country).Append("\n");
        sb.Append("  CostBearerId: ").Append(CostBearerId).Append("\n");
        sb.Append("  CostCenterId: ").Append(CostCenterId).Append("\n");
        sb.Append("  Currency: ").Append(Currency).Append("\n");
        sb.Append("  CustomerId: ").Append(CustomerId).Append("\n");
        sb.Append("  CustomerOrderNumber: ").Append(CustomerOrderNumber).Append("\n");
        sb.Append("  DeliveryBox: ").Append(DeliveryBox).Append("\n");
        sb.Append("  DeliveryBy: ").Append(DeliveryBy).Append("\n");
        sb.Append("  DeliveryCity: ").Append(DeliveryCity).Append("\n");
        sb.Append("  DeliveryCountry: ").Append(DeliveryCountry).Append("\n");
        sb.Append("  DeliveryDate: ").Append(DeliveryDate).Append("\n");
        sb.Append("  DeliveryMethodText: ").Append(DeliveryMethodText).Append("\n");
        sb.Append("  DeliveryName: ").Append(DeliveryName).Append("\n");
        sb.Append("  DeliveryReference: ").Append(DeliveryReference).Append("\n");
        sb.Append("  DeliveryStreet: ").Append(DeliveryStreet).Append("\n");
        sb.Append("  DeliveryTermsText: ").Append(DeliveryTermsText).Append("\n");
        sb.Append("  DeliveryZip: ").Append(DeliveryZip).Append("\n");
        sb.Append("  Department: ").Append(Department).Append("\n");
        sb.Append("  DiscountAmount: ").Append(DiscountAmount).Append("\n");
        sb.Append("  DiscountPercentage: ").Append(DiscountPercentage).Append("\n");
        sb.Append("  Description: ").Append(Description).Append("\n");
        sb.Append("  DocumentHeader: ").Append(DocumentHeader).Append("\n");
        sb.Append("  Email: ").Append(Email).Append("\n");
        sb.Append("  Freight: ").Append(Freight).Append("\n");
        sb.Append("  FreightGross: ").Append(FreightGross).Append("\n");
        sb.Append("  FreightVatPercentage: ").Append(FreightVatPercentage).Append("\n");
        sb.Append("  InvoiceDate: ").Append(InvoiceDate).Append("\n");
        sb.Append("  InvoiceDueDate: ").Append(InvoiceDueDate).Append("\n");
        sb.Append("  InvoiceNumber: ").Append(InvoiceNumber).Append("\n");
        sb.Append("  InvoicePrinted: ").Append(InvoicePrinted).Append("\n");
        sb.Append("  InvoicingBlocked: ").Append(InvoicingBlocked).Append("\n");
        sb.Append("  Lines: ").Append(Lines).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  NoVat: ").Append(NoVat).Append("\n");
        sb.Append("  OcrCheck: ").Append(OcrCheck).Append("\n");
        sb.Append("  OcrRef: ").Append(OcrRef).Append("\n");
        sb.Append("  OurReference: ").Append(OurReference).Append("\n");
        sb.Append("  OrderRegistrationDate: ").Append(OrderRegistrationDate).Append("\n");
        sb.Append("  OrderType: ").Append(OrderType).Append("\n");
        sb.Append("  PaymentTerms: ").Append(PaymentTerms).Append("\n");
        sb.Append("  PaymentTermsText: ").Append(PaymentTermsText).Append("\n");
        sb.Append("  Preliminary: ").Append(Preliminary).Append("\n");
        sb.Append("  ProjectId: ").Append(ProjectId).Append("\n");
        sb.Append("  RegisteredByUser: ").Append(RegisteredByUser).Append("\n");
        sb.Append("  RequisitionNumber: ").Append(RequisitionNumber).Append("\n");
        sb.Append("  Rounding: ").Append(Rounding).Append("\n");
        sb.Append("  ServiceFee: ").Append(ServiceFee).Append("\n");
        sb.Append("  ServiceFeeGross: ").Append(ServiceFeeGross).Append("\n");
        sb.Append("  ServiceFeeVatPercentage: ").Append(ServiceFeeVatPercentage).Append("\n");
        sb.Append("  Street: ").Append(Street).Append("\n");
        sb.Append("  Telephone: ").Append(Telephone).Append("\n");
        sb.Append("  Volume: ").Append(Volume).Append("\n");
        sb.Append("  Weight: ").Append(Weight).Append("\n");
        sb.Append("  YourReference: ").Append(YourReference).Append("\n");
        sb.Append("  Zip: ").Append(Zip).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}