using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Supplier
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets Address1
    /// </summary>
    [DataMember(Name = "address1", EmitDefaultValue = false)]
    [JsonPropertyName("address1")]
    public string Address1 { get; set; }

    /// <summary>
    ///     Gets or Sets Address2
    /// </summary>
    [DataMember(Name = "address2", EmitDefaultValue = false)]
    [JsonPropertyName("address2")]
    public string Address2 { get; set; }

    /// <summary>
    ///     Gets or Sets AutoRegInvoice
    /// </summary>
    [DataMember(Name = "autoRegInvoice", EmitDefaultValue = false)]
    [JsonPropertyName("autoRegInvoice")]
    public bool? AutoRegInvoice { get; set; }

    /// <summary>
    ///     Gets or Sets BankAccount
    /// </summary>
    [DataMember(Name = "bankAccount", EmitDefaultValue = false)]
    [JsonPropertyName("bankAccount")]
    public string BankAccount { get; set; }

    /// <summary>
    ///     Gets or Sets Bg
    /// </summary>
    [DataMember(Name = "bg", EmitDefaultValue = false)]
    [JsonPropertyName("bg")]
    public string Bg { get; set; }

    /// <summary>
    ///     Gets or Sets Category
    /// </summary>
    [DataMember(Name = "category", EmitDefaultValue = false)]
    [JsonPropertyName("category")]
    public string Category { get; set; }

    /// <summary>
    ///     Gets or Sets City
    /// </summary>
    [DataMember(Name = "city", EmitDefaultValue = false)]
    [JsonPropertyName("city")]
    public string City { get; set; }

    /// <summary>
    ///     Gets or Sets Closed
    /// </summary>
    [DataMember(Name = "closed", EmitDefaultValue = false)]
    [JsonPropertyName("closed")]
    public bool? Closed { get; set; }

    /// <summary>
    ///     Gets or Sets Comment
    /// </summary>
    [DataMember(Name = "comment", EmitDefaultValue = false)]
    [JsonPropertyName("comment")]
    public string Comment { get; set; }

    /// <summary>
    ///     Gets or Sets CostAccount
    /// </summary>
    [DataMember(Name = "costAccount", EmitDefaultValue = false)]
    [JsonPropertyName("costAccount")]
    public string CostAccount { get; set; }

    /// <summary>
    ///     Gets or Sets Country
    /// </summary>
    [DataMember(Name = "country", EmitDefaultValue = false)]
    [JsonPropertyName("country")]
    public string Country { get; set; }

    /// <summary>
    ///     Gets or Sets Currency
    /// </summary>
    [DataMember(Name = "currency", EmitDefaultValue = false)]
    [JsonPropertyName("currency")]
    public string Currency { get; set; }

    /// <summary>
    ///     Gets or Sets DefaultAttestant
    /// </summary>
    [DataMember(Name = "defaultAttestant", EmitDefaultValue = false)]
    [JsonPropertyName("defaultAttestant")]
    public string DefaultAttestant { get; set; }

    /// <summary>
    ///     Gets or Sets DefaultOcr
    /// </summary>
    [DataMember(Name = "defaultOcr", EmitDefaultValue = false)]
    [JsonPropertyName("defaultOcr")]
    public string DefaultOcr { get; set; }

    /// <summary>
    ///     Direct debit (autogiro)
    /// </summary>
    /// <value>Direct debit (autogiro)</value>
    [DataMember(Name = "directDebit", EmitDefaultValue = false)]
    [JsonPropertyName("directDebit")]
    public bool? DirectDebit { get; set; }

    /// <summary>
    ///     Gets or Sets Email
    /// </summary>
    [DataMember(Name = "email", EmitDefaultValue = false)]
    [JsonPropertyName("email")]
    public string Email { get; set; }

    /// <summary>
    ///     Gets or Sets Fax
    /// </summary>
    [DataMember(Name = "fax", EmitDefaultValue = false)]
    [JsonPropertyName("fax")]
    public string Fax { get; set; }

    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    /// <summary>
    ///     Gets or Sets Name
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     Gets or Sets OrganisationId
    /// </summary>
    [DataMember(Name = "organisationId", EmitDefaultValue = false)]
    [JsonPropertyName("organisationId")]
    public string OrganisationId { get; set; }

    /// <summary>
    ///     Gets or Sets PaymentTerms
    /// </summary>
    [DataMember(Name = "paymentTerms", EmitDefaultValue = false)]
    [JsonPropertyName("paymentTerms")]
    public string PaymentTerms { get; set; }

    /// <summary>
    ///     Gets or Sets Pg
    /// </summary>
    [DataMember(Name = "pg", EmitDefaultValue = false)]
    [JsonPropertyName("pg")]
    public string Pg { get; set; }

    /// <summary>
    ///     Gets or Sets Phone
    /// </summary>
    [DataMember(Name = "phone", EmitDefaultValue = false)]
    [JsonPropertyName("phone")]
    public string Phone { get; set; }

    /// <summary>
    ///     Gets or Sets ReceivableAccount
    /// </summary>
    [DataMember(Name = "receivableAccount", EmitDefaultValue = false)]
    [JsonPropertyName("receivableAccount")]
    public string ReceivableAccount { get; set; }

    /// <summary>
    ///     Gets or Sets VatCode
    /// </summary>
    [DataMember(Name = "vatCode", EmitDefaultValue = false)]
    [JsonPropertyName("vatCode")]
    public int? VatCode { get; set; }

    /// <summary>
    ///     Gets or Sets VatNr
    /// </summary>
    [DataMember(Name = "vatNr", EmitDefaultValue = false)]
    [JsonPropertyName("vatNr")]
    public string VatNr { get; set; }

    /// <summary>
    ///     Gets or Sets Web
    /// </summary>
    [DataMember(Name = "web", EmitDefaultValue = false)]
    [JsonPropertyName("web")]
    public string Web { get; set; }

    /// <summary>
    ///     Gets or Sets YourReference
    /// </summary>
    [DataMember(Name = "yourReference", EmitDefaultValue = false)]
    [JsonPropertyName("yourReference")]
    public string YourReference { get; set; }

    /// <summary>
    ///     Gets or Sets ZipCode
    /// </summary>
    [DataMember(Name = "zipCode", EmitDefaultValue = false)]
    [JsonPropertyName("zipCode")]
    public string ZipCode { get; set; }

    /// <summary>
    ///     Gets or Sets CostBearer
    /// </summary>
    [DataMember(Name = "costBearer", EmitDefaultValue = false)]
    [JsonPropertyName("costBearer")]
    public string CostBearer { get; set; }

    /// <summary>
    ///     Gets or Sets CostCenter
    /// </summary>
    [DataMember(Name = "costCenter", EmitDefaultValue = false)]
    [JsonPropertyName("costCenter")]
    public string CostCenter { get; set; }

    /// <summary>
    ///     Gets or Sets Project
    /// </summary>
    [DataMember(Name = "project", EmitDefaultValue = false)]
    [JsonPropertyName("project")]
    public string Project { get; set; }

    /// <summary>
    ///     Gets or Sets TotalInvoicedAmount
    /// </summary>
    [DataMember(Name = "totalInvoicedAmount", EmitDefaultValue = false)]
    [JsonPropertyName("totalInvoicedAmount")]
    public double? TotalInvoicedAmount { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Supplier {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Address1: ").Append(Address1).Append("\n");
        sb.Append("  Address2: ").Append(Address2).Append("\n");
        sb.Append("  AutoRegInvoice: ").Append(AutoRegInvoice).Append("\n");
        sb.Append("  BankAccount: ").Append(BankAccount).Append("\n");
        sb.Append("  Bg: ").Append(Bg).Append("\n");
        sb.Append("  Category: ").Append(Category).Append("\n");
        sb.Append("  City: ").Append(City).Append("\n");
        sb.Append("  Closed: ").Append(Closed).Append("\n");
        sb.Append("  Comment: ").Append(Comment).Append("\n");
        sb.Append("  CostAccount: ").Append(CostAccount).Append("\n");
        sb.Append("  Country: ").Append(Country).Append("\n");
        sb.Append("  Currency: ").Append(Currency).Append("\n");
        sb.Append("  DefaultAttestant: ").Append(DefaultAttestant).Append("\n");
        sb.Append("  DefaultOcr: ").Append(DefaultOcr).Append("\n");
        sb.Append("  DirectDebit: ").Append(DirectDebit).Append("\n");
        sb.Append("  Email: ").Append(Email).Append("\n");
        sb.Append("  Fax: ").Append(Fax).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  OrganisationId: ").Append(OrganisationId).Append("\n");
        sb.Append("  PaymentTerms: ").Append(PaymentTerms).Append("\n");
        sb.Append("  Pg: ").Append(Pg).Append("\n");
        sb.Append("  Phone: ").Append(Phone).Append("\n");
        sb.Append("  ReceivableAccount: ").Append(ReceivableAccount).Append("\n");
        sb.Append("  VatCode: ").Append(VatCode).Append("\n");
        sb.Append("  VatNr: ").Append(VatNr).Append("\n");
        sb.Append("  Web: ").Append(Web).Append("\n");
        sb.Append("  YourReference: ").Append(YourReference).Append("\n");
        sb.Append("  ZipCode: ").Append(ZipCode).Append("\n");
        sb.Append("  CostBearer: ").Append(CostBearer).Append("\n");
        sb.Append("  CostCenter: ").Append(CostCenter).Append("\n");
        sb.Append("  Project: ").Append(Project).Append("\n");
        sb.Append("  TotalInvoicedAmount: ").Append(TotalInvoicedAmount).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}