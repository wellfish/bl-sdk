using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class VatInfo
{
    /// <summary>
    ///     Gets or Sets Vat1account
    /// </summary>
    [DataMember(Name = "vat1account", EmitDefaultValue = false)]
    [JsonPropertyName("vat1account")]
    public string Vat1account { get; set; }

    /// <summary>
    ///     Gets or Sets Vat2account
    /// </summary>
    [DataMember(Name = "vat2account", EmitDefaultValue = false)]
    [JsonPropertyName("vat2account")]
    public string Vat2account { get; set; }

    /// <summary>
    ///     Gets or Sets Vat3account
    /// </summary>
    [DataMember(Name = "vat3account", EmitDefaultValue = false)]
    [JsonPropertyName("vat3account")]
    public string Vat3account { get; set; }

    /// <summary>
    ///     Gets or Sets Vat1percentage
    /// </summary>
    [DataMember(Name = "vat1percentage", EmitDefaultValue = false)]
    [JsonPropertyName("vat1percentage")]
    public string Vat1percentage { get; set; }

    /// <summary>
    ///     Gets or Sets Vat2percentage
    /// </summary>
    [DataMember(Name = "vat2percentage", EmitDefaultValue = false)]
    [JsonPropertyName("vat2percentage")]
    public string Vat2percentage { get; set; }

    /// <summary>
    ///     Gets or Sets Vat3percentage
    /// </summary>
    [DataMember(Name = "vat3percentage", EmitDefaultValue = false)]
    [JsonPropertyName("vat3percentage")]
    public string Vat3percentage { get; set; }

    /// <summary>
    ///     Gets or Sets InboundVatAccount
    /// </summary>
    [DataMember(Name = "inboundVatAccount", EmitDefaultValue = false)]
    [JsonPropertyName("inboundVatAccount")]
    public string InboundVatAccount { get; set; }

    /// <summary>
    ///     Gets or Sets ReverseVat1Account
    /// </summary>
    [DataMember(Name = "reverseVat1Account", EmitDefaultValue = false)]
    [JsonPropertyName("reverseVat1Account")]
    public string ReverseVat1Account { get; set; }

    /// <summary>
    ///     Gets or Sets ReverseVat2Account
    /// </summary>
    [DataMember(Name = "reverseVat2Account", EmitDefaultValue = false)]
    [JsonPropertyName("reverseVat2Account")]
    public string ReverseVat2Account { get; set; }

    /// <summary>
    ///     Gets or Sets ReverseVat3Account
    /// </summary>
    [DataMember(Name = "reverseVat3Account", EmitDefaultValue = false)]
    [JsonPropertyName("reverseVat3Account")]
    public string ReverseVat3Account { get; set; }

    /// <summary>
    ///     Gets or Sets ReverseInboundVatAccount
    /// </summary>
    [DataMember(Name = "reverseInboundVatAccount", EmitDefaultValue = false)]
    [JsonPropertyName("reverseInboundVatAccount")]
    public string ReverseInboundVatAccount { get; set; }

    /// <summary>
    ///     Gets or Sets ImportVat1Account
    /// </summary>
    [DataMember(Name = "importVat1Account", EmitDefaultValue = false)]
    [JsonPropertyName("importVat1Account")]
    public string ImportVat1Account { get; set; }

    /// <summary>
    ///     Gets or Sets ImportVat2Account
    /// </summary>
    [DataMember(Name = "importVat2Account", EmitDefaultValue = false)]
    [JsonPropertyName("importVat2Account")]
    public string ImportVat2Account { get; set; }

    /// <summary>
    ///     Gets or Sets ImportVat3Account
    /// </summary>
    [DataMember(Name = "importVat3Account", EmitDefaultValue = false)]
    [JsonPropertyName("importVat3Account")]
    public string ImportVat3Account { get; set; }

    /// <summary>
    ///     Gets or Sets InboundImportVatAccount
    /// </summary>
    [DataMember(Name = "inboundImportVatAccount", EmitDefaultValue = false)]
    [JsonPropertyName("inboundImportVatAccount")]
    public string InboundImportVatAccount { get; set; }

    /// <summary>
    ///     Gets or Sets AccountingVat
    /// </summary>
    [DataMember(Name = "accountingVat", EmitDefaultValue = false)]
    [JsonPropertyName("accountingVat")]
    public string AccountingVat { get; set; }

    /// <summary>
    ///     Gets or Sets RoundingVatAccount
    /// </summary>
    [DataMember(Name = "roundingVatAccount", EmitDefaultValue = false)]
    [JsonPropertyName("roundingVatAccount")]
    public string RoundingVatAccount { get; set; }

    /// <summary>
    ///     Gets or Sets VatLess
    /// </summary>
    [DataMember(Name = "vatLess", EmitDefaultValue = false)]
    [JsonPropertyName("vatLess")]
    public bool? VatLess { get; set; }

    /// <summary>
    ///     Gets or Sets AntMan
    /// </summary>
    [DataMember(Name = "antMan", EmitDefaultValue = false)]
    [JsonPropertyName("antMan")]
    public int? AntMan { get; set; }

    /// <summary>
    ///     Gets or Sets EntityId
    /// </summary>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class VatInfo {\n");
        sb.Append("  Vat1account: ").Append(Vat1account).Append("\n");
        sb.Append("  Vat2account: ").Append(Vat2account).Append("\n");
        sb.Append("  Vat3account: ").Append(Vat3account).Append("\n");
        sb.Append("  Vat1percentage: ").Append(Vat1percentage).Append("\n");
        sb.Append("  Vat2percentage: ").Append(Vat2percentage).Append("\n");
        sb.Append("  Vat3percentage: ").Append(Vat3percentage).Append("\n");
        sb.Append("  InboundVatAccount: ").Append(InboundVatAccount).Append("\n");
        sb.Append("  ReverseVat1Account: ").Append(ReverseVat1Account).Append("\n");
        sb.Append("  ReverseVat2Account: ").Append(ReverseVat2Account).Append("\n");
        sb.Append("  ReverseVat3Account: ").Append(ReverseVat3Account).Append("\n");
        sb.Append("  ReverseInboundVatAccount: ").Append(ReverseInboundVatAccount).Append("\n");
        sb.Append("  ImportVat1Account: ").Append(ImportVat1Account).Append("\n");
        sb.Append("  ImportVat2Account: ").Append(ImportVat2Account).Append("\n");
        sb.Append("  ImportVat3Account: ").Append(ImportVat3Account).Append("\n");
        sb.Append("  InboundImportVatAccount: ").Append(InboundImportVatAccount).Append("\n");
        sb.Append("  AccountingVat: ").Append(AccountingVat).Append("\n");
        sb.Append("  RoundingVatAccount: ").Append(RoundingVatAccount).Append("\n");
        sb.Append("  VatLess: ").Append(VatLess).Append("\n");
        sb.Append("  AntMan: ").Append(AntMan).Append("\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}