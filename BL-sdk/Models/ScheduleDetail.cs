using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class ScheduleDetail
{
    /// <summary>
    ///     Gets or Sets Amount
    /// </summary>
    [DataMember(Name = "amount", EmitDefaultValue = false)]
    [JsonPropertyName("amount")]
    public double? Amount { get; set; }

    /// <summary>
    ///     Gets or Sets Unit
    /// </summary>
    [DataMember(Name = "unit", EmitDefaultValue = false)]
    [JsonPropertyName("unit")]
    public string Unit { get; set; }

    /// <summary>
    ///     Gets or Sets ActivityCode
    /// </summary>
    [DataMember(Name = "activityCode", EmitDefaultValue = false)]
    [JsonPropertyName("activityCode")]
    public string ActivityCode { get; set; }

    /// <summary>
    ///     Gets or Sets CompensationCode
    /// </summary>
    [DataMember(Name = "compensationCode", EmitDefaultValue = false)]
    [JsonPropertyName("compensationCode")]
    public string CompensationCode { get; set; }

    /// <summary>
    ///     Gets or Sets HolidayFounding
    /// </summary>
    [DataMember(Name = "holidayFounding", EmitDefaultValue = false)]
    [JsonPropertyName("holidayFounding")]
    public bool? HolidayFounding { get; set; }

    /// <summary>
    ///     Gets or Sets Invalidated
    /// </summary>
    [DataMember(Name = "invalidated", EmitDefaultValue = false)]
    [JsonPropertyName("invalidated")]
    public bool? Invalidated { get; set; }

    /// <summary>
    ///     Gets or Sets From
    /// </summary>
    [DataMember(Name = "from", EmitDefaultValue = false)]
    [JsonPropertyName("from")]
    public string From { get; set; }

    /// <summary>
    ///     Gets or Sets To
    /// </summary>
    [DataMember(Name = "to", EmitDefaultValue = false)]
    [JsonPropertyName("to")]
    public string To { get; set; }

    /// <summary>
    ///     Gets or Sets EntityId
    /// </summary>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class ScheduleDetail {\n");
        sb.Append("  Amount: ").Append(Amount).Append("\n");
        sb.Append("  Unit: ").Append(Unit).Append("\n");
        sb.Append("  ActivityCode: ").Append(ActivityCode).Append("\n");
        sb.Append("  CompensationCode: ").Append(CompensationCode).Append("\n");
        sb.Append("  HolidayFounding: ").Append(HolidayFounding).Append("\n");
        sb.Append("  Invalidated: ").Append(Invalidated).Append("\n");
        sb.Append("  From: ").Append(From).Append("\n");
        sb.Append("  To: ").Append(To).Append("\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}