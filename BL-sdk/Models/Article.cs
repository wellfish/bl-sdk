using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Article
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "account", EmitDefaultValue = false)]
    [JsonPropertyName("account")]
    public string Account { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "availableStock", EmitDefaultValue = false)]
    [JsonPropertyName("availableStock")]
    public double? AvailableStock { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "barcode", EmitDefaultValue = false)]
    [JsonPropertyName("barcode")]
    public string Barcode { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "closed", EmitDefaultValue = false)]
    [JsonPropertyName("closed")]
    public bool? Closed { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "comment", EmitDefaultValue = false)]
    [JsonPropertyName("comment")]
    public string Comment { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "costBearer", EmitDefaultValue = false)]
    [JsonPropertyName("costBearer")]
    public string CostBearer { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "costCenter", EmitDefaultValue = false)]
    [JsonPropertyName("costCenter")]
    public string CostCenter { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "costPrice", EmitDefaultValue = false)]
    [JsonPropertyName("costPrice")]
    public decimal? CostPrice { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "descriptionDE", EmitDefaultValue = false)]
    [JsonPropertyName("descriptionDE")]
    public string DescriptionDE { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "descriptionES", EmitDefaultValue = false)]
    [JsonPropertyName("descriptionES")]
    public string DescriptionES { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "descriptionFR", EmitDefaultValue = false)]
    [JsonPropertyName("descriptionFR")]
    public string DescriptionFR { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "descriptionIT", EmitDefaultValue = false)]
    [JsonPropertyName("descriptionIT")]
    public string DescriptionIT { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "descriptionUK", EmitDefaultValue = false)]
    [JsonPropertyName("descriptionUK")]
    public string DescriptionUK { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "groupId", EmitDefaultValue = false)]
    [JsonPropertyName("groupId")]
    public string GroupId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "inactive", EmitDefaultValue = false)]
    [JsonPropertyName("inactive")]
    public bool? Inactive { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "orderingPoint", EmitDefaultValue = false)]
    [JsonPropertyName("orderingPoint")]
    public double? OrderingPoint { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "orderingQuantity", EmitDefaultValue = false)]
    [JsonPropertyName("orderingQuantity")]
    public double? OrderingQuantity { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "physicalStock", EmitDefaultValue = false)]
    [JsonPropertyName("physicalStock")]
    public double? PhysicalStock { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "project", EmitDefaultValue = false)]
    [JsonPropertyName("project")]
    public string Project { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "purchaseAccount", EmitDefaultValue = false)]
    [JsonPropertyName("purchaseAccount")]
    public string PurchaseAccount { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "sellPrice1", EmitDefaultValue = false)]
    [JsonPropertyName("sellPrice1")]
    public decimal? SellPrice1 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "sellPrice2", EmitDefaultValue = false)]
    [JsonPropertyName("sellPrice2")]
    public decimal? SellPrice2 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "sellPrice3", EmitDefaultValue = false)]
    [JsonPropertyName("sellPrice3")]
    public decimal? SellPrice3 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "sellPrice4", EmitDefaultValue = false)]
    [JsonPropertyName("sellPrice4")]
    public decimal? SellPrice4 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "sellPrice5", EmitDefaultValue = false)]
    [JsonPropertyName("sellPrice5")]
    public decimal? SellPrice5 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "sellPriceIncVat", EmitDefaultValue = false)]
    [JsonPropertyName("sellPriceIncVat")]
    public decimal? SellPriceIncVat { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "supplierArticleId", EmitDefaultValue = false)]
    [JsonPropertyName("supplierArticleId")]
    public string SupplierArticleId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "supplierId", EmitDefaultValue = false)]
    [JsonPropertyName("supplierId")]
    public string SupplierId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "stockPlacement", EmitDefaultValue = false)]
    [JsonPropertyName("stockPlacement")]
    public string StockPlacement { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "taxDeductable", EmitDefaultValue = false)]
    [JsonPropertyName("taxDeductable")]
    public bool? TaxDeductable { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "taxDeductionCode", EmitDefaultValue = false)]
    [JsonPropertyName("taxDeductionCode")]
    public int? TaxDeductionCode { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "totalInvoicedAmount", EmitDefaultValue = false)]
    [JsonPropertyName("totalInvoicedAmount")]
    public double? TotalInvoicedAmount { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "type", EmitDefaultValue = false)]
    [JsonPropertyName("type")]
    public int? Type { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "unitCostPrice", EmitDefaultValue = false)]
    [JsonPropertyName("unitCostPrice")]
    public string UnitCostPrice { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "unitSellPrice1", EmitDefaultValue = false)]
    [JsonPropertyName("unitSellPrice1")]
    public string UnitSellPrice1 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "unitSellPrice2", EmitDefaultValue = false)]
    [JsonPropertyName("unitSellPrice2")]
    public string UnitSellPrice2 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "unitSellPrice3", EmitDefaultValue = false)]
    [JsonPropertyName("unitSellPrice3")]
    public string UnitSellPrice3 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "unitSellPrice4", EmitDefaultValue = false)]
    [JsonPropertyName("unitSellPrice4")]
    public string UnitSellPrice4 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "unitSellPrice5", EmitDefaultValue = false)]
    [JsonPropertyName("unitSellPrice5")]
    public string UnitSellPrice5 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "vatAccount", EmitDefaultValue = false)]
    [JsonPropertyName("vatAccount")]
    public string VatAccount { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "vatCode", EmitDefaultValue = false)]
    [JsonPropertyName("vatCode")]
    public string VatCode { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "volume", EmitDefaultValue = false)]
    [JsonPropertyName("volume")]
    public double? Volume { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "weight", EmitDefaultValue = false)]
    [JsonPropertyName("weight")]
    public double? Weight { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "weightedCostPrice", EmitDefaultValue = false)]
    [JsonPropertyName("weightedCostPrice")]
    public decimal? WeightedCostPrice { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "documentIds", EmitDefaultValue = false)]
    [JsonPropertyName("documentIds")]
    public List<long>? DocumentIds { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "computingPrice", EmitDefaultValue = false)]
    [JsonPropertyName("computingPrice")]
    public decimal? ComputingPrice { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Article {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Account: ").Append(Account).Append("\n");
        sb.Append("  AvailableStock: ").Append(AvailableStock).Append("\n");
        sb.Append("  Barcode: ").Append(Barcode).Append("\n");
        sb.Append("  Closed: ").Append(Closed).Append("\n");
        sb.Append("  Comment: ").Append(Comment).Append("\n");
        sb.Append("  CostBearer: ").Append(CostBearer).Append("\n");
        sb.Append("  CostCenter: ").Append(CostCenter).Append("\n");
        sb.Append("  CostPrice: ").Append(CostPrice).Append("\n");
        sb.Append("  DescriptionDE: ").Append(DescriptionDE).Append("\n");
        sb.Append("  DescriptionES: ").Append(DescriptionES).Append("\n");
        sb.Append("  DescriptionFR: ").Append(DescriptionFR).Append("\n");
        sb.Append("  DescriptionIT: ").Append(DescriptionIT).Append("\n");
        sb.Append("  DescriptionUK: ").Append(DescriptionUK).Append("\n");
        sb.Append("  GroupId: ").Append(GroupId).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Inactive: ").Append(Inactive).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  OrderingPoint: ").Append(OrderingPoint).Append("\n");
        sb.Append("  OrderingQuantity: ").Append(OrderingQuantity).Append("\n");
        sb.Append("  PhysicalStock: ").Append(PhysicalStock).Append("\n");
        sb.Append("  Project: ").Append(Project).Append("\n");
        sb.Append("  PurchaseAccount: ").Append(PurchaseAccount).Append("\n");
        sb.Append("  SellPrice1: ").Append(SellPrice1).Append("\n");
        sb.Append("  SellPrice2: ").Append(SellPrice2).Append("\n");
        sb.Append("  SellPrice3: ").Append(SellPrice3).Append("\n");
        sb.Append("  SellPrice4: ").Append(SellPrice4).Append("\n");
        sb.Append("  SellPrice5: ").Append(SellPrice5).Append("\n");
        sb.Append("  SellPriceIncVat: ").Append(SellPriceIncVat).Append("\n");
        sb.Append("  SupplierArticleId: ").Append(SupplierArticleId).Append("\n");
        sb.Append("  SupplierId: ").Append(SupplierId).Append("\n");
        sb.Append("  StockPlacement: ").Append(StockPlacement).Append("\n");
        sb.Append("  TaxDeductable: ").Append(TaxDeductable).Append("\n");
        sb.Append("  TaxDeductionCode: ").Append(TaxDeductionCode).Append("\n");
        sb.Append("  TotalInvoicedAmount: ").Append(TotalInvoicedAmount).Append("\n");
        sb.Append("  Type: ").Append(Type).Append("\n");
        sb.Append("  UnitCostPrice: ").Append(UnitCostPrice).Append("\n");
        sb.Append("  UnitSellPrice1: ").Append(UnitSellPrice1).Append("\n");
        sb.Append("  UnitSellPrice2: ").Append(UnitSellPrice2).Append("\n");
        sb.Append("  UnitSellPrice3: ").Append(UnitSellPrice3).Append("\n");
        sb.Append("  UnitSellPrice4: ").Append(UnitSellPrice4).Append("\n");
        sb.Append("  UnitSellPrice5: ").Append(UnitSellPrice5).Append("\n");
        sb.Append("  VatAccount: ").Append(VatAccount).Append("\n");
        sb.Append("  VatCode: ").Append(VatCode).Append("\n");
        sb.Append("  Volume: ").Append(Volume).Append("\n");
        sb.Append("  Weight: ").Append(Weight).Append("\n");
        sb.Append("  WeightedCostPrice: ").Append(WeightedCostPrice).Append("\n");
        sb.Append("  DocumentIds: ").Append(DocumentIds).Append("\n");
        sb.Append("  ComputingPrice: ").Append(ComputingPrice).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}