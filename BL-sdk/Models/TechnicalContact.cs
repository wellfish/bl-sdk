using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class TechnicalContact
{
    /// <summary>
    ///     Gets or Sets Email
    /// </summary>
    [DataMember(Name = "email", EmitDefaultValue = false)]
    [JsonPropertyName("email")]
    public string Email { get; set; }

    /// <summary>
    ///     Gets or Sets Name
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     Gets or Sets Zip
    /// </summary>
    [DataMember(Name = "zip", EmitDefaultValue = false)]
    [JsonPropertyName("zip")]
    public string Zip { get; set; }

    /// <summary>
    ///     Gets or Sets City
    /// </summary>
    [DataMember(Name = "city", EmitDefaultValue = false)]
    [JsonPropertyName("city")]
    public string City { get; set; }

    /// <summary>
    ///     Gets or Sets Phone
    /// </summary>
    [DataMember(Name = "phone", EmitDefaultValue = false)]
    [JsonPropertyName("phone")]
    public string Phone { get; set; }

    /// <summary>
    ///     Gets or Sets Address1
    /// </summary>
    [DataMember(Name = "address1", EmitDefaultValue = false)]
    [JsonPropertyName("address1")]
    public string Address1 { get; set; }

    /// <summary>
    ///     Gets or Sets Address2
    /// </summary>
    [DataMember(Name = "address2", EmitDefaultValue = false)]
    [JsonPropertyName("address2")]
    public string Address2 { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class TechnicalContact {\n");
        sb.Append("  Email: ").Append(Email).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  Zip: ").Append(Zip).Append("\n");
        sb.Append("  City: ").Append(City).Append("\n");
        sb.Append("  Phone: ").Append(Phone).Append("\n");
        sb.Append("  Address1: ").Append(Address1).Append("\n");
        sb.Append("  Address2: ").Append(Address2).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}