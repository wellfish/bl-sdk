using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class CostCenterBalancePerAccount
{
    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    /// <summary>
    ///     Gets or Sets Name
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     Gets or Sets FromPeriod
    /// </summary>
    [DataMember(Name = "fromPeriod", EmitDefaultValue = false)]
    [JsonPropertyName("fromPeriod")]
    public string FromPeriod { get; set; }

    /// <summary>
    ///     Gets or Sets ToPeriod
    /// </summary>
    [DataMember(Name = "toPeriod", EmitDefaultValue = false)]
    [JsonPropertyName("toPeriod")]
    public string ToPeriod { get; set; }

    /// <summary>
    ///     Gets or Sets Accounts
    /// </summary>
    [DataMember(Name = "accounts", EmitDefaultValue = false)]
    [JsonPropertyName("accounts")]
    public List<CostCenterBalanceAccountSums> Accounts { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class CostCenterBalancePerAccount {\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  FromPeriod: ").Append(FromPeriod).Append("\n");
        sb.Append("  ToPeriod: ").Append(ToPeriod).Append("\n");
        sb.Append("  Accounts: ").Append(Accounts).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}