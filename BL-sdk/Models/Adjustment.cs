using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Adjustment
{
    /// <summary>
    ///     Gets or Sets Type
    /// </summary>
    [DataMember(Name = "type", EmitDefaultValue = false)]
    [JsonPropertyName("type")]
    public string Type { get; set; }

    /// <summary>
    ///     Gets or Sets Value
    /// </summary>
    [DataMember(Name = "value", EmitDefaultValue = false)]
    [JsonPropertyName("value")]
    public decimal? Value { get; set; }

    /// <summary>
    ///     Gets or Sets TypeAsInteger
    /// </summary>
    [DataMember(Name = "typeAsInteger", EmitDefaultValue = false)]
    [JsonPropertyName("typeAsInteger")]
    public int? TypeAsInteger { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Adjustment {\n");
        sb.Append("  Type: ").Append(Type).Append("\n");
        sb.Append("  Value: ").Append(Value).Append("\n");
        sb.Append("  TypeAsInteger: ").Append(TypeAsInteger).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}