using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class LocalTime
{
    /// <summary>
    ///     Gets or Sets Hour
    /// </summary>
    [DataMember(Name = "hour", EmitDefaultValue = false)]
    [JsonPropertyName("hour")]
    public int? Hour { get; set; }

    /// <summary>
    ///     Gets or Sets Minute
    /// </summary>
    [DataMember(Name = "minute", EmitDefaultValue = false)]
    [JsonPropertyName("minute")]
    public int? Minute { get; set; }

    /// <summary>
    ///     Gets or Sets Second
    /// </summary>
    [DataMember(Name = "second", EmitDefaultValue = false)]
    [JsonPropertyName("second")]
    public int? Second { get; set; }

    /// <summary>
    ///     Gets or Sets Nano
    /// </summary>
    [DataMember(Name = "nano", EmitDefaultValue = false)]
    [JsonPropertyName("nano")]
    public int? Nano { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class LocalTime {\n");
        sb.Append("  Hour: ").Append(Hour).Append("\n");
        sb.Append("  Minute: ").Append(Minute).Append("\n");
        sb.Append("  Second: ").Append(Second).Append("\n");
        sb.Append("  Nano: ").Append(Nano).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}