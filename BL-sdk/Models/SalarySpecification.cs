using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class SalarySpecification
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public long? Id { get; set; }

    /// <summary>
    ///     Gets or Sets Employee
    /// </summary>
    [DataMember(Name = "employee", EmitDefaultValue = false)]
    [JsonPropertyName("employee")]
    public Dictionary<string, object> Employee { get; set; }

    /// <summary>
    ///     Gets or Sets Street
    /// </summary>
    [DataMember(Name = "street", EmitDefaultValue = false)]
    [JsonPropertyName("street")]
    public string Street { get; set; }

    /// <summary>
    ///     Gets or Sets Box
    /// </summary>
    [DataMember(Name = "box", EmitDefaultValue = false)]
    [JsonPropertyName("box")]
    public string Box { get; set; }

    /// <summary>
    ///     Gets or Sets Zipcode
    /// </summary>
    [DataMember(Name = "zipcode", EmitDefaultValue = false)]
    [JsonPropertyName("zipcode")]
    public string Zipcode { get; set; }

    /// <summary>
    ///     Gets or Sets City
    /// </summary>
    [DataMember(Name = "city", EmitDefaultValue = false)]
    [JsonPropertyName("city")]
    public string City { get; set; }

    /// <summary>
    ///     Gets or Sets RefersTo
    /// </summary>
    [DataMember(Name = "refersTo", EmitDefaultValue = false)]
    [JsonPropertyName("refersTo")]
    public string RefersTo { get; set; }

    /// <summary>
    ///     Gets or Sets PaymentDate
    /// </summary>
    [DataMember(Name = "paymentDate", EmitDefaultValue = false)]
    [JsonPropertyName("paymentDate")]
    public DateTime? PaymentDate { get; set; }

    /// <summary>
    ///     Gets or Sets TaxScaleId
    /// </summary>
    [DataMember(Name = "taxScaleId", EmitDefaultValue = false)]
    [JsonPropertyName("taxScaleId")]
    public int? TaxScaleId { get; set; }

    /// <summary>
    ///     Gets or Sets TaxableAmount
    /// </summary>
    [DataMember(Name = "taxableAmount", EmitDefaultValue = false)]
    [JsonPropertyName("taxableAmount")]
    public decimal? TaxableAmount { get; set; }

    /// <summary>
    ///     Gets or Sets SalaryGross
    /// </summary>
    [DataMember(Name = "salaryGross", EmitDefaultValue = false)]
    [JsonPropertyName("salaryGross")]
    public decimal? SalaryGross { get; set; }

    /// <summary>
    ///     Gets or Sets Benefits
    /// </summary>
    [DataMember(Name = "benefits", EmitDefaultValue = false)]
    [JsonPropertyName("benefits")]
    public decimal? Benefits { get; set; }

    /// <summary>
    ///     Gets or Sets Remuneration
    /// </summary>
    [DataMember(Name = "remuneration", EmitDefaultValue = false)]
    [JsonPropertyName("remuneration")]
    public decimal? Remuneration { get; set; }

    /// <summary>
    ///     Gets or Sets Deduction
    /// </summary>
    [DataMember(Name = "deduction", EmitDefaultValue = false)]
    [JsonPropertyName("deduction")]
    public decimal? Deduction { get; set; }

    /// <summary>
    ///     Gets or Sets PreliminaryTax
    /// </summary>
    [DataMember(Name = "preliminaryTax", EmitDefaultValue = false)]
    [JsonPropertyName("preliminaryTax")]
    public decimal? PreliminaryTax { get; set; }

    /// <summary>
    ///     Gets or Sets Rounding
    /// </summary>
    [DataMember(Name = "rounding", EmitDefaultValue = false)]
    [JsonPropertyName("rounding")]
    public decimal? Rounding { get; set; }

    /// <summary>
    ///     Gets or Sets PaidAmount
    /// </summary>
    [DataMember(Name = "paidAmount", EmitDefaultValue = false)]
    [JsonPropertyName("paidAmount")]
    public decimal? PaidAmount { get; set; }

    /// <summary>
    ///     Gets or Sets Holidays
    /// </summary>
    [DataMember(Name = "holidays", EmitDefaultValue = false)]
    [JsonPropertyName("holidays")]
    public Holidays Holidays { get; set; }

    /// <summary>
    ///     Gets or Sets UnionFee
    /// </summary>
    [DataMember(Name = "unionFee", EmitDefaultValue = false)]
    [JsonPropertyName("unionFee")]
    public decimal? UnionFee { get; set; }

    /// <summary>
    ///     Gets or Sets EmployerPayrollTaxPercentage
    /// </summary>
    [DataMember(Name = "employerPayrollTaxPercentage", EmitDefaultValue = false)]
    [JsonPropertyName("employerPayrollTaxPercentage")]
    public double? EmployerPayrollTaxPercentage { get; set; }

    /// <summary>
    ///     Gets or Sets EmployerPayrollTax
    /// </summary>
    [DataMember(Name = "employerPayrollTax", EmitDefaultValue = false)]
    [JsonPropertyName("employerPayrollTax")]
    public decimal? EmployerPayrollTax { get; set; }

    /// <summary>
    ///     Gets or Sets SpecialPayrollTaxPercentage
    /// </summary>
    [DataMember(Name = "specialPayrollTaxPercentage", EmitDefaultValue = false)]
    [JsonPropertyName("specialPayrollTaxPercentage")]
    public double? SpecialPayrollTaxPercentage { get; set; }

    /// <summary>
    ///     Gets or Sets SpecialPayrollTax
    /// </summary>
    [DataMember(Name = "specialPayrollTax", EmitDefaultValue = false)]
    [JsonPropertyName("specialPayrollTax")]
    public decimal? SpecialPayrollTax { get; set; }

    /// <summary>
    ///     Gets or Sets AccumulatedSalary
    /// </summary>
    [DataMember(Name = "accumulatedSalary", EmitDefaultValue = false)]
    [JsonPropertyName("accumulatedSalary")]
    public decimal? AccumulatedSalary { get; set; }

    /// <summary>
    ///     Gets or Sets AccumulatedTax
    /// </summary>
    [DataMember(Name = "accumulatedTax", EmitDefaultValue = false)]
    [JsonPropertyName("accumulatedTax")]
    public decimal? AccumulatedTax { get; set; }

    /// <summary>
    ///     Gets or Sets AccumulatedBenefit
    /// </summary>
    [DataMember(Name = "accumulatedBenefit", EmitDefaultValue = false)]
    [JsonPropertyName("accumulatedBenefit")]
    public decimal? AccumulatedBenefit { get; set; }

    /// <summary>
    ///     Gets or Sets Accumulators
    /// </summary>
    [DataMember(Name = "accumulators", EmitDefaultValue = false)]
    [JsonPropertyName("accumulators")]
    public List<SalarySpecificationAccumulator> Accumulators { get; set; }

    /// <summary>
    ///     Gets or Sets User
    /// </summary>
    [DataMember(Name = "user", EmitDefaultValue = false)]
    [JsonPropertyName("user")]
    public string User { get; set; }

    /// <summary>
    ///     Gets or Sets Preliminary
    /// </summary>
    [DataMember(Name = "preliminary", EmitDefaultValue = false)]
    [JsonPropertyName("preliminary")]
    public bool? Preliminary { get; set; }

    /// <summary>
    ///     Gets or Sets Printed
    /// </summary>
    [DataMember(Name = "printed", EmitDefaultValue = false)]
    [JsonPropertyName("printed")]
    public bool? Printed { get; set; }

    /// <summary>
    ///     Gets or Sets Closed
    /// </summary>
    [DataMember(Name = "closed", EmitDefaultValue = false)]
    [JsonPropertyName("closed")]
    public bool? Closed { get; set; }

    /// <summary>
    ///     Gets or Sets JournalEntryId
    /// </summary>
    [DataMember(Name = "journalEntryId", EmitDefaultValue = false)]
    [JsonPropertyName("journalEntryId")]
    public int? JournalEntryId { get; set; }

    /// <summary>
    ///     Gets or Sets JournalId
    /// </summary>
    [DataMember(Name = "journalId", EmitDefaultValue = false)]
    [JsonPropertyName("journalId")]
    public string JournalId { get; set; }

    /// <summary>
    ///     Gets or Sets JournalEntryDate
    /// </summary>
    [DataMember(Name = "journalEntryDate", EmitDefaultValue = false)]
    [JsonPropertyName("journalEntryDate")]
    public DateTime? JournalEntryDate { get; set; }

    /// <summary>
    ///     Gets or Sets LastUpdate
    /// </summary>
    [DataMember(Name = "lastUpdate", EmitDefaultValue = false)]
    [JsonPropertyName("lastUpdate")]
    public DateTime? LastUpdate { get; set; }

    /// <summary>
    ///     Gets or Sets LastUpdateBy
    /// </summary>
    [DataMember(Name = "lastUpdateBy", EmitDefaultValue = false)]
    [JsonPropertyName("lastUpdateBy")]
    public string LastUpdateBy { get; set; }

    /// <summary>
    ///     Gets or Sets Comment
    /// </summary>
    [DataMember(Name = "comment", EmitDefaultValue = false)]
    [JsonPropertyName("comment")]
    public string Comment { get; set; }

    /// <summary>
    ///     Gets or Sets SalaryPeriodId
    /// </summary>
    [DataMember(Name = "salaryPeriodId", EmitDefaultValue = false)]
    [JsonPropertyName("salaryPeriodId")]
    public int? SalaryPeriodId { get; set; }

    /// <summary>
    ///     Gets or Sets BankAccount
    /// </summary>
    [DataMember(Name = "bankAccount", EmitDefaultValue = false)]
    [JsonPropertyName("bankAccount")]
    public string BankAccount { get; set; }

    /// <summary>
    ///     Gets or Sets Calendar
    /// </summary>
    [DataMember(Name = "calendar", EmitDefaultValue = false)]
    [JsonPropertyName("calendar")]
    public string Calendar { get; set; }

    /// <summary>
    ///     Gets or Sets SpecificEmployerTaxDeduction
    /// </summary>
    [DataMember(Name = "specificEmployerTaxDeduction", EmitDefaultValue = false)]
    [JsonPropertyName("specificEmployerTaxDeduction")]
    public decimal? SpecificEmployerTaxDeduction { get; set; }

    /// <summary>
    ///     Gets or Sets Lines
    /// </summary>
    [DataMember(Name = "lines", EmitDefaultValue = false)]
    [JsonPropertyName("lines")]
    public List<SalarySpecificationLine> Lines { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class SalarySpecification {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Employee: ").Append(Employee).Append("\n");
        sb.Append("  Street: ").Append(Street).Append("\n");
        sb.Append("  Box: ").Append(Box).Append("\n");
        sb.Append("  Zipcode: ").Append(Zipcode).Append("\n");
        sb.Append("  City: ").Append(City).Append("\n");
        sb.Append("  RefersTo: ").Append(RefersTo).Append("\n");
        sb.Append("  PaymentDate: ").Append(PaymentDate).Append("\n");
        sb.Append("  TaxScaleId: ").Append(TaxScaleId).Append("\n");
        sb.Append("  TaxableAmount: ").Append(TaxableAmount).Append("\n");
        sb.Append("  SalaryGross: ").Append(SalaryGross).Append("\n");
        sb.Append("  Benefits: ").Append(Benefits).Append("\n");
        sb.Append("  Remuneration: ").Append(Remuneration).Append("\n");
        sb.Append("  Deduction: ").Append(Deduction).Append("\n");
        sb.Append("  PreliminaryTax: ").Append(PreliminaryTax).Append("\n");
        sb.Append("  Rounding: ").Append(Rounding).Append("\n");
        sb.Append("  PaidAmount: ").Append(PaidAmount).Append("\n");
        sb.Append("  Holidays: ").Append(Holidays).Append("\n");
        sb.Append("  UnionFee: ").Append(UnionFee).Append("\n");
        sb.Append("  EmployerPayrollTaxPercentage: ").Append(EmployerPayrollTaxPercentage).Append("\n");
        sb.Append("  EmployerPayrollTax: ").Append(EmployerPayrollTax).Append("\n");
        sb.Append("  SpecialPayrollTaxPercentage: ").Append(SpecialPayrollTaxPercentage).Append("\n");
        sb.Append("  SpecialPayrollTax: ").Append(SpecialPayrollTax).Append("\n");
        sb.Append("  AccumulatedSalary: ").Append(AccumulatedSalary).Append("\n");
        sb.Append("  AccumulatedTax: ").Append(AccumulatedTax).Append("\n");
        sb.Append("  AccumulatedBenefit: ").Append(AccumulatedBenefit).Append("\n");
        sb.Append("  Accumulators: ").Append(Accumulators).Append("\n");
        sb.Append("  User: ").Append(User).Append("\n");
        sb.Append("  Preliminary: ").Append(Preliminary).Append("\n");
        sb.Append("  Printed: ").Append(Printed).Append("\n");
        sb.Append("  Closed: ").Append(Closed).Append("\n");
        sb.Append("  JournalEntryId: ").Append(JournalEntryId).Append("\n");
        sb.Append("  JournalId: ").Append(JournalId).Append("\n");
        sb.Append("  JournalEntryDate: ").Append(JournalEntryDate).Append("\n");
        sb.Append("  LastUpdate: ").Append(LastUpdate).Append("\n");
        sb.Append("  LastUpdateBy: ").Append(LastUpdateBy).Append("\n");
        sb.Append("  Comment: ").Append(Comment).Append("\n");
        sb.Append("  SalaryPeriodId: ").Append(SalaryPeriodId).Append("\n");
        sb.Append("  BankAccount: ").Append(BankAccount).Append("\n");
        sb.Append("  Calendar: ").Append(Calendar).Append("\n");
        sb.Append("  SpecificEmployerTaxDeduction: ").Append(SpecificEmployerTaxDeduction).Append("\n");
        sb.Append("  Lines: ").Append(Lines).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}