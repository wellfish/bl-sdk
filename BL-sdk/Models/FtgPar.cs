using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class FtgPar
{
    /// <summary>
    ///     Gets or Sets AutoAccounting
    /// </summary>
    [DataMember(Name = "autoAccounting", EmitDefaultValue = false)]
    [JsonPropertyName("autoAccounting")]
    public bool? AutoAccounting { get; set; }

    /// <summary>
    ///     Gets or Sets AutoAccountingSupplier
    /// </summary>
    [DataMember(Name = "autoAccountingSupplier", EmitDefaultValue = false)]
    [JsonPropertyName("autoAccountingSupplier")]
    public bool? AutoAccountingSupplier { get; set; }

    /// <summary>
    ///     Gets or Sets BankingCosts
    /// </summary>
    [DataMember(Name = "bankingCosts", EmitDefaultValue = false)]
    [JsonPropertyName("bankingCosts")]
    public string BankingCosts { get; set; }

    /// <summary>
    ///     Gets or Sets BgAccount
    /// </summary>
    [DataMember(Name = "bgAccount", EmitDefaultValue = false)]
    [JsonPropertyName("bgAccount")]
    public string BgAccount { get; set; }

    /// <summary>
    ///     Gets or Sets Cash
    /// </summary>
    [DataMember(Name = "cash", EmitDefaultValue = false)]
    [JsonPropertyName("cash")]
    public string Cash { get; set; }

    /// <summary>
    ///     Gets or Sets CashAccounting
    /// </summary>
    [DataMember(Name = "cashAccounting", EmitDefaultValue = false)]
    [JsonPropertyName("cashAccounting")]
    public bool? CashAccounting { get; set; }

    /// <summary>
    ///     Gets or Sets CoreBusinessCode
    /// </summary>
    [DataMember(Name = "coreBusinessCode", EmitDefaultValue = false)]
    [JsonPropertyName("coreBusinessCode")]
    public long? CoreBusinessCode { get; set; }

    /// <summary>
    ///     Gets or Sets Currency
    /// </summary>
    [DataMember(Name = "currency", EmitDefaultValue = false)]
    [JsonPropertyName("currency")]
    public string Currency { get; set; }

    /// <summary>
    ///     Gets or Sets CustomerReceivables
    /// </summary>
    [DataMember(Name = "customerReceivables", EmitDefaultValue = false)]
    [JsonPropertyName("customerReceivables")]
    public string CustomerReceivables { get; set; }

    /// <summary>
    ///     Gets or Sets CustomerLosses
    /// </summary>
    [DataMember(Name = "customerLosses", EmitDefaultValue = false)]
    [JsonPropertyName("customerLosses")]
    public string CustomerLosses { get; set; }

    /// <summary>
    ///     Gets or Sets ExpeditionVat1
    /// </summary>
    [DataMember(Name = "expeditionVat1", EmitDefaultValue = false)]
    [JsonPropertyName("expeditionVat1")]
    public string ExpeditionVat1 { get; set; }

    /// <summary>
    ///     Gets or Sets ExpeditionVat2
    /// </summary>
    [DataMember(Name = "expeditionVat2", EmitDefaultValue = false)]
    [JsonPropertyName("expeditionVat2")]
    public string ExpeditionVat2 { get; set; }

    /// <summary>
    ///     Gets or Sets ExpeditionVat3
    /// </summary>
    [DataMember(Name = "expeditionVat3", EmitDefaultValue = false)]
    [JsonPropertyName("expeditionVat3")]
    public string ExpeditionVat3 { get; set; }

    /// <summary>
    ///     Gets or Sets HasArticleSalesAccountPriority
    /// </summary>
    [DataMember(Name = "hasArticleSalesAccountPriority", EmitDefaultValue = false)]
    [JsonPropertyName("hasArticleSalesAccountPriority")]
    public bool? HasArticleSalesAccountPriority { get; set; }

    /// <summary>
    ///     Gets or Sets IsTaxDeductionActive
    /// </summary>
    [DataMember(Name = "isTaxDeductionActive", EmitDefaultValue = false)]
    [JsonPropertyName("isTaxDeductionActive")]
    public bool? IsTaxDeductionActive { get; set; }

    /// <summary>
    ///     Gets or Sets NextCustomerNumber
    /// </summary>
    [DataMember(Name = "nextCustomerNumber", EmitDefaultValue = false)]
    [JsonPropertyName("nextCustomerNumber")]
    public string NextCustomerNumber { get; set; }

    /// <summary>
    ///     Gets or Sets NextInvoiceNumber
    /// </summary>
    [DataMember(Name = "nextInvoiceNumber", EmitDefaultValue = false)]
    [JsonPropertyName("nextInvoiceNumber")]
    public long? NextInvoiceNumber { get; set; }

    /// <summary>
    ///     Gets or Sets NextAgreementNumber
    /// </summary>
    [DataMember(Name = "nextAgreementNumber", EmitDefaultValue = false)]
    [JsonPropertyName("nextAgreementNumber")]
    public long? NextAgreementNumber { get; set; }

    /// <summary>
    ///     Gets or Sets NextOrderNumber
    /// </summary>
    [DataMember(Name = "nextOrderNumber", EmitDefaultValue = false)]
    [JsonPropertyName("nextOrderNumber")]
    public long? NextOrderNumber { get; set; }

    /// <summary>
    ///     Gets or Sets NextSupplierInvoiceNumber
    /// </summary>
    [DataMember(Name = "nextSupplierInvoiceNumber", EmitDefaultValue = false)]
    [JsonPropertyName("nextSupplierInvoiceNumber")]
    public long? NextSupplierInvoiceNumber { get; set; }

    /// <summary>
    ///     Gets or Sets NextSupplierNumber
    /// </summary>
    [DataMember(Name = "nextSupplierNumber", EmitDefaultValue = false)]
    [JsonPropertyName("nextSupplierNumber")]
    public string NextSupplierNumber { get; set; }

    /// <summary>
    ///     Gets or Sets ObsAccount
    /// </summary>
    [DataMember(Name = "obsAccount", EmitDefaultValue = false)]
    [JsonPropertyName("obsAccount")]
    public string ObsAccount { get; set; }

    /// <summary>
    ///     Gets or Sets OurReference
    /// </summary>
    [DataMember(Name = "ourReference", EmitDefaultValue = false)]
    [JsonPropertyName("ourReference")]
    public string OurReference { get; set; }

    /// <summary>
    ///     Gets or Sets PaymentAccount
    /// </summary>
    [DataMember(Name = "paymentAccount", EmitDefaultValue = false)]
    [JsonPropertyName("paymentAccount")]
    public string PaymentAccount { get; set; }

    /// <summary>
    ///     Gets or Sets PaymentTerms
    /// </summary>
    [DataMember(Name = "paymentTerms", EmitDefaultValue = false)]
    [JsonPropertyName("paymentTerms")]
    public string PaymentTerms { get; set; }

    /// <summary>
    ///     Gets or Sets PaymentTermsSupplier
    /// </summary>
    [DataMember(Name = "paymentTermsSupplier", EmitDefaultValue = false)]
    [JsonPropertyName("paymentTermsSupplier")]
    public string PaymentTermsSupplier { get; set; }

    /// <summary>
    ///     Gets or Sets PenaltyInterest
    /// </summary>
    [DataMember(Name = "penaltyInterest", EmitDefaultValue = false)]
    [JsonPropertyName("penaltyInterest")]
    public string PenaltyInterest { get; set; }

    /// <summary>
    ///     Gets or Sets PgAccount
    /// </summary>
    [DataMember(Name = "pgAccount", EmitDefaultValue = false)]
    [JsonPropertyName("pgAccount")]
    public string PgAccount { get; set; }

    /// <summary>
    ///     Gets or Sets ReminderFee
    /// </summary>
    [DataMember(Name = "reminderFee", EmitDefaultValue = false)]
    [JsonPropertyName("reminderFee")]
    public double? ReminderFee { get; set; }

    /// <summary>
    ///     Gets or Sets Rounding
    /// </summary>
    [DataMember(Name = "rounding", EmitDefaultValue = false)]
    [JsonPropertyName("rounding")]
    public string Rounding { get; set; }

    /// <summary>
    ///     Gets or Sets RoundingSetting
    /// </summary>
    [DataMember(Name = "roundingSetting", EmitDefaultValue = false)]
    [JsonPropertyName("roundingSetting")]
    public bool? RoundingSetting { get; set; }

    /// <summary>
    ///     Gets or Sets SalesAccountVat1
    /// </summary>
    [DataMember(Name = "salesAccountVat1", EmitDefaultValue = false)]
    [JsonPropertyName("salesAccountVat1")]
    public string SalesAccountVat1 { get; set; }

    /// <summary>
    ///     Gets or Sets SalesAccountVat2
    /// </summary>
    [DataMember(Name = "salesAccountVat2", EmitDefaultValue = false)]
    [JsonPropertyName("salesAccountVat2")]
    public string SalesAccountVat2 { get; set; }

    /// <summary>
    ///     Gets or Sets SalesAccountVat3
    /// </summary>
    [DataMember(Name = "salesAccountVat3", EmitDefaultValue = false)]
    [JsonPropertyName("salesAccountVat3")]
    public string SalesAccountVat3 { get; set; }

    /// <summary>
    ///     Gets or Sets SalesAccountNoVat
    /// </summary>
    [DataMember(Name = "salesAccountNoVat", EmitDefaultValue = false)]
    [JsonPropertyName("salesAccountNoVat")]
    public string SalesAccountNoVat { get; set; }

    /// <summary>
    ///     Gets or Sets ReverseSalesAccount
    /// </summary>
    [DataMember(Name = "reverseSalesAccount", EmitDefaultValue = false)]
    [JsonPropertyName("reverseSalesAccount")]
    public string ReverseSalesAccount { get; set; }

    /// <summary>
    ///     Gets or Sets ShippingVat1
    /// </summary>
    [DataMember(Name = "shippingVat1", EmitDefaultValue = false)]
    [JsonPropertyName("shippingVat1")]
    public string ShippingVat1 { get; set; }

    /// <summary>
    ///     Gets or Sets ShippingVat2
    /// </summary>
    [DataMember(Name = "shippingVat2", EmitDefaultValue = false)]
    [JsonPropertyName("shippingVat2")]
    public string ShippingVat2 { get; set; }

    /// <summary>
    ///     Gets or Sets ShippingVat3
    /// </summary>
    [DataMember(Name = "shippingVat3", EmitDefaultValue = false)]
    [JsonPropertyName("shippingVat3")]
    public string ShippingVat3 { get; set; }

    /// <summary>
    ///     Gets or Sets SupplierPayable
    /// </summary>
    [DataMember(Name = "supplierPayable", EmitDefaultValue = false)]
    [JsonPropertyName("supplierPayable")]
    public string SupplierPayable { get; set; }

    /// <summary>
    ///     Gets or Sets UnattestedSupplierInvoice
    /// </summary>
    [DataMember(Name = "unattestedSupplierInvoice", EmitDefaultValue = false)]
    [JsonPropertyName("unattestedSupplierInvoice")]
    public string UnattestedSupplierInvoice { get; set; }

    /// <summary>
    ///     Gets or Sets ValidPaymentAccounts
    /// </summary>
    [DataMember(Name = "validPaymentAccounts", EmitDefaultValue = false)]
    [JsonPropertyName("validPaymentAccounts")]
    public List<Dictionary<string, object>> ValidPaymentAccounts { get; set; }

    /// <summary>
    ///     Gets or Sets EntityId
    /// </summary>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets DefaultSupplierInvoicePaymentAccount
    /// </summary>
    [DataMember(Name = "defaultSupplierInvoicePaymentAccount", EmitDefaultValue = false)]
    [JsonPropertyName("defaultSupplierInvoicePaymentAccount")]
    public string DefaultSupplierInvoicePaymentAccount { get; set; }

    /// <summary>
    ///     Gets or Sets SupplierInvoiceBgAccount
    /// </summary>
    [DataMember(Name = "supplierInvoiceBgAccount", EmitDefaultValue = false)]
    [JsonPropertyName("supplierInvoiceBgAccount")]
    public string SupplierInvoiceBgAccount { get; set; }

    /// <summary>
    ///     Gets or Sets SupplierInvoicePgAccount
    /// </summary>
    [DataMember(Name = "supplierInvoicePgAccount", EmitDefaultValue = false)]
    [JsonPropertyName("supplierInvoicePgAccount")]
    public string SupplierInvoicePgAccount { get; set; }

    /// <summary>
    ///     Gets or Sets ProductChargeAdjustment
    /// </summary>
    [DataMember(Name = "productChargeAdjustment", EmitDefaultValue = false)]
    [JsonPropertyName("productChargeAdjustment")]
    public bool? ProductChargeAdjustment { get; set; }

    /// <summary>
    ///     Gets or Sets CostPrice
    /// </summary>
    [DataMember(Name = "costPrice", EmitDefaultValue = false)]
    [JsonPropertyName("costPrice")]
    public bool? CostPrice { get; set; }

    /// <summary>
    ///     Gets or Sets WeightedCostPrice
    /// </summary>
    [DataMember(Name = "weightedCostPrice", EmitDefaultValue = false)]
    [JsonPropertyName("weightedCostPrice")]
    public bool? WeightedCostPrice { get; set; }

    /// <summary>
    ///     Gets or Sets ComputingPrice
    /// </summary>
    [DataMember(Name = "computingPrice", EmitDefaultValue = false)]
    [JsonPropertyName("computingPrice")]
    public bool? ComputingPrice { get; set; }

    /// <summary>
    ///     Gets or Sets StockAccount
    /// </summary>
    [DataMember(Name = "stockAccount", EmitDefaultValue = false)]
    [JsonPropertyName("stockAccount")]
    public string StockAccount { get; set; }

    /// <summary>
    ///     Gets or Sets StockCostAccount
    /// </summary>
    [DataMember(Name = "stockCostAccount", EmitDefaultValue = false)]
    [JsonPropertyName("stockCostAccount")]
    public string StockCostAccount { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class FtgPar {\n");
        sb.Append("  AutoAccounting: ").Append(AutoAccounting).Append("\n");
        sb.Append("  AutoAccountingSupplier: ").Append(AutoAccountingSupplier).Append("\n");
        sb.Append("  BankingCosts: ").Append(BankingCosts).Append("\n");
        sb.Append("  BgAccount: ").Append(BgAccount).Append("\n");
        sb.Append("  Cash: ").Append(Cash).Append("\n");
        sb.Append("  CashAccounting: ").Append(CashAccounting).Append("\n");
        sb.Append("  CoreBusinessCode: ").Append(CoreBusinessCode).Append("\n");
        sb.Append("  Currency: ").Append(Currency).Append("\n");
        sb.Append("  CustomerReceivables: ").Append(CustomerReceivables).Append("\n");
        sb.Append("  CustomerLosses: ").Append(CustomerLosses).Append("\n");
        sb.Append("  ExpeditionVat1: ").Append(ExpeditionVat1).Append("\n");
        sb.Append("  ExpeditionVat2: ").Append(ExpeditionVat2).Append("\n");
        sb.Append("  ExpeditionVat3: ").Append(ExpeditionVat3).Append("\n");
        sb.Append("  HasArticleSalesAccountPriority: ").Append(HasArticleSalesAccountPriority).Append("\n");
        sb.Append("  IsTaxDeductionActive: ").Append(IsTaxDeductionActive).Append("\n");
        sb.Append("  NextCustomerNumber: ").Append(NextCustomerNumber).Append("\n");
        sb.Append("  NextInvoiceNumber: ").Append(NextInvoiceNumber).Append("\n");
        sb.Append("  NextAgreementNumber: ").Append(NextAgreementNumber).Append("\n");
        sb.Append("  NextOrderNumber: ").Append(NextOrderNumber).Append("\n");
        sb.Append("  NextSupplierInvoiceNumber: ").Append(NextSupplierInvoiceNumber).Append("\n");
        sb.Append("  NextSupplierNumber: ").Append(NextSupplierNumber).Append("\n");
        sb.Append("  ObsAccount: ").Append(ObsAccount).Append("\n");
        sb.Append("  OurReference: ").Append(OurReference).Append("\n");
        sb.Append("  PaymentAccount: ").Append(PaymentAccount).Append("\n");
        sb.Append("  PaymentTerms: ").Append(PaymentTerms).Append("\n");
        sb.Append("  PaymentTermsSupplier: ").Append(PaymentTermsSupplier).Append("\n");
        sb.Append("  PenaltyInterest: ").Append(PenaltyInterest).Append("\n");
        sb.Append("  PgAccount: ").Append(PgAccount).Append("\n");
        sb.Append("  ReminderFee: ").Append(ReminderFee).Append("\n");
        sb.Append("  Rounding: ").Append(Rounding).Append("\n");
        sb.Append("  RoundingSetting: ").Append(RoundingSetting).Append("\n");
        sb.Append("  SalesAccountVat1: ").Append(SalesAccountVat1).Append("\n");
        sb.Append("  SalesAccountVat2: ").Append(SalesAccountVat2).Append("\n");
        sb.Append("  SalesAccountVat3: ").Append(SalesAccountVat3).Append("\n");
        sb.Append("  SalesAccountNoVat: ").Append(SalesAccountNoVat).Append("\n");
        sb.Append("  ReverseSalesAccount: ").Append(ReverseSalesAccount).Append("\n");
        sb.Append("  ShippingVat1: ").Append(ShippingVat1).Append("\n");
        sb.Append("  ShippingVat2: ").Append(ShippingVat2).Append("\n");
        sb.Append("  ShippingVat3: ").Append(ShippingVat3).Append("\n");
        sb.Append("  SupplierPayable: ").Append(SupplierPayable).Append("\n");
        sb.Append("  UnattestedSupplierInvoice: ").Append(UnattestedSupplierInvoice).Append("\n");
        sb.Append("  ValidPaymentAccounts: ").Append(ValidPaymentAccounts).Append("\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  DefaultSupplierInvoicePaymentAccount: ").Append(DefaultSupplierInvoicePaymentAccount).Append("\n");
        sb.Append("  SupplierInvoiceBgAccount: ").Append(SupplierInvoiceBgAccount).Append("\n");
        sb.Append("  SupplierInvoicePgAccount: ").Append(SupplierInvoicePgAccount).Append("\n");
        sb.Append("  ProductChargeAdjustment: ").Append(ProductChargeAdjustment).Append("\n");
        sb.Append("  CostPrice: ").Append(CostPrice).Append("\n");
        sb.Append("  WeightedCostPrice: ").Append(WeightedCostPrice).Append("\n");
        sb.Append("  ComputingPrice: ").Append(ComputingPrice).Append("\n");
        sb.Append("  StockAccount: ").Append(StockAccount).Append("\n");
        sb.Append("  StockCostAccount: ").Append(StockCostAccount).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}