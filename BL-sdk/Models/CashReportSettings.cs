using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class CashReportSettings
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Id of the report
    /// </summary>
    /// <value>Id of the report</value>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public long? Id { get; set; }

    /// <summary>
    ///     Type of report setting
    /// </summary>
    /// <value>Type of report setting</value>
    [DataMember(Name = "type", EmitDefaultValue = false)]
    [JsonPropertyName("type")]
    public string Type { get; set; }

    /// <summary>
    ///     Gets or Sets Text
    /// </summary>
    [DataMember(Name = "text", EmitDefaultValue = false)]
    [JsonPropertyName("text")]
    public string Text { get; set; }

    /// <summary>
    ///     Gets or Sets Account
    /// </summary>
    [DataMember(Name = "account", EmitDefaultValue = false)]
    [JsonPropertyName("account")]
    public string Account { get; set; }

    /// <summary>
    ///     Gets or Sets CostCenter
    /// </summary>
    [DataMember(Name = "costCenter", EmitDefaultValue = false)]
    [JsonPropertyName("costCenter")]
    public string CostCenter { get; set; }

    /// <summary>
    ///     Gets or Sets CostBearer
    /// </summary>
    [DataMember(Name = "costBearer", EmitDefaultValue = false)]
    [JsonPropertyName("costBearer")]
    public string CostBearer { get; set; }

    /// <summary>
    ///     Gets or Sets Project
    /// </summary>
    [DataMember(Name = "project", EmitDefaultValue = false)]
    [JsonPropertyName("project")]
    public string Project { get; set; }

    /// <summary>
    ///     Gets or Sets VatPercent
    /// </summary>
    [DataMember(Name = "vatPercent", EmitDefaultValue = false)]
    [JsonPropertyName("vatPercent")]
    public double? VatPercent { get; set; }

    /// <summary>
    ///     Gets or Sets VatAmount
    /// </summary>
    [DataMember(Name = "vatAmount", EmitDefaultValue = false)]
    [JsonPropertyName("vatAmount")]
    public decimal? VatAmount { get; set; }

    /// <summary>
    ///     Gets or Sets Amount
    /// </summary>
    [DataMember(Name = "amount", EmitDefaultValue = false)]
    [JsonPropertyName("amount")]
    public decimal? Amount { get; set; }

    /// <summary>
    ///     Gets or Sets Date
    /// </summary>
    [DataMember(Name = "date", EmitDefaultValue = false)]
    [JsonPropertyName("date")]
    public DateTime? Date { get; set; }

    /// <summary>
    ///     Gets or Sets LastUpdate
    /// </summary>
    [DataMember(Name = "lastUpdate", EmitDefaultValue = false)]
    [JsonPropertyName("lastUpdate")]
    public DateTime? LastUpdate { get; set; }

    /// <summary>
    ///     Gets or Sets LastUpdatedBy
    /// </summary>
    [DataMember(Name = "lastUpdatedBy", EmitDefaultValue = false)]
    [JsonPropertyName("lastUpdatedBy")]
    public string LastUpdatedBy { get; set; }

    /// <summary>
    ///     Gets or Sets Locked
    /// </summary>
    [DataMember(Name = "locked", EmitDefaultValue = false)]
    [JsonPropertyName("locked")]
    public bool? Locked { get; set; }

    /// <summary>
    ///     Gets or Sets ExcludeVat
    /// </summary>
    [DataMember(Name = "excludeVat", EmitDefaultValue = false)]
    [JsonPropertyName("excludeVat")]
    public bool? ExcludeVat { get; set; }

    /// <summary>
    ///     Gets or Sets EditableText
    /// </summary>
    [DataMember(Name = "editableText", EmitDefaultValue = false)]
    [JsonPropertyName("editableText")]
    public bool? EditableText { get; set; }

    /// <summary>
    ///     Gets or Sets AllowCcAccounting
    /// </summary>
    [DataMember(Name = "allowCcAccounting", EmitDefaultValue = false)]
    [JsonPropertyName("allowCcAccounting")]
    public bool? AllowCcAccounting { get; set; }

    /// <summary>
    ///     Gets or Sets AllowCbAccounting
    /// </summary>
    [DataMember(Name = "allowCbAccounting", EmitDefaultValue = false)]
    [JsonPropertyName("allowCbAccounting")]
    public bool? AllowCbAccounting { get; set; }

    /// <summary>
    ///     Gets or Sets AllowProjectAccounting
    /// </summary>
    [DataMember(Name = "allowProjectAccounting", EmitDefaultValue = false)]
    [JsonPropertyName("allowProjectAccounting")]
    public bool? AllowProjectAccounting { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class CashReportSettings {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Type: ").Append(Type).Append("\n");
        sb.Append("  Text: ").Append(Text).Append("\n");
        sb.Append("  Account: ").Append(Account).Append("\n");
        sb.Append("  CostCenter: ").Append(CostCenter).Append("\n");
        sb.Append("  CostBearer: ").Append(CostBearer).Append("\n");
        sb.Append("  Project: ").Append(Project).Append("\n");
        sb.Append("  VatPercent: ").Append(VatPercent).Append("\n");
        sb.Append("  VatAmount: ").Append(VatAmount).Append("\n");
        sb.Append("  Amount: ").Append(Amount).Append("\n");
        sb.Append("  Date: ").Append(Date).Append("\n");
        sb.Append("  LastUpdate: ").Append(LastUpdate).Append("\n");
        sb.Append("  LastUpdatedBy: ").Append(LastUpdatedBy).Append("\n");
        sb.Append("  Locked: ").Append(Locked).Append("\n");
        sb.Append("  ExcludeVat: ").Append(ExcludeVat).Append("\n");
        sb.Append("  EditableText: ").Append(EditableText).Append("\n");
        sb.Append("  AllowCcAccounting: ").Append(AllowCcAccounting).Append("\n");
        sb.Append("  AllowCbAccounting: ").Append(AllowCbAccounting).Append("\n");
        sb.Append("  AllowProjectAccounting: ").Append(AllowProjectAccounting).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}