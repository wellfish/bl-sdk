using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class CostCenterBalance
{
    /// <summary>
    ///     Gets or Sets Type
    /// </summary>
    [DataMember(Name = "type", EmitDefaultValue = false)]
    [JsonPropertyName("type")]
    public string Type { get; set; }

    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    /// <summary>
    ///     Gets or Sets Name
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     Gets or Sets Account
    /// </summary>
    [DataMember(Name = "account", EmitDefaultValue = false)]
    [JsonPropertyName("account")]
    public string Account { get; set; }

    /// <summary>
    ///     Gets or Sets Period
    /// </summary>
    [DataMember(Name = "period", EmitDefaultValue = false)]
    [JsonPropertyName("period")]
    public string Period { get; set; }

    /// <summary>
    ///     Gets or Sets Eoy
    /// </summary>
    [DataMember(Name = "eoy", EmitDefaultValue = false)]
    [JsonPropertyName("eoy")]
    public string Eoy { get; set; }

    /// <summary>
    ///     Gets or Sets OpeningBalance
    /// </summary>
    [DataMember(Name = "openingBalance", EmitDefaultValue = false)]
    [JsonPropertyName("openingBalance")]
    public decimal? OpeningBalance { get; set; }

    /// <summary>
    ///     Gets or Sets Balance
    /// </summary>
    [DataMember(Name = "balance", EmitDefaultValue = false)]
    [JsonPropertyName("balance")]
    public decimal? Balance { get; set; }

    /// <summary>
    ///     Gets or Sets AccumulatedBalance
    /// </summary>
    [DataMember(Name = "accumulatedBalance", EmitDefaultValue = false)]
    [JsonPropertyName("accumulatedBalance")]
    public decimal? AccumulatedBalance { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class CostCenterBalance {\n");
        sb.Append("  Type: ").Append(Type).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  Account: ").Append(Account).Append("\n");
        sb.Append("  Period: ").Append(Period).Append("\n");
        sb.Append("  Eoy: ").Append(Eoy).Append("\n");
        sb.Append("  OpeningBalance: ").Append(OpeningBalance).Append("\n");
        sb.Append("  Balance: ").Append(Balance).Append("\n");
        sb.Append("  AccumulatedBalance: ").Append(AccumulatedBalance).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}