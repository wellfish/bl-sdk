using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class FinancialYear
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    /// <summary>
    ///     Gets or Sets FromDate
    /// </summary>
    [DataMember(Name = "fromDate", EmitDefaultValue = false)]
    [JsonPropertyName("fromDate")]
    public DateTime FromDate { get; set; }

    /// <summary>
    ///     Gets or Sets ToDate
    /// </summary>
    [DataMember(Name = "toDate", EmitDefaultValue = false)]
    [JsonPropertyName("toDate")]
    public DateTime ToDate { get; set; }

    /// <summary>
    ///     Gets or Sets LatestVoucherDate
    /// </summary>
    [DataMember(Name = "latestVoucherDate", EmitDefaultValue = false)]
    [JsonPropertyName("latestVoucherDate")]
    public DateTime? LatestVoucherDate { get; set; }

    /// <summary>
    ///     Gets or Sets FirstOpenPeriod
    /// </summary>
    [DataMember(Name = "firstOpenPeriod", EmitDefaultValue = false)]
    [JsonPropertyName("firstOpenPeriod")]
    public string FirstOpenPeriod { get; set; }

    /// <summary>
    ///     Gets or Sets Open
    /// </summary>
    [DataMember(Name = "open", EmitDefaultValue = false)]
    [JsonPropertyName("open")]
    public bool? Open { get; set; }

    /// <summary>
    ///     Gets or Sets OpeningBalanceClosed
    /// </summary>
    [DataMember(Name = "openingBalanceClosed", EmitDefaultValue = false)]
    [JsonPropertyName("openingBalanceClosed")]
    public bool? OpeningBalanceClosed { get; set; }

    /// <summary>
    ///     Gets or Sets ClosingActive
    /// </summary>
    [DataMember(Name = "closingActive", EmitDefaultValue = false)]
    [JsonPropertyName("closingActive")]
    public bool? ClosingActive { get; set; }

    /// <summary>
    ///     Gets or Sets BlockFollowingOpeningBalance
    /// </summary>
    [DataMember(Name = "blockFollowingOpeningBalance", EmitDefaultValue = false)]
    [JsonPropertyName("blockFollowingOpeningBalance")]
    public bool? BlockFollowingOpeningBalance { get; set; }

    /// <summary>
    ///     Gets or Sets Comment
    /// </summary>
    [DataMember(Name = "comment", EmitDefaultValue = false)]
    [JsonPropertyName("comment")]
    public string Comment { get; set; }
}