using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Balance
{
    /// <summary>
    ///     Gets or Sets BalanceType
    /// </summary>
    [DataMember(Name = "balanceType", EmitDefaultValue = false)]
    [JsonPropertyName("balanceType")]
    public string BalanceType { get; set; }

    /// <summary>
    ///     Gets or Sets ClearingNumber
    /// </summary>
    [DataMember(Name = "clearingNumber", EmitDefaultValue = false)]
    [JsonPropertyName("clearingNumber")]
    public string ClearingNumber { get; set; }

    /// <summary>
    ///     Gets or Sets ReferenceDate
    /// </summary>
    [DataMember(Name = "referenceDate", EmitDefaultValue = false)]
    [JsonPropertyName("referenceDate")]
    public DateTime? ReferenceDate { get; set; }

    /// <summary>
    ///     Gets or Sets LastChangeDateTime
    /// </summary>
    [DataMember(Name = "lastChangeDateTime", EmitDefaultValue = false)]
    [JsonPropertyName("lastChangeDateTime")]
    public DateTime? LastChangeDateTime { get; set; }

    /// <summary>
    ///     Gets or Sets BalanceAmount
    /// </summary>
    [DataMember(Name = "balanceAmount", EmitDefaultValue = false)]
    [JsonPropertyName("balanceAmount")]
    public Amount BalanceAmount { get; set; }

    /// <summary>
    ///     Gets or Sets CreditLimitIncluded
    /// </summary>
    [DataMember(Name = "creditLimitIncluded", EmitDefaultValue = false)]
    [JsonPropertyName("creditLimitIncluded")]
    public bool? CreditLimitIncluded { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Balance {\n");
        sb.Append("  BalanceType: ").Append(BalanceType).Append("\n");
        sb.Append("  ClearingNumber: ").Append(ClearingNumber).Append("\n");
        sb.Append("  ReferenceDate: ").Append(ReferenceDate).Append("\n");
        sb.Append("  LastChangeDateTime: ").Append(LastChangeDateTime).Append("\n");
        sb.Append("  BalanceAmount: ").Append(BalanceAmount).Append("\n");
        sb.Append("  CreditLimitIncluded: ").Append(CreditLimitIncluded).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}