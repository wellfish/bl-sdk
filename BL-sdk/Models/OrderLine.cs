using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
///     ___
/// </summary>
[DataContract]
public sealed class OrderLine
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "accountId", EmitDefaultValue = false)]
    [JsonPropertyName("accountId")]
    public string AccountId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "articleId", EmitDefaultValue = false)]
    [JsonPropertyName("articleId")]
    public string ArticleId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "comment", EmitDefaultValue = false)]
    [JsonPropertyName("comment")]
    public string Comment { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "contributionMarginAmount", EmitDefaultValue = false)]
    [JsonPropertyName("contributionMarginAmount")]
    public decimal? ContributionMarginAmount { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "contributionMarginPercent", EmitDefaultValue = false)]
    [JsonPropertyName("contributionMarginPercent")]
    public double? ContributionMarginPercent { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "costBearerId", EmitDefaultValue = false)]
    [JsonPropertyName("costBearerId")]
    public string CostBearerId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "costCenterId", EmitDefaultValue = false)]
    [JsonPropertyName("costCenterId")]
    public string CostCenterId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "deliveredQuantity", EmitDefaultValue = false)]
    [JsonPropertyName("deliveredQuantity")]
    public double? DeliveredQuantity { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "description", EmitDefaultValue = false)]
    [JsonPropertyName("description")]
    public string Description { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "discountAmount", EmitDefaultValue = false)]
    [JsonPropertyName("discountAmount")]
    public decimal? DiscountAmount { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "discountPercentage", EmitDefaultValue = false)]
    [JsonPropertyName("discountPercentage")]
    public double? DiscountPercentage { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "grossPrice", EmitDefaultValue = false)]
    [JsonPropertyName("grossPrice")]
    public decimal? GrossPrice { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "netPrice", EmitDefaultValue = false)]
    [JsonPropertyName("netPrice")]
    public decimal? NetPrice { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "orderedQuantity", EmitDefaultValue = false)]
    [JsonPropertyName("orderedQuantity")]
    public double? OrderedQuantity { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "projectId", EmitDefaultValue = false)]
    [JsonPropertyName("projectId")]
    public string ProjectId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "purchasePrice", EmitDefaultValue = false)]
    [JsonPropertyName("purchasePrice")]
    public decimal? PurchasePrice { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "registeredDate", EmitDefaultValue = false)]
    [JsonPropertyName("registeredDate")]
    public DateTime? RegisteredDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "rowNumber", EmitDefaultValue = false)]
    [JsonPropertyName("rowNumber")]
    public int? RowNumber { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "taxDeductionCode", EmitDefaultValue = false)]
    [JsonPropertyName("taxDeductionCode")]
    public int? TaxDeductionCode { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "totalPriceIncVat", EmitDefaultValue = false)]
    [JsonPropertyName("totalPriceIncVat")]
    public decimal? TotalPriceIncVat { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "unit", EmitDefaultValue = false)]
    [JsonPropertyName("unit")]
    public string Unit { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "unitPrice", EmitDefaultValue = false)]
    [JsonPropertyName("unitPrice")]
    public decimal? UnitPrice { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "unitPriceIncVat", EmitDefaultValue = false)]
    [JsonPropertyName("unitPriceIncVat")]
    public decimal? UnitPriceIncVat { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "vatCode", EmitDefaultValue = false)]
    [JsonPropertyName("vatCode")]
    public string VatCode { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "vatPrice", EmitDefaultValue = false)]
    [JsonPropertyName("vatPrice")]
    public decimal? VatPrice { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "volume", EmitDefaultValue = false)]
    [JsonPropertyName("volume")]
    public double? Volume { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "weight", EmitDefaultValue = false)]
    [JsonPropertyName("weight")]
    public double? Weight { get; set; }

    /// <summary>
    ///     Gets or Sets OrderEntityId
    /// </summary>
    [DataMember(Name = "orderEntityId", EmitDefaultValue = false)]
    [JsonPropertyName("orderEntityId")]
    public long? OrderEntityId { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class OrderLine {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  AccountId: ").Append(AccountId).Append("\n");
        sb.Append("  ArticleId: ").Append(ArticleId).Append("\n");
        sb.Append("  Comment: ").Append(Comment).Append("\n");
        sb.Append("  ContributionMarginAmount: ").Append(ContributionMarginAmount).Append("\n");
        sb.Append("  ContributionMarginPercent: ").Append(ContributionMarginPercent).Append("\n");
        sb.Append("  CostBearerId: ").Append(CostBearerId).Append("\n");
        sb.Append("  CostCenterId: ").Append(CostCenterId).Append("\n");
        sb.Append("  DeliveredQuantity: ").Append(DeliveredQuantity).Append("\n");
        sb.Append("  Description: ").Append(Description).Append("\n");
        sb.Append("  DiscountAmount: ").Append(DiscountAmount).Append("\n");
        sb.Append("  DiscountPercentage: ").Append(DiscountPercentage).Append("\n");
        sb.Append("  GrossPrice: ").Append(GrossPrice).Append("\n");
        sb.Append("  NetPrice: ").Append(NetPrice).Append("\n");
        sb.Append("  OrderedQuantity: ").Append(OrderedQuantity).Append("\n");
        sb.Append("  ProjectId: ").Append(ProjectId).Append("\n");
        sb.Append("  PurchasePrice: ").Append(PurchasePrice).Append("\n");
        sb.Append("  RegisteredDate: ").Append(RegisteredDate).Append("\n");
        sb.Append("  RowNumber: ").Append(RowNumber).Append("\n");
        sb.Append("  TaxDeductionCode: ").Append(TaxDeductionCode).Append("\n");
        sb.Append("  TotalPriceIncVat: ").Append(TotalPriceIncVat).Append("\n");
        sb.Append("  Unit: ").Append(Unit).Append("\n");
        sb.Append("  UnitPrice: ").Append(UnitPrice).Append("\n");
        sb.Append("  UnitPriceIncVat: ").Append(UnitPriceIncVat).Append("\n");
        sb.Append("  VatCode: ").Append(VatCode).Append("\n");
        sb.Append("  VatPrice: ").Append(VatPrice).Append("\n");
        sb.Append("  Volume: ").Append(Volume).Append("\n");
        sb.Append("  Weight: ").Append(Weight).Append("\n");
        sb.Append("  OrderEntityId: ").Append(OrderEntityId).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}