using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Details
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets Auditor
    /// </summary>
    [DataMember(Name = "auditor", EmitDefaultValue = false)]
    [JsonPropertyName("auditor")]
    public string Auditor { get; set; }

    /// <summary>
    ///     Gets or Sets Box
    /// </summary>
    [DataMember(Name = "box", EmitDefaultValue = false)]
    [JsonPropertyName("box")]
    public string Box { get; set; }

    /// <summary>
    ///     Gets or Sets Category
    /// </summary>
    [DataMember(Name = "category", EmitDefaultValue = false)]
    [JsonPropertyName("category")]
    public string Category { get; set; }

    /// <summary>
    ///     Gets or Sets City
    /// </summary>
    [DataMember(Name = "city", EmitDefaultValue = false)]
    [JsonPropertyName("city")]
    public string City { get; set; }

    /// <summary>
    ///     Gets or Sets Comment
    /// </summary>
    [DataMember(Name = "comment", EmitDefaultValue = false)]
    [JsonPropertyName("comment")]
    public string Comment { get; set; }

    /// <summary>
    ///     Gets or Sets Country
    /// </summary>
    [DataMember(Name = "country", EmitDefaultValue = false)]
    [JsonPropertyName("country")]
    public string Country { get; set; }

    /// <summary>
    ///     Gets or Sets Email
    /// </summary>
    [DataMember(Name = "email", EmitDefaultValue = false)]
    [JsonPropertyName("email")]
    public string Email { get; set; }

    /// <summary>
    ///     Gets or Sets Fax
    /// </summary>
    [DataMember(Name = "fax", EmitDefaultValue = false)]
    [JsonPropertyName("fax")]
    public string Fax { get; set; }

    /// <summary>
    ///     Gets or Sets Mobile
    /// </summary>
    [DataMember(Name = "mobile", EmitDefaultValue = false)]
    [JsonPropertyName("mobile")]
    public string Mobile { get; set; }

    /// <summary>
    ///     Gets or Sets Name
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     Gets or Sets OrgNumber
    /// </summary>
    [DataMember(Name = "orgNumber", EmitDefaultValue = false)]
    [JsonPropertyName("orgNumber")]
    public string OrgNumber { get; set; }

    /// <summary>
    ///     Gets or Sets EnrichedData
    /// </summary>
    [DataMember(Name = "enrichedData", EmitDefaultValue = false)]
    [JsonPropertyName("enrichedData")]
    public Dictionary<string, object> EnrichedData { get; set; }

    /// <summary>
    ///     Gets or Sets Phone
    /// </summary>
    [DataMember(Name = "phone", EmitDefaultValue = false)]
    [JsonPropertyName("phone")]
    public string Phone { get; set; }

    /// <summary>
    ///     Gets or Sets Street
    /// </summary>
    [DataMember(Name = "street", EmitDefaultValue = false)]
    [JsonPropertyName("street")]
    public string Street { get; set; }

    /// <summary>
    ///     Gets or Sets Web
    /// </summary>
    [DataMember(Name = "web", EmitDefaultValue = false)]
    [JsonPropertyName("web")]
    public string Web { get; set; }

    /// <summary>
    ///     Gets or Sets Zip
    /// </summary>
    [DataMember(Name = "zip", EmitDefaultValue = false)]
    [JsonPropertyName("zip")]
    public string Zip { get; set; }

    /// <summary>
    ///     Gets or Sets Bg
    /// </summary>
    [DataMember(Name = "bg", EmitDefaultValue = false)]
    [JsonPropertyName("bg")]
    public string Bg { get; set; }

    /// <summary>
    ///     Gets or Sets BgOcr
    /// </summary>
    [DataMember(Name = "bgOcr", EmitDefaultValue = false)]
    [JsonPropertyName("bgOcr")]
    public string BgOcr { get; set; }

    /// <summary>
    ///     Gets or Sets Pg
    /// </summary>
    [DataMember(Name = "pg", EmitDefaultValue = false)]
    [JsonPropertyName("pg")]
    public string Pg { get; set; }

    /// <summary>
    ///     Gets or Sets PgOcr
    /// </summary>
    [DataMember(Name = "pgOcr", EmitDefaultValue = false)]
    [JsonPropertyName("pgOcr")]
    public string PgOcr { get; set; }

    /// <summary>
    ///     Gets or Sets Iban
    /// </summary>
    [DataMember(Name = "iban", EmitDefaultValue = false)]
    [JsonPropertyName("iban")]
    public string Iban { get; set; }

    /// <summary>
    ///     Gets or Sets VatInfo
    /// </summary>
    [DataMember(Name = "vatInfo", EmitDefaultValue = false)]
    [JsonPropertyName("vatInfo")]
    public VatInfo VatInfo { get; set; }

    /// <summary>
    ///     Gets or Sets PreferredSettings
    /// </summary>
    [DataMember(Name = "preferredSettings", EmitDefaultValue = false)]
    [JsonPropertyName("preferredSettings")]
    public FtgPar PreferredSettings { get; set; }

    /// <summary>
    ///     Gets or Sets Contacts
    /// </summary>
    [DataMember(Name = "contacts", EmitDefaultValue = false)]
    [JsonPropertyName("contacts")]
    public Dictionary<string, object> Contacts { get; set; }

    /// <summary>
    ///     Gets or Sets Contact
    /// </summary>
    [DataMember(Name = "contact", EmitDefaultValue = false)]
    [JsonPropertyName("contact")]
    public Contact Contact { get; set; }

    /// <summary>
    ///     Gets or Sets TechnicalContact
    /// </summary>
    [DataMember(Name = "technicalContact", EmitDefaultValue = false)]
    [JsonPropertyName("technicalContact")]
    public TechnicalContact TechnicalContact { get; set; }

    /// <summary>
    ///     Gets or Sets AccountingMethod
    /// </summary>
    [DataMember(Name = "accountingMethod", EmitDefaultValue = false)]
    [JsonPropertyName("accountingMethod")]
    public bool? AccountingMethod { get; set; }

    /// <summary>
    ///     Gets or Sets VatAccountingInterval
    /// </summary>
    [DataMember(Name = "vatAccountingInterval", EmitDefaultValue = false)]
    [JsonPropertyName("vatAccountingInterval")]
    public int? VatAccountingInterval { get; set; }

    /// <summary>
    ///     Gets or Sets CompanyType
    /// </summary>
    [DataMember(Name = "companyType", EmitDefaultValue = false)]
    [JsonPropertyName("companyType")]
    public int? CompanyType { get; set; }

    /// <summary>
    ///     Gets or Sets LogotypeId
    /// </summary>
    [DataMember(Name = "logotypeId", EmitDefaultValue = false)]
    [JsonPropertyName("logotypeId")]
    public int? LogotypeId { get; set; }

    /// <summary>
    ///     Gets or Sets ReadPermissionAllPeriods
    /// </summary>
    [DataMember(Name = "readPermissionAllPeriods", EmitDefaultValue = false)]
    [JsonPropertyName("readPermissionAllPeriods")]
    public bool? ReadPermissionAllPeriods { get; set; }

    /// <summary>
    ///     Gets or Sets BankAccount
    /// </summary>
    [DataMember(Name = "bankAccount", EmitDefaultValue = false)]
    [JsonPropertyName("bankAccount")]
    public string BankAccount { get; set; }

    /// <summary>
    ///     Gets or Sets Bic
    /// </summary>
    [DataMember(Name = "bic", EmitDefaultValue = false)]
    [JsonPropertyName("bic")]
    public string Bic { get; set; }

    /// <summary>
    ///     Gets or Sets Swish
    /// </summary>
    [DataMember(Name = "swish", EmitDefaultValue = false)]
    [JsonPropertyName("swish")]
    public string Swish { get; set; }

    /// <summary>
    ///     Gets or Sets CompanyVatText
    /// </summary>
    [DataMember(Name = "companyVatText", EmitDefaultValue = false)]
    [JsonPropertyName("companyVatText")]
    public string CompanyVatText { get; set; }

    /// <summary>
    ///     Gets or Sets Domicile
    /// </summary>
    [DataMember(Name = "domicile", EmitDefaultValue = false)]
    [JsonPropertyName("domicile")]
    public string Domicile { get; set; }

    /// <summary>
    ///     Gets or Sets PeppolId
    /// </summary>
    [DataMember(Name = "peppolId", EmitDefaultValue = false)]
    [JsonPropertyName("peppolId")]
    public string PeppolId { get; set; }

    /// <summary>
    ///     Gets or Sets VatNumber
    /// </summary>
    [DataMember(Name = "vatNumber", EmitDefaultValue = false)]
    [JsonPropertyName("vatNumber")]
    public string VatNumber { get; set; }

    /// <summary>
    ///     Gets or Sets BankName
    /// </summary>
    [DataMember(Name = "bankName", EmitDefaultValue = false)]
    [JsonPropertyName("bankName")]
    public string BankName { get; set; }

    /// <summary>
    ///     Gets or Sets BankAddress
    /// </summary>
    [DataMember(Name = "bankAddress", EmitDefaultValue = false)]
    [JsonPropertyName("bankAddress")]
    public string BankAddress { get; set; }

    /// <summary>
    ///     Gets or Sets BankPostalAddress
    /// </summary>
    [DataMember(Name = "bankPostalAddress", EmitDefaultValue = false)]
    [JsonPropertyName("bankPostalAddress")]
    public string BankPostalAddress { get; set; }

    /// <summary>
    ///     Gets or Sets ApiKey
    /// </summary>
    [DataMember(Name = "apiKey", EmitDefaultValue = false)]
    [JsonPropertyName("apiKey")]
    public string ApiKey { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Details {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Auditor: ").Append(Auditor).Append("\n");
        sb.Append("  Box: ").Append(Box).Append("\n");
        sb.Append("  Category: ").Append(Category).Append("\n");
        sb.Append("  City: ").Append(City).Append("\n");
        sb.Append("  Comment: ").Append(Comment).Append("\n");
        sb.Append("  Country: ").Append(Country).Append("\n");
        sb.Append("  Email: ").Append(Email).Append("\n");
        sb.Append("  Fax: ").Append(Fax).Append("\n");
        sb.Append("  Mobile: ").Append(Mobile).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  OrgNumber: ").Append(OrgNumber).Append("\n");
        sb.Append("  EnrichedData: ").Append(EnrichedData).Append("\n");
        sb.Append("  Phone: ").Append(Phone).Append("\n");
        sb.Append("  Street: ").Append(Street).Append("\n");
        sb.Append("  Web: ").Append(Web).Append("\n");
        sb.Append("  Zip: ").Append(Zip).Append("\n");
        sb.Append("  Bg: ").Append(Bg).Append("\n");
        sb.Append("  BgOcr: ").Append(BgOcr).Append("\n");
        sb.Append("  Pg: ").Append(Pg).Append("\n");
        sb.Append("  PgOcr: ").Append(PgOcr).Append("\n");
        sb.Append("  Iban: ").Append(Iban).Append("\n");
        sb.Append("  VatInfo: ").Append(VatInfo).Append("\n");
        sb.Append("  PreferredSettings: ").Append(PreferredSettings).Append("\n");
        sb.Append("  Contacts: ").Append(Contacts).Append("\n");
        sb.Append("  Contact: ").Append(Contact).Append("\n");
        sb.Append("  TechnicalContact: ").Append(TechnicalContact).Append("\n");
        sb.Append("  AccountingMethod: ").Append(AccountingMethod).Append("\n");
        sb.Append("  VatAccountingInterval: ").Append(VatAccountingInterval).Append("\n");
        sb.Append("  CompanyType: ").Append(CompanyType).Append("\n");
        sb.Append("  LogotypeId: ").Append(LogotypeId).Append("\n");
        sb.Append("  ReadPermissionAllPeriods: ").Append(ReadPermissionAllPeriods).Append("\n");
        sb.Append("  BankAccount: ").Append(BankAccount).Append("\n");
        sb.Append("  Bic: ").Append(Bic).Append("\n");
        sb.Append("  Swish: ").Append(Swish).Append("\n");
        sb.Append("  CompanyVatText: ").Append(CompanyVatText).Append("\n");
        sb.Append("  Domicile: ").Append(Domicile).Append("\n");
        sb.Append("  PeppolId: ").Append(PeppolId).Append("\n");
        sb.Append("  VatNumber: ").Append(VatNumber).Append("\n");
        sb.Append("  BankName: ").Append(BankName).Append("\n");
        sb.Append("  BankAddress: ").Append(BankAddress).Append("\n");
        sb.Append("  BankPostalAddress: ").Append(BankPostalAddress).Append("\n");
        sb.Append("  ApiKey: ").Append(ApiKey).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}