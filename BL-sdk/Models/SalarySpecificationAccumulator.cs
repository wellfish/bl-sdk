using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class SalarySpecificationAccumulator
{
    /// <summary>
    ///     Gets or Sets Common
    /// </summary>
    [DataMember(Name = "common", EmitDefaultValue = false)]
    [JsonPropertyName("common")]
    public string Common { get; set; }

    /// <summary>
    ///     Gets or Sets Text
    /// </summary>
    [DataMember(Name = "text", EmitDefaultValue = false)]
    [JsonPropertyName("text")]
    public string Text { get; set; }

    /// <summary>
    ///     Gets or Sets Quantity
    /// </summary>
    [DataMember(Name = "quantity", EmitDefaultValue = false)]
    [JsonPropertyName("quantity")]
    public double? Quantity { get; set; }

    /// <summary>
    ///     Gets or Sets QuantityTotal
    /// </summary>
    [DataMember(Name = "quantityTotal", EmitDefaultValue = false)]
    [JsonPropertyName("quantityTotal")]
    public double? QuantityTotal { get; set; }

    /// <summary>
    ///     Gets or Sets Amount
    /// </summary>
    [DataMember(Name = "amount", EmitDefaultValue = false)]
    [JsonPropertyName("amount")]
    public decimal? Amount { get; set; }

    /// <summary>
    ///     Gets or Sets AmountTotal
    /// </summary>
    [DataMember(Name = "amountTotal", EmitDefaultValue = false)]
    [JsonPropertyName("amountTotal")]
    public decimal? AmountTotal { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class SalarySpecificationAccumulator {\n");
        sb.Append("  Common: ").Append(Common).Append("\n");
        sb.Append("  Text: ").Append(Text).Append("\n");
        sb.Append("  Quantity: ").Append(Quantity).Append("\n");
        sb.Append("  QuantityTotal: ").Append(QuantityTotal).Append("\n");
        sb.Append("  Amount: ").Append(Amount).Append("\n");
        sb.Append("  AmountTotal: ").Append(AmountTotal).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}