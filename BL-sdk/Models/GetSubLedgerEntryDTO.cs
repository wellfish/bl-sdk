using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
///     ___
/// </summary>
[DataContract]
public sealed class GetSubLedgerEntryDTO
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public long? Id { get; set; }

    /// <summary>
    ///     Gets or Sets AccountId
    /// </summary>
    [DataMember(Name = "accountId", EmitDefaultValue = false)]
    [JsonPropertyName("accountId")]
    public string AccountId { get; set; }

    /// <summary>
    ///     Gets or Sets Amount
    /// </summary>
    [DataMember(Name = "amount", EmitDefaultValue = false)]
    [JsonPropertyName("amount")]
    public decimal? Amount { get; set; }

    /// <summary>
    ///     Gets or Sets Quantity
    /// </summary>
    [DataMember(Name = "quantity", EmitDefaultValue = false)]
    [JsonPropertyName("quantity")]
    public double? Quantity { get; set; }

    /// <summary>
    ///     Gets or Sets Text
    /// </summary>
    [DataMember(Name = "text", EmitDefaultValue = false)]
    [JsonPropertyName("text")]
    public string Text { get; set; }

    /// <summary>
    ///     Gets or Sets CostCenterId
    /// </summary>
    [DataMember(Name = "costCenterId", EmitDefaultValue = false)]
    [JsonPropertyName("costCenterId")]
    public string CostCenterId { get; set; }

    /// <summary>
    ///     Gets or Sets CostBearerId
    /// </summary>
    [DataMember(Name = "costBearerId", EmitDefaultValue = false)]
    [JsonPropertyName("costBearerId")]
    public string CostBearerId { get; set; }

    /// <summary>
    ///     Gets or Sets ProjectId
    /// </summary>
    [DataMember(Name = "projectId", EmitDefaultValue = false)]
    [JsonPropertyName("projectId")]
    public string ProjectId { get; set; }

    /// <summary>
    ///     Gets or Sets Updated
    /// </summary>
    [DataMember(Name = "updated", EmitDefaultValue = false)]
    [JsonPropertyName("updated")]
    public bool? Updated { get; set; }

    /// <summary>
    ///     Gets or Sets Locked
    /// </summary>
    [DataMember(Name = "locked", EmitDefaultValue = false)]
    [JsonPropertyName("locked")]
    public bool? Locked { get; set; }

    /// <summary>
    ///     Gets or Sets Origin
    /// </summary>
    [DataMember(Name = "origin", EmitDefaultValue = false)]
    [JsonPropertyName("origin")]
    public string Origin { get; set; }

    /// <summary>
    ///     Gets or Sets CustomerInvoiceEntityId
    /// </summary>
    [DataMember(Name = "customerInvoiceEntityId", EmitDefaultValue = false)]
    [JsonPropertyName("customerInvoiceEntityId")]
    public long? CustomerInvoiceEntityId { get; set; }

    /// <summary>
    ///     Gets or Sets SupplierInvoiceEntityId
    /// </summary>
    [DataMember(Name = "supplierInvoiceEntityId", EmitDefaultValue = false)]
    [JsonPropertyName("supplierInvoiceEntityId")]
    public long? SupplierInvoiceEntityId { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class GetSubLedgerEntryDTO {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  AccountId: ").Append(AccountId).Append("\n");
        sb.Append("  Amount: ").Append(Amount).Append("\n");
        sb.Append("  Quantity: ").Append(Quantity).Append("\n");
        sb.Append("  Text: ").Append(Text).Append("\n");
        sb.Append("  CostCenterId: ").Append(CostCenterId).Append("\n");
        sb.Append("  CostBearerId: ").Append(CostBearerId).Append("\n");
        sb.Append("  ProjectId: ").Append(ProjectId).Append("\n");
        sb.Append("  Updated: ").Append(Updated).Append("\n");
        sb.Append("  Locked: ").Append(Locked).Append("\n");
        sb.Append("  Origin: ").Append(Origin).Append("\n");
        sb.Append("  CustomerInvoiceEntityId: ").Append(CustomerInvoiceEntityId).Append("\n");
        sb.Append("  SupplierInvoiceEntityId: ").Append(SupplierInvoiceEntityId).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}