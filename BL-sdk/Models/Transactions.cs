using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Transactions
{
    /// <summary>
    ///     Gets or Sets Booked
    /// </summary>
    [DataMember(Name = "booked", EmitDefaultValue = false)]
    [JsonPropertyName("booked")]
    public List<Transaction> Booked { get; set; }

    /// <summary>
    ///     Gets or Sets Pending
    /// </summary>
    [DataMember(Name = "pending", EmitDefaultValue = false)]
    [JsonPropertyName("pending")]
    public List<Transaction> Pending { get; set; }

    /// <summary>
    ///     Gets or Sets Bankgiro
    /// </summary>
    [DataMember(Name = "bankgiro", EmitDefaultValue = false)]
    [JsonPropertyName("bankgiro")]
    public List<BgInfo> Bankgiro { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Transactions {\n");
        sb.Append("  Booked: ").Append(Booked).Append("\n");
        sb.Append("  Pending: ").Append(Pending).Append("\n");
        sb.Append("  Bankgiro: ").Append(Bankgiro).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}