using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class EmailAddress
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets Type
    /// </summary>
    [DataMember(Name = "type", EmitDefaultValue = false)]
    [JsonPropertyName("type")]
    public string Type { get; set; }

    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    /// <summary>
    ///     Gets or Sets Email
    /// </summary>
    [DataMember(Name = "email", EmitDefaultValue = false)]
    [JsonPropertyName("email")]
    public string Email { get; set; }

    /// <summary>
    ///     Gets or Sets Line
    /// </summary>
    [DataMember(Name = "line", EmitDefaultValue = false)]
    [JsonPropertyName("line")]
    public int? Line { get; set; }

    /// <summary>
    ///     Gets or Sets CustomerEntityId
    /// </summary>
    [DataMember(Name = "customerEntityId", EmitDefaultValue = false)]
    [JsonPropertyName("customerEntityId")]
    public long? CustomerEntityId { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class EmailAddress {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Type: ").Append(Type).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Email: ").Append(Email).Append("\n");
        sb.Append("  Line: ").Append(Line).Append("\n");
        sb.Append("  CustomerEntityId: ").Append(CustomerEntityId).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}