using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class AttachedDocumentDTO
{
    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public long? Id { get; set; }

    /// <summary>
    ///     Gets or Sets Created
    /// </summary>
    [DataMember(Name = "created", EmitDefaultValue = false)]
    [JsonPropertyName("created")]
    public DateTime? Created { get; set; }

    /// <summary>
    ///     Gets or Sets Comment
    /// </summary>
    [DataMember(Name = "comment", EmitDefaultValue = false)]
    [JsonPropertyName("comment")]
    public string Comment { get; set; }

    /// <summary>
    ///     Gets or Sets Reference
    /// </summary>
    [DataMember(Name = "reference", EmitDefaultValue = false)]
    [JsonPropertyName("reference")]
    public string Reference { get; set; }

    /// <summary>
    ///     Gets or Sets FileExt
    /// </summary>
    [DataMember(Name = "fileExt", EmitDefaultValue = false)]
    [JsonPropertyName("fileExt")]
    public string FileExt { get; set; }

    /// <summary>
    ///     Gets or Sets ContentType
    /// </summary>
    [DataMember(Name = "contentType", EmitDefaultValue = false)]
    [JsonPropertyName("contentType")]
    public string ContentType { get; set; }

    /// <summary>
    ///     Gets or Sets AttachedTo
    /// </summary>
    [DataMember(Name = "attachedTo", EmitDefaultValue = false)]
    [JsonPropertyName("attachedTo")]
    public AttachedTo AttachedTo { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class AttachedDocumentDTO {\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Created: ").Append(Created).Append("\n");
        sb.Append("  Comment: ").Append(Comment).Append("\n");
        sb.Append("  Reference: ").Append(Reference).Append("\n");
        sb.Append("  FileExt: ").Append(FileExt).Append("\n");
        sb.Append("  ContentType: ").Append(ContentType).Append("\n");
        sb.Append("  AttachedTo: ").Append(AttachedTo).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}