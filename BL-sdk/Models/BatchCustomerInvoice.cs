using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class BatchCustomerInvoice
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "startDate", EmitDefaultValue = false)]
    [JsonPropertyName("startDate")]
    public DateTime? StartDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "endDate", EmitDefaultValue = false)]
    [JsonPropertyName("endDate")]
    public DateTime? EndDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "offset", EmitDefaultValue = false)]
    [JsonPropertyName("offset")]
    public int? Offset { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "pageRequested", EmitDefaultValue = false)]
    [JsonPropertyName("pageRequested")]
    public int? PageRequested { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "rowsRequested", EmitDefaultValue = false)]
    [JsonPropertyName("rowsRequested")]
    public int? RowsRequested { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "rowsReturned", EmitDefaultValue = false)]
    [JsonPropertyName("rowsReturned")]
    public int? RowsReturned { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "sortKey", EmitDefaultValue = false)]
    [JsonPropertyName("sortKey")]
    public string SortKey { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "sortOrder", EmitDefaultValue = false)]
    [JsonPropertyName("sortOrder")]
    public string SortOrder { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "totalPages", EmitDefaultValue = false)]
    [JsonPropertyName("totalPages")]
    public int? TotalPages { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "totalRows", EmitDefaultValue = false)]
    [JsonPropertyName("totalRows")]
    public int? TotalRows { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "data", EmitDefaultValue = false)]
    [JsonPropertyName("data")]
    public List<CustomerInvoice> Data { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class BatchCustomerInvoice {\n");
        sb.Append("  StartDate: ").Append(StartDate).Append("\n");
        sb.Append("  EndDate: ").Append(EndDate).Append("\n");
        sb.Append("  Offset: ").Append(Offset).Append("\n");
        sb.Append("  PageRequested: ").Append(PageRequested).Append("\n");
        sb.Append("  RowsRequested: ").Append(RowsRequested).Append("\n");
        sb.Append("  RowsReturned: ").Append(RowsReturned).Append("\n");
        sb.Append("  SortKey: ").Append(SortKey).Append("\n");
        sb.Append("  SortOrder: ").Append(SortOrder).Append("\n");
        sb.Append("  TotalPages: ").Append(TotalPages).Append("\n");
        sb.Append("  TotalRows: ").Append(TotalRows).Append("\n");
        sb.Append("  Data: ").Append(Data).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}