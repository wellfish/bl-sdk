using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class TransactionsDetails
{
    /// <summary>
    ///     Gets or Sets TransactionId
    /// </summary>
    [DataMember(Name = "transactionId", EmitDefaultValue = false)]
    [JsonPropertyName("transactionId")]
    public string TransactionId { get; set; }

    /// <summary>
    ///     Gets or Sets CreditorName
    /// </summary>
    [DataMember(Name = "creditorName", EmitDefaultValue = false)]
    [JsonPropertyName("creditorName")]
    public string CreditorName { get; set; }

    /// <summary>
    ///     Gets or Sets CreditoeAccount
    /// </summary>
    [DataMember(Name = "creditoeAccount", EmitDefaultValue = false)]
    [JsonPropertyName("creditoeAccount")]
    public BankAccount CreditoeAccount { get; set; }

    /// <summary>
    ///     Gets or Sets MandateId
    /// </summary>
    [DataMember(Name = "mandateId", EmitDefaultValue = false)]
    [JsonPropertyName("mandateId")]
    public string MandateId { get; set; }

    /// <summary>
    ///     Gets or Sets TransactionAmount
    /// </summary>
    [DataMember(Name = "transactionAmount", EmitDefaultValue = false)]
    [JsonPropertyName("transactionAmount")]
    public Amount TransactionAmount { get; set; }

    /// <summary>
    ///     Gets or Sets BookingDate
    /// </summary>
    [DataMember(Name = "bookingDate", EmitDefaultValue = false)]
    [JsonPropertyName("bookingDate")]
    public string BookingDate { get; set; }

    /// <summary>
    ///     Gets or Sets ValueDate
    /// </summary>
    [DataMember(Name = "valueDate", EmitDefaultValue = false)]
    [JsonPropertyName("valueDate")]
    public string ValueDate { get; set; }

    /// <summary>
    ///     Gets or Sets RemittanceInformationUnstructured
    /// </summary>
    [DataMember(Name = "remittanceInformationUnstructured", EmitDefaultValue = false)]
    [JsonPropertyName("remittanceInformationUnstructured")]
    public string RemittanceInformationUnstructured { get; set; }

    /// <summary>
    ///     Gets or Sets BankTransactionCode
    /// </summary>
    [DataMember(Name = "bankTransactionCode", EmitDefaultValue = false)]
    [JsonPropertyName("bankTransactionCode")]
    public string BankTransactionCode { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class TransactionsDetails {\n");
        sb.Append("  TransactionId: ").Append(TransactionId).Append("\n");
        sb.Append("  CreditorName: ").Append(CreditorName).Append("\n");
        sb.Append("  CreditoeAccount: ").Append(CreditoeAccount).Append("\n");
        sb.Append("  MandateId: ").Append(MandateId).Append("\n");
        sb.Append("  TransactionAmount: ").Append(TransactionAmount).Append("\n");
        sb.Append("  BookingDate: ").Append(BookingDate).Append("\n");
        sb.Append("  ValueDate: ").Append(ValueDate).Append("\n");
        sb.Append("  RemittanceInformationUnstructured: ").Append(RemittanceInformationUnstructured).Append("\n");
        sb.Append("  BankTransactionCode: ").Append(BankTransactionCode).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}