using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class CostCenterBalanceAccountSums
{
    /// <summary>
    ///     Gets or Sets Account
    /// </summary>
    [DataMember(Name = "account", EmitDefaultValue = false)]
    [JsonPropertyName("account")]
    public string Account { get; set; }

    /// <summary>
    ///     Gets or Sets OpeningBalance
    /// </summary>
    [DataMember(Name = "openingBalance", EmitDefaultValue = false)]
    [JsonPropertyName("openingBalance")]
    public decimal? OpeningBalance { get; set; }

    /// <summary>
    ///     Gets or Sets Balance
    /// </summary>
    [DataMember(Name = "balance", EmitDefaultValue = false)]
    [JsonPropertyName("balance")]
    public decimal? Balance { get; set; }

    /// <summary>
    ///     Gets or Sets AccumulatedBalance
    /// </summary>
    [DataMember(Name = "accumulatedBalance", EmitDefaultValue = false)]
    [JsonPropertyName("accumulatedBalance")]
    public decimal? AccumulatedBalance { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class CostCenterBalanceAccountSums {\n");
        sb.Append("  Account: ").Append(Account).Append("\n");
        sb.Append("  OpeningBalance: ").Append(OpeningBalance).Append("\n");
        sb.Append("  Balance: ").Append(Balance).Append("\n");
        sb.Append("  AccumulatedBalance: ").Append(AccumulatedBalance).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}