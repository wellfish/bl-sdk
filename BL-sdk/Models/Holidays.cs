using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Holidays
{
    /// <summary>
    ///     Gets or Sets Utilized
    /// </summary>
    [DataMember(Name = "utilized", EmitDefaultValue = false)]
    [JsonPropertyName("utilized")]
    public double? Utilized { get; set; }

    /// <summary>
    ///     Gets or Sets Paid
    /// </summary>
    [DataMember(Name = "paid", EmitDefaultValue = false)]
    [JsonPropertyName("paid")]
    public double? Paid { get; set; }

    /// <summary>
    ///     Gets or Sets Unpaid
    /// </summary>
    [DataMember(Name = "unpaid", EmitDefaultValue = false)]
    [JsonPropertyName("unpaid")]
    public double? Unpaid { get; set; }

    /// <summary>
    ///     Gets or Sets Saved
    /// </summary>
    [DataMember(Name = "saved", EmitDefaultValue = false)]
    [JsonPropertyName("saved")]
    public double? Saved { get; set; }

    /// <summary>
    ///     Gets or Sets Advance
    /// </summary>
    [DataMember(Name = "advance", EmitDefaultValue = false)]
    [JsonPropertyName("advance")]
    public double? Advance { get; set; }

    /// <summary>
    ///     Gets or Sets PaidTotal
    /// </summary>
    [DataMember(Name = "paidTotal", EmitDefaultValue = false)]
    [JsonPropertyName("paidTotal")]
    public double? PaidTotal { get; set; }

    /// <summary>
    ///     Gets or Sets UnpaidTotal
    /// </summary>
    [DataMember(Name = "unpaidTotal", EmitDefaultValue = false)]
    [JsonPropertyName("unpaidTotal")]
    public double? UnpaidTotal { get; set; }

    /// <summary>
    ///     Gets or Sets AdvanceTotal
    /// </summary>
    [DataMember(Name = "advanceTotal", EmitDefaultValue = false)]
    [JsonPropertyName("advanceTotal")]
    public double? AdvanceTotal { get; set; }

    /// <summary>
    ///     Gets or Sets SavedTotal
    /// </summary>
    [DataMember(Name = "savedTotal", EmitDefaultValue = false)]
    [JsonPropertyName("savedTotal")]
    public double? SavedTotal { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Holidays {\n");
        sb.Append("  Utilized: ").Append(Utilized).Append("\n");
        sb.Append("  Paid: ").Append(Paid).Append("\n");
        sb.Append("  Unpaid: ").Append(Unpaid).Append("\n");
        sb.Append("  Saved: ").Append(Saved).Append("\n");
        sb.Append("  Advance: ").Append(Advance).Append("\n");
        sb.Append("  PaidTotal: ").Append(PaidTotal).Append("\n");
        sb.Append("  UnpaidTotal: ").Append(UnpaidTotal).Append("\n");
        sb.Append("  AdvanceTotal: ").Append(AdvanceTotal).Append("\n");
        sb.Append("  SavedTotal: ").Append(SavedTotal).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}