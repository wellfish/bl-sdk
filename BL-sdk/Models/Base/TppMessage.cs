using System.Text.Json.Serialization;

namespace Bl_sdk.Models.Base;

public sealed class TppMessage
{
    [JsonPropertyName("category")]
    public string? Category { get; set; }

    [JsonPropertyName("code")]
    public string? Code { get; set; }

    [JsonPropertyName("path")]
    public string? Path { get; set; }

    [JsonPropertyName("text")]
    public string? Text { get; set; }
}