using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class SalaryDeviation
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public long? Id { get; set; }

    /// <summary>
    ///     Gets or Sets Text
    /// </summary>
    [DataMember(Name = "text", EmitDefaultValue = false)]
    [JsonPropertyName("text")]
    public string Text { get; set; }

    /// <summary>
    ///     Gets or Sets DeviationStartDate
    /// </summary>
    [DataMember(Name = "deviationStartDate", EmitDefaultValue = false)]
    [JsonPropertyName("deviationStartDate")]
    public DateTime? DeviationStartDate { get; set; }

    /// <summary>
    ///     Gets or Sets EmployeeId
    /// </summary>
    [DataMember(Name = "employeeId", EmitDefaultValue = false)]
    [JsonPropertyName("employeeId")]
    public string EmployeeId { get; set; }

    /// <summary>
    ///     Gets or Sets DeviationType
    /// </summary>
    [DataMember(Name = "deviationType", EmitDefaultValue = false)]
    [JsonPropertyName("deviationType")]
    public int? DeviationType { get; set; }

    /// <summary>
    ///     Gets or Sets SalaryTypeCode
    /// </summary>
    [DataMember(Name = "salaryTypeCode", EmitDefaultValue = false)]
    [JsonPropertyName("salaryTypeCode")]
    public string SalaryTypeCode { get; set; }

    /// <summary>
    ///     Gets or Sets HourQuantity
    /// </summary>
    [DataMember(Name = "hourQuantity", EmitDefaultValue = false)]
    [JsonPropertyName("hourQuantity")]
    public decimal? HourQuantity { get; set; }

    /// <summary>
    ///     Gets or Sets Percent
    /// </summary>
    [DataMember(Name = "percent", EmitDefaultValue = false)]
    [JsonPropertyName("percent")]
    public decimal? Percent { get; set; }

    /// <summary>
    ///     Gets or Sets TimeStart
    /// </summary>
    [DataMember(Name = "timeStart", EmitDefaultValue = false)]
    [JsonPropertyName("timeStart")]
    public string TimeStart { get; set; }

    /// <summary>
    ///     Gets or Sets TimeEnd
    /// </summary>
    [DataMember(Name = "timeEnd", EmitDefaultValue = false)]
    [JsonPropertyName("timeEnd")]
    public string TimeEnd { get; set; }

    /// <summary>
    ///     Gets or Sets QualifyingDayOfSickness
    /// </summary>
    [DataMember(Name = "qualifyingDayOfSickness", EmitDefaultValue = false)]
    [JsonPropertyName("qualifyingDayOfSickness")]
    public bool? QualifyingDayOfSickness { get; set; }

    /// <summary>
    ///     Gets or Sets HollidayAgreementId
    /// </summary>
    [DataMember(Name = "hollidayAgreementId", EmitDefaultValue = false)]
    [JsonPropertyName("hollidayAgreementId")]
    public string HollidayAgreementId { get; set; }

    /// <summary>
    ///     Gets or Sets Quantity
    /// </summary>
    [DataMember(Name = "quantity", EmitDefaultValue = false)]
    [JsonPropertyName("quantity")]
    public string Quantity { get; set; }

    /// <summary>
    ///     Gets or Sets Unit
    /// </summary>
    [DataMember(Name = "unit", EmitDefaultValue = false)]
    [JsonPropertyName("unit")]
    public string Unit { get; set; }

    /// <summary>
    ///     Gets or Sets Updated
    /// </summary>
    [DataMember(Name = "updated", EmitDefaultValue = false)]
    [JsonPropertyName("updated")]
    public bool? Updated { get; set; }

    /// <summary>
    ///     Gets or Sets DeviationEndDate
    /// </summary>
    [DataMember(Name = "deviationEndDate", EmitDefaultValue = false)]
    [JsonPropertyName("deviationEndDate")]
    public DateTime? DeviationEndDate { get; set; }

    /// <summary>
    ///     Gets or Sets WorkingDay
    /// </summary>
    [DataMember(Name = "workingDay", EmitDefaultValue = false)]
    [JsonPropertyName("workingDay")]
    public bool? WorkingDay { get; set; }

    /// <summary>
    ///     Gets or Sets HollidayBased
    /// </summary>
    [DataMember(Name = "hollidayBased", EmitDefaultValue = false)]
    [JsonPropertyName("hollidayBased")]
    public bool? HollidayBased { get; set; }

    /// <summary>
    ///     Gets or Sets Ib
    /// </summary>
    [DataMember(Name = "ib", EmitDefaultValue = false)]
    [JsonPropertyName("ib")]
    public bool? Ib { get; set; }

    /// <summary>
    ///     Gets or Sets FullTime
    /// </summary>
    [DataMember(Name = "fullTime", EmitDefaultValue = false)]
    [JsonPropertyName("fullTime")]
    public bool? FullTime { get; set; }

    /// <summary>
    ///     Gets or Sets HollidayBasedType
    /// </summary>
    [DataMember(Name = "hollidayBasedType", EmitDefaultValue = false)]
    [JsonPropertyName("hollidayBasedType")]
    public int? HollidayBasedType { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class SalaryDeviation {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Text: ").Append(Text).Append("\n");
        sb.Append("  DeviationStartDate: ").Append(DeviationStartDate).Append("\n");
        sb.Append("  EmployeeId: ").Append(EmployeeId).Append("\n");
        sb.Append("  DeviationType: ").Append(DeviationType).Append("\n");
        sb.Append("  SalaryTypeCode: ").Append(SalaryTypeCode).Append("\n");
        sb.Append("  HourQuantity: ").Append(HourQuantity).Append("\n");
        sb.Append("  Percent: ").Append(Percent).Append("\n");
        sb.Append("  TimeStart: ").Append(TimeStart).Append("\n");
        sb.Append("  TimeEnd: ").Append(TimeEnd).Append("\n");
        sb.Append("  QualifyingDayOfSickness: ").Append(QualifyingDayOfSickness).Append("\n");
        sb.Append("  HollidayAgreementId: ").Append(HollidayAgreementId).Append("\n");
        sb.Append("  Quantity: ").Append(Quantity).Append("\n");
        sb.Append("  Unit: ").Append(Unit).Append("\n");
        sb.Append("  Updated: ").Append(Updated).Append("\n");
        sb.Append("  DeviationEndDate: ").Append(DeviationEndDate).Append("\n");
        sb.Append("  WorkingDay: ").Append(WorkingDay).Append("\n");
        sb.Append("  HollidayBased: ").Append(HollidayBased).Append("\n");
        sb.Append("  Ib: ").Append(Ib).Append("\n");
        sb.Append("  FullTime: ").Append(FullTime).Append("\n");
        sb.Append("  HollidayBasedType: ").Append(HollidayBasedType).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}