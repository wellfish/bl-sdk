using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class SupplierPayment
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets PaymentDate
    /// </summary>
    [DataMember(Name = "paymentDate", EmitDefaultValue = false)]
    [JsonPropertyName("paymentDate")]
    public DateTime? PaymentDate { get; set; }

    /// <summary>
    ///     Gets or Sets AmountInOriginalCurrency
    /// </summary>
    [DataMember(Name = "amountInOriginalCurrency", EmitDefaultValue = false)]
    [JsonPropertyName("amountInOriginalCurrency")]
    public decimal? AmountInOriginalCurrency { get; set; }

    /// <summary>
    ///     Gets or Sets AmountInLocalCurrency
    /// </summary>
    [DataMember(Name = "amountInLocalCurrency", EmitDefaultValue = false)]
    [JsonPropertyName("amountInLocalCurrency")]
    public decimal? AmountInLocalCurrency { get; set; }

    /// <summary>
    ///     Gets or Sets AccountId
    /// </summary>
    [DataMember(Name = "accountId", EmitDefaultValue = false)]
    [JsonPropertyName("accountId")]
    public string AccountId { get; set; }

    /// <summary>
    ///     Gets or Sets Preliminary
    /// </summary>
    [DataMember(Name = "preliminary", EmitDefaultValue = false)]
    [JsonPropertyName("preliminary")]
    public bool? Preliminary { get; set; }

    /// <summary>
    ///     Gets or Sets RegisteredByUser
    /// </summary>
    [DataMember(Name = "registeredByUser", EmitDefaultValue = false)]
    [JsonPropertyName("registeredByUser")]
    public string RegisteredByUser { get; set; }

    /// <summary>
    ///     Gets or Sets PaymentMethod
    /// </summary>
    [DataMember(Name = "paymentMethod", EmitDefaultValue = false)]
    [JsonPropertyName("paymentMethod")]
    public string PaymentMethod { get; set; }

    /// <summary>
    ///     Gets or Sets ExternalWatch
    /// </summary>
    [DataMember(Name = "externalWatch", EmitDefaultValue = false)]
    [JsonPropertyName("externalWatch")]
    public bool? ExternalWatch { get; set; }

    /// <summary>
    ///     Gets or Sets ExternalWatchDate
    /// </summary>
    [DataMember(Name = "externalWatchDate", EmitDefaultValue = false)]
    [JsonPropertyName("externalWatchDate")]
    public DateTime? ExternalWatchDate { get; set; }

    /// <summary>
    ///     Gets or Sets JournalId
    /// </summary>
    [DataMember(Name = "journalId", EmitDefaultValue = false)]
    [JsonPropertyName("journalId")]
    public string JournalId { get; set; }

    /// <summary>
    ///     Gets or Sets Pain001Id
    /// </summary>
    [DataMember(Name = "pain001Id", EmitDefaultValue = false)]
    [JsonPropertyName("pain001Id")]
    public string Pain001Id { get; set; }

    /// <summary>
    ///     Gets or Sets JournalEntryId
    /// </summary>
    [DataMember(Name = "journalEntryId", EmitDefaultValue = false)]
    [JsonPropertyName("journalEntryId")]
    public long? JournalEntryId { get; set; }

    /// <summary>
    ///     Gets or Sets JournalEntryDate
    /// </summary>
    [DataMember(Name = "journalEntryDate", EmitDefaultValue = false)]
    [JsonPropertyName("journalEntryDate")]
    public DateTime? JournalEntryDate { get; set; }

    /// <summary>
    ///     Gets or Sets PaymentToAccount
    /// </summary>
    [DataMember(Name = "paymentToAccount", EmitDefaultValue = false)]
    [JsonPropertyName("paymentToAccount")]
    public string PaymentToAccount { get; set; }

    /// <summary>
    ///     Gets or Sets AccountingOfRest
    /// </summary>
    [DataMember(Name = "accountingOfRest", EmitDefaultValue = false)]
    [JsonPropertyName("accountingOfRest")]
    public bool? AccountingOfRest { get; set; }

    /// <summary>
    ///     Gets or Sets InvoiceEntityId
    /// </summary>
    [DataMember(Name = "invoiceEntityId", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceEntityId")]
    public long? InvoiceEntityId { get; set; }

    /// <summary>
    ///     Gets or Sets InvoiceId
    /// </summary>
    [DataMember(Name = "invoiceId", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceId")]
    public long? InvoiceId { get; set; }

    /// <summary>
    ///     Gets or Sets Psd2Id
    /// </summary>
    [DataMember(Name = "psd2Id", EmitDefaultValue = false)]
    [JsonPropertyName("psd2Id")]
    public string Psd2Id { get; set; }

    /// <summary>
    ///     Gets or Sets Psd2State
    /// </summary>
    [DataMember(Name = "psd2State", EmitDefaultValue = false)]
    [JsonPropertyName("psd2State")]
    public string Psd2State { get; set; }

    /// <summary>
    ///     Gets or Sets SupplierId
    /// </summary>
    [DataMember(Name = "supplierId", EmitDefaultValue = false)]
    [JsonPropertyName("supplierId")]
    public string SupplierId { get; set; }

    /// <summary>
    ///     Gets or Sets SupplierName
    /// </summary>
    [DataMember(Name = "supplierName", EmitDefaultValue = false)]
    [JsonPropertyName("supplierName")]
    public string SupplierName { get; set; }

    /// <summary>
    ///     Gets or Sets Ocr
    /// </summary>
    [DataMember(Name = "ocr", EmitDefaultValue = false)]
    [JsonPropertyName("ocr")]
    public string Ocr { get; set; }

    /// <summary>
    ///     Gets or Sets InvoiceNumber
    /// </summary>
    [DataMember(Name = "invoiceNumber", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceNumber")]
    public string InvoiceNumber { get; set; }

    /// <summary>
    ///     Gets or Sets CreatedDate
    /// </summary>
    [DataMember(Name = "createdDate", EmitDefaultValue = false)]
    [JsonPropertyName("createdDate")]
    public DateTime? CreatedDate { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class SupplierPayment {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  PaymentDate: ").Append(PaymentDate).Append("\n");
        sb.Append("  AmountInOriginalCurrency: ").Append(AmountInOriginalCurrency).Append("\n");
        sb.Append("  AmountInLocalCurrency: ").Append(AmountInLocalCurrency).Append("\n");
        sb.Append("  AccountId: ").Append(AccountId).Append("\n");
        sb.Append("  Preliminary: ").Append(Preliminary).Append("\n");
        sb.Append("  RegisteredByUser: ").Append(RegisteredByUser).Append("\n");
        sb.Append("  PaymentMethod: ").Append(PaymentMethod).Append("\n");
        sb.Append("  ExternalWatch: ").Append(ExternalWatch).Append("\n");
        sb.Append("  ExternalWatchDate: ").Append(ExternalWatchDate).Append("\n");
        sb.Append("  JournalId: ").Append(JournalId).Append("\n");
        sb.Append("  Pain001Id: ").Append(Pain001Id).Append("\n");
        sb.Append("  JournalEntryId: ").Append(JournalEntryId).Append("\n");
        sb.Append("  JournalEntryDate: ").Append(JournalEntryDate).Append("\n");
        sb.Append("  PaymentToAccount: ").Append(PaymentToAccount).Append("\n");
        sb.Append("  AccountingOfRest: ").Append(AccountingOfRest).Append("\n");
        sb.Append("  InvoiceEntityId: ").Append(InvoiceEntityId).Append("\n");
        sb.Append("  InvoiceId: ").Append(InvoiceId).Append("\n");
        sb.Append("  Psd2Id: ").Append(Psd2Id).Append("\n");
        sb.Append("  Psd2State: ").Append(Psd2State).Append("\n");
        sb.Append("  SupplierId: ").Append(SupplierId).Append("\n");
        sb.Append("  SupplierName: ").Append(SupplierName).Append("\n");
        sb.Append("  Ocr: ").Append(Ocr).Append("\n");
        sb.Append("  InvoiceNumber: ").Append(InvoiceNumber).Append("\n");
        sb.Append("  CreatedDate: ").Append(CreatedDate).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}