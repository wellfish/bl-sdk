using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class SupplierInvoice
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long EntityId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "amountInLocalCurrency", EmitDefaultValue = false)]
    [JsonPropertyName("amountInLocalCurrency")]
    public decimal? AmountInLocalCurrency { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "amountInOriginalCurrency", EmitDefaultValue = false)]
    [JsonPropertyName("amountInOriginalCurrency")]
    public decimal? AmountInOriginalCurrency { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "amountPaidInLocalCurrency", EmitDefaultValue = false)]
    [JsonPropertyName("amountPaidInLocalCurrency")]
    public decimal? AmountPaidInLocalCurrency { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "amountPaidInOriginalCurrency", EmitDefaultValue = false)]
    [JsonPropertyName("amountPaidInOriginalCurrency")]
    public decimal? AmountPaidInOriginalCurrency { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "amountRemainingInLocalCurrency", EmitDefaultValue = false)]
    [JsonPropertyName("amountRemainingInLocalCurrency")]
    public decimal? AmountRemainingInLocalCurrency { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "amountRemainingInOriginalCurrency", EmitDefaultValue = false)]
    [JsonPropertyName("amountRemainingInOriginalCurrency")]
    public decimal? AmountRemainingInOriginalCurrency { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "associatedPayments", EmitDefaultValue = false)]
    [JsonPropertyName("associatedPayments")]
    public List<SupplierPayment> AssociatedPayments { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "attestationCode", EmitDefaultValue = false)]
    [JsonPropertyName("attestationCode")]
    public string AttestationCode { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "attestationJournalEntryDate", EmitDefaultValue = false)]
    [JsonPropertyName("attestationJournalEntryDate")]
    public DateTime? AttestationJournalEntryDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "attestationJournalEntryId", EmitDefaultValue = false)]
    [JsonPropertyName("attestationJournalEntryId")]
    public long? AttestationJournalEntryId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "attestationJournalId", EmitDefaultValue = false)]
    [JsonPropertyName("attestationJournalId")]
    public string AttestationJournalId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "attested", EmitDefaultValue = false)]
    [JsonPropertyName("attested")]
    public bool? Attested { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "autoRegistered", EmitDefaultValue = false)]
    [JsonPropertyName("autoRegistered")]
    public bool? AutoRegistered { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "comment", EmitDefaultValue = false)]
    [JsonPropertyName("comment")]
    public string Comment { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "consecutiveNumber", EmitDefaultValue = false)]
    [JsonPropertyName("consecutiveNumber")]
    public long? ConsecutiveNumber { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "costBearerId", EmitDefaultValue = false)]
    [JsonPropertyName("costBearerId")]
    public string CostBearerId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "costCenterId", EmitDefaultValue = false)]
    [JsonPropertyName("costCenterId")]
    public string CostCenterId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "currency", EmitDefaultValue = false)]
    [JsonPropertyName("currency")]
    public string Currency { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "dateOfLatestPayment", EmitDefaultValue = false)]
    [JsonPropertyName("dateOfLatestPayment")]
    public DateTime? DateOfLatestPayment { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "documentIds", EmitDefaultValue = false)]
    [JsonPropertyName("documentIds")]
    public List<long> DocumentIds { get; set; } = [];

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "dueDate", EmitDefaultValue = false)]
    [JsonPropertyName("dueDate")]
    public DateTime? DueDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "externalInvoiceId", EmitDefaultValue = false)]
    [JsonPropertyName("externalInvoiceId")]
    public long? ExternalInvoiceId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "externalWatch", EmitDefaultValue = false)]
    [JsonPropertyName("externalWatch")]
    public bool? ExternalWatch { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public int? Id { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "invoiceDate", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceDate")]
    public DateTime? InvoiceDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "invoiceNumber", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceNumber")]
    public string InvoiceNumber { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "journalEntryDate", EmitDefaultValue = false)]
    [JsonPropertyName("journalEntryDate")]
    public DateTime? JournalEntryDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "journalEntryEntityId", EmitDefaultValue = false)]
    [JsonPropertyName("journalEntryEntityId")]
    public long? JournalEntryEntityId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "journalEntryId", EmitDefaultValue = false)]
    [JsonPropertyName("journalEntryId")]
    public long? JournalEntryId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "journalId", EmitDefaultValue = false)]
    [JsonPropertyName("journalId")]
    public string JournalId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "ledgerEntries", EmitDefaultValue = false)]
    [JsonPropertyName("ledgerEntries")]
    public List<LedgerEntry> LedgerEntries { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "ledgerNumber", EmitDefaultValue = false)]
    [JsonPropertyName("ledgerNumber")]
    public int? LedgerNumber { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "lockedFromPayment", EmitDefaultValue = false)]
    [JsonPropertyName("lockedFromPayment")]
    public bool? LockedFromPayment { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "measures", EmitDefaultValue = false)]
    [JsonPropertyName("measures")]
    public List<string> Measures { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "ocr", EmitDefaultValue = false)]
    [JsonPropertyName("ocr")]
    public string Ocr { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "paid", EmitDefaultValue = false)]
    [JsonPropertyName("paid")]
    public bool? Paid { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "paymentType", EmitDefaultValue = false)]
    [JsonPropertyName("paymentType")]
    public long? PaymentType { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "preliminary", EmitDefaultValue = false)]
    [JsonPropertyName("preliminary")]
    public bool? Preliminary { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "projectId", EmitDefaultValue = false)]
    [JsonPropertyName("projectId")]
    public string ProjectId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "registeredByUser", EmitDefaultValue = false)]
    [JsonPropertyName("registeredByUser")]
    public string RegisteredByUser { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "registrationDate", EmitDefaultValue = false)]
    [JsonPropertyName("registrationDate")]
    public DateTime? RegistrationDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "subLedgerEntries", EmitDefaultValue = false)]
    [JsonPropertyName("subLedgerEntries")]
    public List<SubLedgerEntry> SubLedgerEntries { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "supplierBankAccount", EmitDefaultValue = false)]
    [JsonPropertyName("supplierBankAccount")]
    public string SupplierBankAccount { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "supplierBg", EmitDefaultValue = false)]
    [JsonPropertyName("supplierBg")]
    public string SupplierBg { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "supplierId", EmitDefaultValue = false)]
    [JsonPropertyName("supplierId")]
    public string SupplierId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "supplierName", EmitDefaultValue = false)]
    [JsonPropertyName("supplierName")]
    public string SupplierName { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "supplierPg", EmitDefaultValue = false)]
    [JsonPropertyName("supplierPg")]
    public string SupplierPg { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "status", EmitDefaultValue = false)]
    [JsonPropertyName("status")]
    public List<int?> Status { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "paymentError", EmitDefaultValue = false)]
    [JsonPropertyName("paymentError")]
    public string PaymentError { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class SupplierInvoice {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  AmountInLocalCurrency: ").Append(AmountInLocalCurrency).Append("\n");
        sb.Append("  AmountInOriginalCurrency: ").Append(AmountInOriginalCurrency).Append("\n");
        sb.Append("  AmountPaidInLocalCurrency: ").Append(AmountPaidInLocalCurrency).Append("\n");
        sb.Append("  AmountPaidInOriginalCurrency: ").Append(AmountPaidInOriginalCurrency).Append("\n");
        sb.Append("  AmountRemainingInLocalCurrency: ").Append(AmountRemainingInLocalCurrency).Append("\n");
        sb.Append("  AmountRemainingInOriginalCurrency: ").Append(AmountRemainingInOriginalCurrency).Append("\n");
        sb.Append("  AssociatedPayments: ").Append(AssociatedPayments).Append("\n");
        sb.Append("  AttestationCode: ").Append(AttestationCode).Append("\n");
        sb.Append("  AttestationJournalEntryDate: ").Append(AttestationJournalEntryDate).Append("\n");
        sb.Append("  AttestationJournalEntryId: ").Append(AttestationJournalEntryId).Append("\n");
        sb.Append("  AttestationJournalId: ").Append(AttestationJournalId).Append("\n");
        sb.Append("  Attested: ").Append(Attested).Append("\n");
        sb.Append("  AutoRegistered: ").Append(AutoRegistered).Append("\n");
        sb.Append("  Comment: ").Append(Comment).Append("\n");
        sb.Append("  ConsecutiveNumber: ").Append(ConsecutiveNumber).Append("\n");
        sb.Append("  CostBearerId: ").Append(CostBearerId).Append("\n");
        sb.Append("  CostCenterId: ").Append(CostCenterId).Append("\n");
        sb.Append("  Currency: ").Append(Currency).Append("\n");
        sb.Append("  DateOfLatestPayment: ").Append(DateOfLatestPayment).Append("\n");
        sb.Append("  DocumentIds: ").Append(DocumentIds).Append("\n");
        sb.Append("  DueDate: ").Append(DueDate).Append("\n");
        sb.Append("  ExternalInvoiceId: ").Append(ExternalInvoiceId).Append("\n");
        sb.Append("  ExternalWatch: ").Append(ExternalWatch).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  InvoiceDate: ").Append(InvoiceDate).Append("\n");
        sb.Append("  InvoiceNumber: ").Append(InvoiceNumber).Append("\n");
        sb.Append("  JournalEntryDate: ").Append(JournalEntryDate).Append("\n");
        sb.Append("  JournalEntryEntityId: ").Append(JournalEntryEntityId).Append("\n");
        sb.Append("  JournalEntryId: ").Append(JournalEntryId).Append("\n");
        sb.Append("  JournalId: ").Append(JournalId).Append("\n");
        sb.Append("  LedgerEntries: ").Append(LedgerEntries).Append("\n");
        sb.Append("  LedgerNumber: ").Append(LedgerNumber).Append("\n");
        sb.Append("  LockedFromPayment: ").Append(LockedFromPayment).Append("\n");
        sb.Append("  Measures: ").Append(Measures).Append("\n");
        sb.Append("  Ocr: ").Append(Ocr).Append("\n");
        sb.Append("  Paid: ").Append(Paid).Append("\n");
        sb.Append("  PaymentType: ").Append(PaymentType).Append("\n");
        sb.Append("  Preliminary: ").Append(Preliminary).Append("\n");
        sb.Append("  ProjectId: ").Append(ProjectId).Append("\n");
        sb.Append("  RegisteredByUser: ").Append(RegisteredByUser).Append("\n");
        sb.Append("  RegistrationDate: ").Append(RegistrationDate).Append("\n");
        sb.Append("  SubLedgerEntries: ").Append(SubLedgerEntries).Append("\n");
        sb.Append("  SupplierBankAccount: ").Append(SupplierBankAccount).Append("\n");
        sb.Append("  SupplierBg: ").Append(SupplierBg).Append("\n");
        sb.Append("  SupplierId: ").Append(SupplierId).Append("\n");
        sb.Append("  SupplierName: ").Append(SupplierName).Append("\n");
        sb.Append("  SupplierPg: ").Append(SupplierPg).Append("\n");
        sb.Append("  Status: ").Append(Status).Append("\n");
        sb.Append("  PaymentError: ").Append(PaymentError).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}