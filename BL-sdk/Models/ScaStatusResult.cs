using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class ScaStatusResult
{
    /// <summary>
    ///     Gets or Sets Status
    /// </summary>
    [DataMember(Name = "status", EmitDefaultValue = false)]
    [JsonPropertyName("status")]
    public string Status { get; set; }

    /// <summary>
    ///     Gets or Sets QrImage
    /// </summary>
    [DataMember(Name = "qrImage", EmitDefaultValue = false)]
    [JsonPropertyName("qrImage")]
    public string QrImage { get; set; }

    /// <summary>
    ///     Gets or Sets ErrorCode
    /// </summary>
    [DataMember(Name = "errorCode", EmitDefaultValue = false)]
    [JsonPropertyName("errorCode")]
    public string ErrorCode { get; set; }

    /// <summary>
    ///     Gets or Sets ErrorText
    /// </summary>
    [DataMember(Name = "errorText", EmitDefaultValue = false)]
    [JsonPropertyName("errorText")]
    public string ErrorText { get; set; }

    /// <summary>
    ///     Gets or Sets BankIdToken
    /// </summary>
    [DataMember(Name = "bankIdToken", EmitDefaultValue = false)]
    [JsonPropertyName("bankIdToken")]
    public string BankIdToken { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class ScaStatusResult {\n");
        sb.Append("  Status: ").Append(Status).Append("\n");
        sb.Append("  QrImage: ").Append(QrImage).Append("\n");
        sb.Append("  ErrorCode: ").Append(ErrorCode).Append("\n");
        sb.Append("  ErrorText: ").Append(ErrorText).Append("\n");
        sb.Append("  BankIdToken: ").Append(BankIdToken).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}