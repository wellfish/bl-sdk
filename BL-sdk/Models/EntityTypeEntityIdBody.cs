using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class EntityTypeEntityIdBody
{
    /// <summary>
    ///     The file to upload
    /// </summary>
    /// <value>The file to upload</value>
    [DataMember(Name = "file", EmitDefaultValue = false)]
    [JsonPropertyName("file")]
    public byte[] File { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class EntityTypeEntityIdBody {\n");
        sb.Append("  File: ").Append(File).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}