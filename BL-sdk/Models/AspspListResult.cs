using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class AspspListResult
{
    /// <summary>
    ///     Gets or Sets Aspsps
    /// </summary>
    [DataMember(Name = "aspsps", EmitDefaultValue = false)]
    [JsonPropertyName("aspsps")]
    public List<BasicAspspInfo> Aspsps { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class AspspListResult {\n");
        sb.Append("  Aspsps: ").Append(Aspsps).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}