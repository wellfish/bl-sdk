using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class FtgParRow
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    /// <summary>
    ///     Gets or Sets ParText20
    /// </summary>
    [DataMember(Name = "parText20", EmitDefaultValue = false)]
    [JsonPropertyName("parText20")]
    public string ParText20 { get; set; }

    /// <summary>
    ///     Gets or Sets ParText6
    /// </summary>
    [DataMember(Name = "parText6", EmitDefaultValue = false)]
    [JsonPropertyName("parText6")]
    public string ParText6 { get; set; }

    /// <summary>
    ///     Gets or Sets ParLong
    /// </summary>
    [DataMember(Name = "parLong", EmitDefaultValue = false)]
    [JsonPropertyName("parLong")]
    public long? ParLong { get; set; }

    /// <summary>
    ///     Gets or Sets ParCurr
    /// </summary>
    [DataMember(Name = "parCurr", EmitDefaultValue = false)]
    [JsonPropertyName("parCurr")]
    public double? ParCurr { get; set; }

    /// <summary>
    ///     Gets or Sets ParBool
    /// </summary>
    [DataMember(Name = "parBool", EmitDefaultValue = false)]
    [JsonPropertyName("parBool")]
    public bool? ParBool { get; set; }

    /// <summary>
    ///     Gets or Sets ParDouble
    /// </summary>
    [DataMember(Name = "parDouble", EmitDefaultValue = false)]
    [JsonPropertyName("parDouble")]
    public double? ParDouble { get; set; }

    /// <summary>
    ///     Gets or Sets ParDate
    /// </summary>
    [DataMember(Name = "parDate", EmitDefaultValue = false)]
    [JsonPropertyName("parDate")]
    public DateTime? ParDate { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class FtgParRow {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  ParText20: ").Append(ParText20).Append("\n");
        sb.Append("  ParText6: ").Append(ParText6).Append("\n");
        sb.Append("  ParLong: ").Append(ParLong).Append("\n");
        sb.Append("  ParCurr: ").Append(ParCurr).Append("\n");
        sb.Append("  ParBool: ").Append(ParBool).Append("\n");
        sb.Append("  ParDouble: ").Append(ParDouble).Append("\n");
        sb.Append("  ParDate: ").Append(ParDate).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}