using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class ApiError
{
    /// <summary>
    ///     Gets or Sets Status
    /// </summary>
    [DataMember(Name = "status", EmitDefaultValue = false)]
    [JsonPropertyName("status")]
    public string Status { get; set; }

    /// <summary>
    ///     Gets or Sets Timestamp
    /// </summary>
    [DataMember(Name = "timestamp", EmitDefaultValue = false)]
    [JsonPropertyName("timestamp")]
    public DateTime? Timestamp { get; set; }

    /// <summary>
    ///     Gets or Sets Message
    /// </summary>
    [DataMember(Name = "message", EmitDefaultValue = false)]
    [JsonPropertyName("message")]
    public string Message { get; set; }

    /// <summary>
    ///     Gets or Sets DebugMessage
    /// </summary>
    [DataMember(Name = "debugMessage", EmitDefaultValue = false)]
    [JsonPropertyName("debugMessage")]
    public string DebugMessage { get; set; }

    /// <summary>
    ///     Gets or Sets SubErrors
    /// </summary>
    [DataMember(Name = "subErrors", EmitDefaultValue = false)]
    [JsonPropertyName("subErrors")]
    public List<ApiSubError> SubErrors { get; set; }

    /// <summary>
    ///     Gets or Sets TppMessages
    /// </summary>
    [DataMember(Name = "tppMessages", EmitDefaultValue = false)]
    [JsonPropertyName("tppMessages")]
    public List<TppMessage> TppMessages { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class ApiError {\n");
        sb.Append("  Status: ").Append(Status).Append("\n");
        sb.Append("  Timestamp: ").Append(Timestamp).Append("\n");
        sb.Append("  Message: ").Append(Message).Append("\n");
        sb.Append("  DebugMessage: ").Append(DebugMessage).Append("\n");
        sb.Append("  SubErrors: ").Append(SubErrors).Append("\n");
        sb.Append("  TppMessages: ").Append(TppMessages).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}