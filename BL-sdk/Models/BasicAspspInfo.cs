using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class BasicAspspInfo
{
    /// <summary>
    ///     Gets or Sets BicFi
    /// </summary>
    [DataMember(Name = "bicFi", EmitDefaultValue = false)]
    [JsonPropertyName("bicFi")]
    public string BicFi { get; set; }

    /// <summary>
    ///     Gets or Sets Name
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     Gets or Sets LogoUrl
    /// </summary>
    [DataMember(Name = "logoUrl", EmitDefaultValue = false)]
    [JsonPropertyName("logoUrl")]
    public string LogoUrl { get; set; }

    /// <summary>
    ///     Gets or Sets ExtendedData
    /// </summary>
    [DataMember(Name = "extendedData", EmitDefaultValue = false)]
    [JsonPropertyName("extendedData")]
    public ClientAdaptedBankInfo ExtendedData { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class BasicAspspInfo {\n");
        sb.Append("  BicFi: ").Append(BicFi).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  LogoUrl: ").Append(LogoUrl).Append("\n");
        sb.Append("  ExtendedData: ").Append(ExtendedData).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}