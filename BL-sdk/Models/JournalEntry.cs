using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class JournalEntry
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets JournalId
    /// </summary>
    [DataMember(Name = "journalId", EmitDefaultValue = false)]
    [JsonPropertyName("journalId")]
    public string JournalId { get; set; }

    /// <summary>
    ///     Gets or Sets JournalName
    /// </summary>
    [DataMember(Name = "journalName", EmitDefaultValue = false)]
    [JsonPropertyName("journalName")]
    public string JournalName { get; set; }

    /// <summary>
    ///     Gets or Sets JournalEntryId
    /// </summary>
    [DataMember(Name = "journalEntryId", EmitDefaultValue = false)]
    [JsonPropertyName("journalEntryId")]
    public int? JournalEntryId { get; set; }

    /// <summary>
    ///     Gets or Sets JournalEntryDate
    /// </summary>
    [DataMember(Name = "journalEntryDate", EmitDefaultValue = false)]
    [JsonPropertyName("journalEntryDate")]
    public DateTime? JournalEntryDate { get; set; }

    /// <summary>
    ///     Gets or Sets EntryInfoDate
    /// </summary>
    [DataMember(Name = "entryInfoDate", EmitDefaultValue = false)]
    [JsonPropertyName("entryInfoDate")]
    public DateTime? EntryInfoDate { get; set; }

    /// <summary>
    ///     Gets or Sets ReportDate
    /// </summary>
    [DataMember(Name = "reportDate", EmitDefaultValue = false)]
    [JsonPropertyName("reportDate")]
    public DateTime? ReportDate { get; set; }

    /// <summary>
    ///     Gets or Sets EntryInfoBy
    /// </summary>
    [DataMember(Name = "entryInfoBy", EmitDefaultValue = false)]
    [JsonPropertyName("entryInfoBy")]
    public string EntryInfoBy { get; set; }

    /// <summary>
    ///     Gets or Sets FinancialYearId
    /// </summary>
    [DataMember(Name = "financialYearId", EmitDefaultValue = false)]
    [JsonPropertyName("financialYearId")]
    public string FinancialYearId { get; set; }

    /// <summary>
    ///     Gets or Sets LockingInfoDate
    /// </summary>
    [DataMember(Name = "lockingInfoDate", EmitDefaultValue = false)]
    [JsonPropertyName("lockingInfoDate")]
    public DateTime? LockingInfoDate { get; set; }

    /// <summary>
    ///     Gets or Sets LockingInfoBy
    /// </summary>
    [DataMember(Name = "lockingInfoBy", EmitDefaultValue = false)]
    [JsonPropertyName("lockingInfoBy")]
    public object LockingInfoBy { get; set; }

    /// <summary>
    ///     Gets or Sets JournalEntryText
    /// </summary>
    [DataMember(Name = "journalEntryText", EmitDefaultValue = false)]
    [JsonPropertyName("journalEntryText")]
    public string JournalEntryText { get; set; }

    /// <summary>
    ///     Gets or Sets DocumentIds
    /// </summary>
    [DataMember(Name = "documentIds", EmitDefaultValue = false)]
    [JsonPropertyName("documentIds")]
    public List<long> DocumentIds { get; set; } = [];

    /// <summary>
    ///     Gets or Sets LedgerEntries
    /// </summary>
    [DataMember(Name = "ledgerEntries", EmitDefaultValue = false)]
    [JsonPropertyName("ledgerEntries")]
    public List<LedgerEntry> LedgerEntries { get; set; } = [];

    /// <summary>
    ///     Gets or Sets IsEditable
    /// </summary>
    [DataMember(Name = "isEditable", EmitDefaultValue = false)]
    [JsonPropertyName("isEditable")]
    public bool? IsEditable { get; set; }

    /// <summary>
    ///     Gets or Sets IsOpen
    /// </summary>
    [DataMember(Name = "isOpen", EmitDefaultValue = false)]
    [JsonPropertyName("isOpen")]
    public bool? IsOpen { get; set; }

    /// <summary>
    ///     Gets or Sets Origin
    /// </summary>
    [DataMember(Name = "origin", EmitDefaultValue = false)]
    [JsonPropertyName("origin")]
    public string Origin { get; set; }

    /// <summary>
    ///     Gets or Sets AutoRegistered
    /// </summary>
    [DataMember(Name = "autoRegistered", EmitDefaultValue = false)]
    [JsonPropertyName("autoRegistered")]
    public bool? AutoRegistered { get; set; }

    /// <summary>
    ///     End of year
    /// </summary>
    /// <value>End of year</value>
    [DataMember(Name = "eoy", EmitDefaultValue = false)]
    [JsonPropertyName("eoy")]
    public bool? Eoy { get; set; }

    /// <summary>
    ///     Gets or Sets UpsertDate
    /// </summary>
    [DataMember(Name = "upsertDate", EmitDefaultValue = false)]
    [JsonPropertyName("upsertDate")]
    public DateTime? UpsertDate { get; set; }

    /// <summary>
    ///     Gets or Sets ModifiedId
    /// </summary>
    [DataMember(Name = "modifiedId", EmitDefaultValue = false)]
    [JsonPropertyName("modifiedId")]
    public long? ModifiedId { get; set; }

    /// <summary>
    ///     Gets or Sets PreviouslyTemporary
    /// </summary>
    [DataMember(Name = "previouslyTemporary", EmitDefaultValue = false)]
    [JsonPropertyName("previouslyTemporary")]
    public bool? PreviouslyTemporary { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class JournalEntry {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  JournalId: ").Append(JournalId).Append("\n");
        sb.Append("  JournalName: ").Append(JournalName).Append("\n");
        sb.Append("  JournalEntryId: ").Append(JournalEntryId).Append("\n");
        sb.Append("  JournalEntryDate: ").Append(JournalEntryDate).Append("\n");
        sb.Append("  EntryInfoDate: ").Append(EntryInfoDate).Append("\n");
        sb.Append("  ReportDate: ").Append(ReportDate).Append("\n");
        sb.Append("  EntryInfoBy: ").Append(EntryInfoBy).Append("\n");
        sb.Append("  FinancialYearId: ").Append(FinancialYearId).Append("\n");
        sb.Append("  LockingInfoDate: ").Append(LockingInfoDate).Append("\n");
        sb.Append("  LockingInfoBy: ").Append(LockingInfoBy).Append("\n");
        sb.Append("  JournalEntryText: ").Append(JournalEntryText).Append("\n");
        sb.Append("  DocumentIds: ").Append(DocumentIds).Append("\n");
        sb.Append("  LedgerEntries: ").Append(LedgerEntries).Append("\n");
        sb.Append("  IsEditable: ").Append(IsEditable).Append("\n");
        sb.Append("  IsOpen: ").Append(IsOpen).Append("\n");
        sb.Append("  Origin: ").Append(Origin).Append("\n");
        sb.Append("  AutoRegistered: ").Append(AutoRegistered).Append("\n");
        sb.Append("  Eoy: ").Append(Eoy).Append("\n");
        sb.Append("  UpsertDate: ").Append(UpsertDate).Append("\n");
        sb.Append("  ModifiedId: ").Append(ModifiedId).Append("\n");
        sb.Append("  PreviouslyTemporary: ").Append(PreviouslyTemporary).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}