using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Attestant
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "accountingPrivilege", EmitDefaultValue = false)]
    [JsonPropertyName("accountingPrivilege")]
    public bool? AccountingPrivilege { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "cloudUserId", EmitDefaultValue = false)]
    [JsonPropertyName("cloudUserId")]
    public string CloudUserId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "limitedAccountingPrivilege", EmitDefaultValue = false)]
    [JsonPropertyName("limitedAccountingPrivilege")]
    public bool? LimitedAccountingPrivilege { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "nextId", EmitDefaultValue = false)]
    [JsonPropertyName("nextId")]
    public string NextId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "remarks", EmitDefaultValue = false)]
    [JsonPropertyName("remarks")]
    public string Remarks { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "type", EmitDefaultValue = false)]
    [JsonPropertyName("type")]
    public string Type { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Attestant {\n");
        sb.Append("  AccountingPrivilege: ").Append(AccountingPrivilege).Append("\n");
        sb.Append("  CloudUserId: ").Append(CloudUserId).Append("\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  LimitedAccountingPrivilege: ").Append(LimitedAccountingPrivilege).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  NextId: ").Append(NextId).Append("\n");
        sb.Append("  Remarks: ").Append(Remarks).Append("\n");
        sb.Append("  Type: ").Append(Type).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}