using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Document
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public long? Id { get; set; }

    /// <summary>
    ///     Gets or Sets Name
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     Gets or Sets Type
    /// </summary>
    [DataMember(Name = "type", EmitDefaultValue = false)]
    [JsonPropertyName("type")]
    public string Type { get; set; }

    /// <summary>
    ///     Gets or Sets TextId
    /// </summary>
    [DataMember(Name = "textId", EmitDefaultValue = false)]
    [JsonPropertyName("textId")]
    public string TextId { get; set; }

    /// <summary>
    ///     Gets or Sets NumberId
    /// </summary>
    [DataMember(Name = "numberId", EmitDefaultValue = false)]
    [JsonPropertyName("numberId")]
    public int? NumberId { get; set; }

    /// <summary>
    ///     Gets or Sets OwningType
    /// </summary>
    [DataMember(Name = "owningType", EmitDefaultValue = false)]
    [JsonPropertyName("owningType")]
    public string OwningType { get; set; }

    /// <summary>
    ///     Gets or Sets Created
    /// </summary>
    [DataMember(Name = "created", EmitDefaultValue = false)]
    [JsonPropertyName("created")]
    public DateTime? Created { get; set; }

    /// <summary>
    ///     Gets or Sets Reference
    /// </summary>
    [DataMember(Name = "reference", EmitDefaultValue = false)]
    [JsonPropertyName("reference")]
    public string Reference { get; set; }

    /// <summary>
    ///     Gets or Sets Comment
    /// </summary>
    [DataMember(Name = "comment", EmitDefaultValue = false)]
    [JsonPropertyName("comment")]
    public string Comment { get; set; }

    /// <summary>
    ///     Gets or Sets ByCloudUser
    /// </summary>
    [DataMember(Name = "byCloudUser", EmitDefaultValue = false)]
    [JsonPropertyName("byCloudUser")]
    public long? ByCloudUser { get; set; }

    /// <summary>
    ///     Gets or Sets CloudUserName
    /// </summary>
    [DataMember(Name = "cloudUserName", EmitDefaultValue = false)]
    [JsonPropertyName("cloudUserName")]
    public string CloudUserName { get; set; }

    /// <summary>
    ///     Gets or Sets PaymentTypeId
    /// </summary>
    [DataMember(Name = "paymentTypeId", EmitDefaultValue = false)]
    [JsonPropertyName("paymentTypeId")]
    public long? PaymentTypeId { get; set; }

    /// <summary>
    ///     Gets or Sets Properties
    /// </summary>
    [DataMember(Name = "properties", EmitDefaultValue = false)]
    [JsonPropertyName("properties")]
    public List<DocumentProperty> Properties { get; set; }

    /// <summary>
    ///     Gets or Sets Interpretation
    /// </summary>
    [DataMember(Name = "interpretation", EmitDefaultValue = false)]
    [JsonPropertyName("interpretation")]
    public Dictionary<string, object> Interpretation { get; set; }

    /// <summary>
    ///     Gets or Sets Xml
    /// </summary>
    [DataMember(Name = "xml", EmitDefaultValue = false)]
    [JsonPropertyName("xml")]
    public Dictionary<string, JsonNode> Xml { get; set; }

    /// <summary>
    ///     Gets or Sets Data
    /// </summary>
    [DataMember(Name = "data", EmitDefaultValue = false)]
    [JsonPropertyName("data")]
    public List<byte[]> Data { get; set; }

    /// <summary>
    ///     Gets or Sets Image
    /// </summary>
    [DataMember(Name = "image", EmitDefaultValue = false)]
    [JsonPropertyName("image")]
    public bool? Image { get; set; }

    /// <summary>
    ///     Gets or Sets Pdf
    /// </summary>
    [DataMember(Name = "pdf", EmitDefaultValue = false)]
    [JsonPropertyName("pdf")]
    public bool? Pdf { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Document {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  Type: ").Append(Type).Append("\n");
        sb.Append("  TextId: ").Append(TextId).Append("\n");
        sb.Append("  NumberId: ").Append(NumberId).Append("\n");
        sb.Append("  OwningType: ").Append(OwningType).Append("\n");
        sb.Append("  Created: ").Append(Created).Append("\n");
        sb.Append("  Reference: ").Append(Reference).Append("\n");
        sb.Append("  Comment: ").Append(Comment).Append("\n");
        sb.Append("  ByCloudUser: ").Append(ByCloudUser).Append("\n");
        sb.Append("  CloudUserName: ").Append(CloudUserName).Append("\n");
        sb.Append("  PaymentTypeId: ").Append(PaymentTypeId).Append("\n");
        sb.Append("  Properties: ").Append(Properties).Append("\n");
        sb.Append("  Interpretation: ").Append(Interpretation).Append("\n");
        sb.Append("  Xml: ").Append(Xml).Append("\n");
        sb.Append("  Data: ").Append(Data).Append("\n");
        sb.Append("  Image: ").Append(Image).Append("\n");
        sb.Append("  Pdf: ").Append(Pdf).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}