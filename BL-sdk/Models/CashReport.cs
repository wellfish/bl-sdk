using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class CashReport
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Id of the report
    /// </summary>
    /// <value>Id of the report</value>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public long? Id { get; set; }

    /// <summary>
    ///     date for the report
    /// </summary>
    /// <value>date for the report</value>
    [DataMember(Name = "date", EmitDefaultValue = false)]
    [JsonPropertyName("date")]
    public DateTime? Date { get; set; }

    /// <summary>
    ///     Gets or Sets Number
    /// </summary>
    [DataMember(Name = "number", EmitDefaultValue = false)]
    [JsonPropertyName("number")]
    public int? Number { get; set; }

    /// <summary>
    ///     Gets or Sets Type
    /// </summary>
    [DataMember(Name = "type", EmitDefaultValue = false)]
    [JsonPropertyName("type")]
    public string Type { get; set; }

    /// <summary>
    ///     Gets or Sets Account
    /// </summary>
    [DataMember(Name = "account", EmitDefaultValue = false)]
    [JsonPropertyName("account")]
    public string Account { get; set; }

    /// <summary>
    ///     Gets or Sets CostCenter
    /// </summary>
    [DataMember(Name = "costCenter", EmitDefaultValue = false)]
    [JsonPropertyName("costCenter")]
    public string CostCenter { get; set; }

    /// <summary>
    ///     Gets or Sets CostBearer
    /// </summary>
    [DataMember(Name = "costBearer", EmitDefaultValue = false)]
    [JsonPropertyName("costBearer")]
    public string CostBearer { get; set; }

    /// <summary>
    ///     Gets or Sets Project
    /// </summary>
    [DataMember(Name = "project", EmitDefaultValue = false)]
    [JsonPropertyName("project")]
    public string Project { get; set; }

    /// <summary>
    ///     Gets or Sets VatCode
    /// </summary>
    [DataMember(Name = "vatCode", EmitDefaultValue = false)]
    [JsonPropertyName("vatCode")]
    public int? VatCode { get; set; }

    /// <summary>
    ///     Gets or Sets VatAmount
    /// </summary>
    [DataMember(Name = "vatAmount", EmitDefaultValue = false)]
    [JsonPropertyName("vatAmount")]
    public decimal? VatAmount { get; set; }

    /// <summary>
    ///     Gets or Sets Amount
    /// </summary>
    [DataMember(Name = "amount", EmitDefaultValue = false)]
    [JsonPropertyName("amount")]
    public decimal? Amount { get; set; }

    /// <summary>
    ///     Gets or Sets Text
    /// </summary>
    [DataMember(Name = "text", EmitDefaultValue = false)]
    [JsonPropertyName("text")]
    public string Text { get; set; }

    /// <summary>
    ///     Gets or Sets Journal
    /// </summary>
    [DataMember(Name = "journal", EmitDefaultValue = false)]
    [JsonPropertyName("journal")]
    public string Journal { get; set; }

    /// <summary>
    ///     Gets or Sets JournalEntry
    /// </summary>
    [DataMember(Name = "journalEntry", EmitDefaultValue = false)]
    [JsonPropertyName("journalEntry")]
    public int? JournalEntry { get; set; }

    /// <summary>
    ///     Gets or Sets EntryText
    /// </summary>
    [DataMember(Name = "entryText", EmitDefaultValue = false)]
    [JsonPropertyName("entryText")]
    public string EntryText { get; set; }

    /// <summary>
    ///     Gets or Sets Updated
    /// </summary>
    [DataMember(Name = "updated", EmitDefaultValue = false)]
    [JsonPropertyName("updated")]
    public bool? Updated { get; set; }

    /// <summary>
    ///     Gets or Sets Sie
    /// </summary>
    [DataMember(Name = "sie", EmitDefaultValue = false)]
    [JsonPropertyName("sie")]
    public bool? Sie { get; set; }

    /// <summary>
    ///     Gets or Sets Created
    /// </summary>
    [DataMember(Name = "created", EmitDefaultValue = false)]
    [JsonPropertyName("created")]
    public DateTime? Created { get; set; }

    /// <summary>
    ///     Gets or Sets CreatedBy
    /// </summary>
    [DataMember(Name = "createdBy", EmitDefaultValue = false)]
    [JsonPropertyName("createdBy")]
    public string CreatedBy { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class CashReport {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Date: ").Append(Date).Append("\n");
        sb.Append("  Number: ").Append(Number).Append("\n");
        sb.Append("  Type: ").Append(Type).Append("\n");
        sb.Append("  Account: ").Append(Account).Append("\n");
        sb.Append("  CostCenter: ").Append(CostCenter).Append("\n");
        sb.Append("  CostBearer: ").Append(CostBearer).Append("\n");
        sb.Append("  Project: ").Append(Project).Append("\n");
        sb.Append("  VatCode: ").Append(VatCode).Append("\n");
        sb.Append("  VatAmount: ").Append(VatAmount).Append("\n");
        sb.Append("  Amount: ").Append(Amount).Append("\n");
        sb.Append("  Text: ").Append(Text).Append("\n");
        sb.Append("  Journal: ").Append(Journal).Append("\n");
        sb.Append("  JournalEntry: ").Append(JournalEntry).Append("\n");
        sb.Append("  EntryText: ").Append(EntryText).Append("\n");
        sb.Append("  Updated: ").Append(Updated).Append("\n");
        sb.Append("  Sie: ").Append(Sie).Append("\n");
        sb.Append("  Created: ").Append(Created).Append("\n");
        sb.Append("  CreatedBy: ").Append(CreatedBy).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}