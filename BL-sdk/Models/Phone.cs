using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Phone
{
    /// <summary>
    ///     Gets or Sets Label
    /// </summary>
    [DataMember(Name = "label", EmitDefaultValue = false)]
    [JsonPropertyName("label")]
    public string Label { get; set; }

    /// <summary>
    ///     Gets or Sets Number
    /// </summary>
    [DataMember(Name = "number", EmitDefaultValue = false)]
    [JsonPropertyName("number")]
    public string Number { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Phone {\n");
        sb.Append("  Label: ").Append(Label).Append("\n");
        sb.Append("  Number: ").Append(Number).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}