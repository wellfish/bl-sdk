using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Schedule
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets EmployeeId
    /// </summary>
    [DataMember(Name = "employeeId", EmitDefaultValue = false)]
    [JsonPropertyName("employeeId")]
    public string EmployeeId { get; set; }

    /// <summary>
    ///     Gets or Sets Date
    /// </summary>
    [DataMember(Name = "date", EmitDefaultValue = false)]
    [JsonPropertyName("date")]
    public DateTime? Date { get; set; }

    /// <summary>
    ///     Gets or Sets Details
    /// </summary>
    [DataMember(Name = "details", EmitDefaultValue = false)]
    [JsonPropertyName("details")]
    public List<ScheduleDetail> Details { get; set; }

    /// <summary>
    ///     Gets or Sets WeekNo
    /// </summary>
    [DataMember(Name = "weekNo", EmitDefaultValue = false)]
    [JsonPropertyName("weekNo")]
    public int? WeekNo { get; set; }

    /// <summary>
    ///     Gets or Sets Weekday
    /// </summary>
    [DataMember(Name = "weekday", EmitDefaultValue = false)]
    [JsonPropertyName("weekday")]
    public string Weekday { get; set; }

    /// <summary>
    ///     Gets or Sets HolidayCode
    /// </summary>
    [DataMember(Name = "holidayCode", EmitDefaultValue = false)]
    [JsonPropertyName("holidayCode")]
    public string HolidayCode { get; set; }

    /// <summary>
    ///     Gets or Sets WorkingDay
    /// </summary>
    [DataMember(Name = "workingDay", EmitDefaultValue = false)]
    [JsonPropertyName("workingDay")]
    public bool? WorkingDay { get; set; }

    /// <summary>
    ///     Gets or Sets FullDay
    /// </summary>
    [DataMember(Name = "fullDay", EmitDefaultValue = false)]
    [JsonPropertyName("fullDay")]
    public bool? FullDay { get; set; }

    /// <summary>
    ///     Gets or Sets SpecialDay
    /// </summary>
    [DataMember(Name = "specialDay", EmitDefaultValue = false)]
    [JsonPropertyName("specialDay")]
    public bool? SpecialDay { get; set; }

    /// <summary>
    ///     Gets or Sets Hours
    /// </summary>
    [DataMember(Name = "hours", EmitDefaultValue = false)]
    [JsonPropertyName("hours")]
    public double? Hours { get; set; }

    /// <summary>
    ///     Gets or Sets WeeklyWorkingHours
    /// </summary>
    [DataMember(Name = "weeklyWorkingHours", EmitDefaultValue = false)]
    [JsonPropertyName("weeklyWorkingHours")]
    public double? WeeklyWorkingHours { get; set; }

    /// <summary>
    ///     Gets or Sets EmploymentRate
    /// </summary>
    [DataMember(Name = "employmentRate", EmitDefaultValue = false)]
    [JsonPropertyName("employmentRate")]
    public double? EmploymentRate { get; set; }

    /// <summary>
    ///     Gets or Sets Comment
    /// </summary>
    [DataMember(Name = "comment", EmitDefaultValue = false)]
    [JsonPropertyName("comment")]
    public string Comment { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Schedule {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  EmployeeId: ").Append(EmployeeId).Append("\n");
        sb.Append("  Date: ").Append(Date).Append("\n");
        sb.Append("  Details: ").Append(Details).Append("\n");
        sb.Append("  WeekNo: ").Append(WeekNo).Append("\n");
        sb.Append("  Weekday: ").Append(Weekday).Append("\n");
        sb.Append("  HolidayCode: ").Append(HolidayCode).Append("\n");
        sb.Append("  WorkingDay: ").Append(WorkingDay).Append("\n");
        sb.Append("  FullDay: ").Append(FullDay).Append("\n");
        sb.Append("  SpecialDay: ").Append(SpecialDay).Append("\n");
        sb.Append("  Hours: ").Append(Hours).Append("\n");
        sb.Append("  WeeklyWorkingHours: ").Append(WeeklyWorkingHours).Append("\n");
        sb.Append("  EmploymentRate: ").Append(EmploymentRate).Append("\n");
        sb.Append("  Comment: ").Append(Comment).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}