using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Xml;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class AccountBalance
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Id of the account.
    /// </summary>
    /// <value>Id of the account.</value>
    [DataMember(Name = "accountId", EmitDefaultValue = false)]
    [JsonPropertyName("accountId")]
    public string AccountId { get; set; }

    /// <summary>
    ///     Period for the balance.
    /// </summary>
    /// <value>Period for the balance.</value>
    [DataMember(Name = "period", EmitDefaultValue = false)]
    [JsonPropertyName("period")]
    public string Period { get; set; }

    /// <summary>
    ///     Opening balance for the first period
    /// </summary>
    /// <value>Opening balance for the first period</value>
    [DataMember(Name = "openingBalance", EmitDefaultValue = false)]
    [JsonPropertyName("openingBalance")]
    public decimal? OpeningBalance { get; set; }

    /// <summary>
    ///     Gets or Sets Balance
    /// </summary>
    [DataMember(Name = "balance", EmitDefaultValue = false)]
    [JsonPropertyName("balance")]
    public decimal? Balance { get; set; }

    /// <summary>
    ///     Gets or Sets CumulativeBalance
    /// </summary>
    [DataMember(Name = "cumulativeBalance", EmitDefaultValue = false)]
    [JsonPropertyName("cumulativeBalance")]
    public decimal? CumulativeBalance { get; set; }

    /// <summary>
    ///     Gets or Sets Type
    /// </summary>
    [DataMember(Name = "type", EmitDefaultValue = false)]
    [JsonPropertyName("type")]
    public string Type { get; set; }

    /// <summary>
    ///     Gets or Sets Eoy
    /// </summary>
    [DataMember(Name = "eoy", EmitDefaultValue = false)]
    [JsonPropertyName("eoy")]
    public string Eoy { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class AccountBalance {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  AccountId: ").Append(AccountId).Append("\n");
        sb.Append("  Period: ").Append(Period).Append("\n");
        sb.Append("  OpeningBalance: ").Append(OpeningBalance).Append("\n");
        sb.Append("  Balance: ").Append(Balance).Append("\n");
        sb.Append("  CumulativeBalance: ").Append(CumulativeBalance).Append("\n");
        sb.Append("  Type: ").Append(Type).Append("\n");
        sb.Append("  Eoy: ").Append(Eoy).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}