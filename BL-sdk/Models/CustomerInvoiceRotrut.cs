using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class CustomerInvoiceRotrut
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public long? Id { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "type", EmitDefaultValue = false)]
    [JsonPropertyName("type")]
    public int? Type { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "invoiceNumber", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceNumber")]
    public long? InvoiceNumber { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "orderNumber", EmitDefaultValue = false)]
    [JsonPropertyName("orderNumber")]
    public long? OrderNumber { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "customerId", EmitDefaultValue = false)]
    [JsonPropertyName("customerId")]
    public string CustomerId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "customerName", EmitDefaultValue = false)]
    [JsonPropertyName("customerName")]
    public string CustomerName { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "customerOrgNumber", EmitDefaultValue = false)]
    [JsonPropertyName("customerOrgNumber")]
    public string CustomerOrgNumber { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "socialSecurityNumber1", EmitDefaultValue = false)]
    [JsonPropertyName("socialSecurityNumber1")]
    public string SocialSecurityNumber1 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "socialSecurityNumber2", EmitDefaultValue = false)]
    [JsonPropertyName("socialSecurityNumber2")]
    public string SocialSecurityNumber2 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "propertyId", EmitDefaultValue = false)]
    [JsonPropertyName("propertyId")]
    public string PropertyId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "apartmentId", EmitDefaultValue = false)]
    [JsonPropertyName("apartmentId")]
    public string ApartmentId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "address", EmitDefaultValue = false)]
    [JsonPropertyName("address")]
    public string Address { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "zip", EmitDefaultValue = false)]
    [JsonPropertyName("zip")]
    public string Zip { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "city", EmitDefaultValue = false)]
    [JsonPropertyName("city")]
    public string City { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "amount", EmitDefaultValue = false)]
    [JsonPropertyName("amount")]
    public double? Amount { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "amountPerson1", EmitDefaultValue = false)]
    [JsonPropertyName("amountPerson1")]
    public double? AmountPerson1 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "amountPerson2", EmitDefaultValue = false)]
    [JsonPropertyName("amountPerson2")]
    public double? AmountPerson2 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "workCost", EmitDefaultValue = false)]
    [JsonPropertyName("workCost")]
    public double? WorkCost { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "manualCount", EmitDefaultValue = false)]
    [JsonPropertyName("manualCount")]
    public bool? ManualCount { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "forward", EmitDefaultValue = false)]
    [JsonPropertyName("forward")]
    public bool? Forward { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "paymentDate", EmitDefaultValue = false)]
    [JsonPropertyName("paymentDate")]
    public DateTime? PaymentDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "createDate", EmitDefaultValue = false)]
    [JsonPropertyName("createDate")]
    public DateTime? CreateDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "forwardDate", EmitDefaultValue = false)]
    [JsonPropertyName("forwardDate")]
    public DateTime? ForwardDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "otherCosts", EmitDefaultValue = false)]
    [JsonPropertyName("otherCosts")]
    public int? OtherCosts { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "buildHours", EmitDefaultValue = false)]
    [JsonPropertyName("buildHours")]
    public int? BuildHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "buildMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("buildMaterials")]
    public int? BuildMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "electricityHours", EmitDefaultValue = false)]
    [JsonPropertyName("electricityHours")]
    public int? ElectricityHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "electricityMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("electricityMaterials")]
    public int? ElectricityMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "glassHours", EmitDefaultValue = false)]
    [JsonPropertyName("glassHours")]
    public int? GlassHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "glassMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("glassMaterials")]
    public int? GlassMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "soilHours", EmitDefaultValue = false)]
    [JsonPropertyName("soilHours")]
    public int? SoilHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "soilMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("soilMaterials")]
    public int? SoilMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "masonHours", EmitDefaultValue = false)]
    [JsonPropertyName("masonHours")]
    public int? MasonHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "masonMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("masonMaterials")]
    public int? MasonMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "paintHours", EmitDefaultValue = false)]
    [JsonPropertyName("paintHours")]
    public int? PaintHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "paintMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("paintMaterials")]
    public int? PaintMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "plumbingHours", EmitDefaultValue = false)]
    [JsonPropertyName("plumbingHours")]
    public int? PlumbingHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "plumbingMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("plumbingMaterials")]
    public int? PlumbingMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "cleaningHours", EmitDefaultValue = false)]
    [JsonPropertyName("cleaningHours")]
    public int? CleaningHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "cleaningMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("cleaningMaterials")]
    public int? CleaningMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "clothesHours", EmitDefaultValue = false)]
    [JsonPropertyName("clothesHours")]
    public int? ClothesHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "clothesMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("clothesMaterials")]
    public int? ClothesMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "itHours", EmitDefaultValue = false)]
    [JsonPropertyName("itHours")]
    public int? ItHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "itMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("itMaterials")]
    public int? ItMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "snowHours", EmitDefaultValue = false)]
    [JsonPropertyName("snowHours")]
    public int? SnowHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "snowMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("snowMaterials")]
    public int? SnowMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "gardenHours", EmitDefaultValue = false)]
    [JsonPropertyName("gardenHours")]
    public int? GardenHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "gardenMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("gardenMaterials")]
    public int? GardenMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "childCareHours", EmitDefaultValue = false)]
    [JsonPropertyName("childCareHours")]
    public int? ChildCareHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "childCareMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("childCareMaterials")]
    public int? ChildCareMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "otherCareHours", EmitDefaultValue = false)]
    [JsonPropertyName("otherCareHours")]
    public int? OtherCareHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "otherCareMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("otherCareMaterials")]
    public int? OtherCareMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "movingHours", EmitDefaultValue = false)]
    [JsonPropertyName("movingHours")]
    public int? MovingHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "movingMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("movingMaterials")]
    public int? MovingMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "appliancesHours", EmitDefaultValue = false)]
    [JsonPropertyName("appliancesHours")]
    public int? AppliancesHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "appliancesMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("appliancesMaterials")]
    public int? AppliancesMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "washingHours", EmitDefaultValue = false)]
    [JsonPropertyName("washingHours")]
    public int? WashingHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "furnishingHours", EmitDefaultValue = false)]
    [JsonPropertyName("furnishingHours")]
    public int? FurnishingHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "furnishingMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("furnishingMaterials")]
    public int? FurnishingMaterials { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "transportingHours", EmitDefaultValue = false)]
    [JsonPropertyName("transportingHours")]
    public int? TransportingHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "supervisingHours", EmitDefaultValue = false)]
    [JsonPropertyName("supervisingHours")]
    public int? SupervisingHours { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "supervisingMaterials", EmitDefaultValue = false)]
    [JsonPropertyName("supervisingMaterials")]
    public int? SupervisingMaterials { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class CustomerInvoiceRotrut {\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Type: ").Append(Type).Append("\n");
        sb.Append("  InvoiceNumber: ").Append(InvoiceNumber).Append("\n");
        sb.Append("  OrderNumber: ").Append(OrderNumber).Append("\n");
        sb.Append("  CustomerId: ").Append(CustomerId).Append("\n");
        sb.Append("  CustomerName: ").Append(CustomerName).Append("\n");
        sb.Append("  CustomerOrgNumber: ").Append(CustomerOrgNumber).Append("\n");
        sb.Append("  SocialSecurityNumber1: ").Append(SocialSecurityNumber1).Append("\n");
        sb.Append("  SocialSecurityNumber2: ").Append(SocialSecurityNumber2).Append("\n");
        sb.Append("  PropertyId: ").Append(PropertyId).Append("\n");
        sb.Append("  ApartmentId: ").Append(ApartmentId).Append("\n");
        sb.Append("  Address: ").Append(Address).Append("\n");
        sb.Append("  Zip: ").Append(Zip).Append("\n");
        sb.Append("  City: ").Append(City).Append("\n");
        sb.Append("  Amount: ").Append(Amount).Append("\n");
        sb.Append("  AmountPerson1: ").Append(AmountPerson1).Append("\n");
        sb.Append("  AmountPerson2: ").Append(AmountPerson2).Append("\n");
        sb.Append("  WorkCost: ").Append(WorkCost).Append("\n");
        sb.Append("  ManualCount: ").Append(ManualCount).Append("\n");
        sb.Append("  Forward: ").Append(Forward).Append("\n");
        sb.Append("  PaymentDate: ").Append(PaymentDate).Append("\n");
        sb.Append("  CreateDate: ").Append(CreateDate).Append("\n");
        sb.Append("  ForwardDate: ").Append(ForwardDate).Append("\n");
        sb.Append("  OtherCosts: ").Append(OtherCosts).Append("\n");
        sb.Append("  BuildHours: ").Append(BuildHours).Append("\n");
        sb.Append("  BuildMaterials: ").Append(BuildMaterials).Append("\n");
        sb.Append("  ElectricityHours: ").Append(ElectricityHours).Append("\n");
        sb.Append("  ElectricityMaterials: ").Append(ElectricityMaterials).Append("\n");
        sb.Append("  GlassHours: ").Append(GlassHours).Append("\n");
        sb.Append("  GlassMaterials: ").Append(GlassMaterials).Append("\n");
        sb.Append("  SoilHours: ").Append(SoilHours).Append("\n");
        sb.Append("  SoilMaterials: ").Append(SoilMaterials).Append("\n");
        sb.Append("  MasonHours: ").Append(MasonHours).Append("\n");
        sb.Append("  MasonMaterials: ").Append(MasonMaterials).Append("\n");
        sb.Append("  PaintHours: ").Append(PaintHours).Append("\n");
        sb.Append("  PaintMaterials: ").Append(PaintMaterials).Append("\n");
        sb.Append("  PlumbingHours: ").Append(PlumbingHours).Append("\n");
        sb.Append("  PlumbingMaterials: ").Append(PlumbingMaterials).Append("\n");
        sb.Append("  CleaningHours: ").Append(CleaningHours).Append("\n");
        sb.Append("  CleaningMaterials: ").Append(CleaningMaterials).Append("\n");
        sb.Append("  ClothesHours: ").Append(ClothesHours).Append("\n");
        sb.Append("  ClothesMaterials: ").Append(ClothesMaterials).Append("\n");
        sb.Append("  ItHours: ").Append(ItHours).Append("\n");
        sb.Append("  ItMaterials: ").Append(ItMaterials).Append("\n");
        sb.Append("  SnowHours: ").Append(SnowHours).Append("\n");
        sb.Append("  SnowMaterials: ").Append(SnowMaterials).Append("\n");
        sb.Append("  GardenHours: ").Append(GardenHours).Append("\n");
        sb.Append("  GardenMaterials: ").Append(GardenMaterials).Append("\n");
        sb.Append("  ChildCareHours: ").Append(ChildCareHours).Append("\n");
        sb.Append("  ChildCareMaterials: ").Append(ChildCareMaterials).Append("\n");
        sb.Append("  OtherCareHours: ").Append(OtherCareHours).Append("\n");
        sb.Append("  OtherCareMaterials: ").Append(OtherCareMaterials).Append("\n");
        sb.Append("  MovingHours: ").Append(MovingHours).Append("\n");
        sb.Append("  MovingMaterials: ").Append(MovingMaterials).Append("\n");
        sb.Append("  AppliancesHours: ").Append(AppliancesHours).Append("\n");
        sb.Append("  AppliancesMaterials: ").Append(AppliancesMaterials).Append("\n");
        sb.Append("  WashingHours: ").Append(WashingHours).Append("\n");
        sb.Append("  FurnishingHours: ").Append(FurnishingHours).Append("\n");
        sb.Append("  FurnishingMaterials: ").Append(FurnishingMaterials).Append("\n");
        sb.Append("  TransportingHours: ").Append(TransportingHours).Append("\n");
        sb.Append("  SupervisingHours: ").Append(SupervisingHours).Append("\n");
        sb.Append("  SupervisingMaterials: ").Append(SupervisingMaterials).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}