using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class ClientAdaptedBankInfo
{
    /// <summary>
    ///     Gets or Sets SupportsDecoupledSinglePayment
    /// </summary>
    [DataMember(Name = "supportsDecoupledSinglePayment", EmitDefaultValue = false)]
    [JsonPropertyName("supportsDecoupledSinglePayment")]
    public bool? SupportsDecoupledSinglePayment { get; set; }

    /// <summary>
    ///     Gets or Sets SupportsDecoupledSigningBasket
    /// </summary>
    [DataMember(Name = "supportsDecoupledSigningBasket", EmitDefaultValue = false)]
    [JsonPropertyName("supportsDecoupledSigningBasket")]
    public bool? SupportsDecoupledSigningBasket { get; set; }

    /// <summary>
    ///     Gets or Sets SupportsFutureDaySettlement
    /// </summary>
    [DataMember(Name = "supportsFutureDaySettlement", EmitDefaultValue = false)]
    [JsonPropertyName("supportsFutureDaySettlement")]
    public bool? SupportsFutureDaySettlement { get; set; }

    /// <summary>
    ///     Gets or Sets SupportsSigningBaskets
    /// </summary>
    [DataMember(Name = "supportsSigningBaskets", EmitDefaultValue = false)]
    [JsonPropertyName("supportsSigningBaskets")]
    public bool? SupportsSigningBaskets { get; set; }

    /// <summary>
    ///     Gets or Sets DefaultPaymentAuthorizationMethod
    /// </summary>
    [DataMember(Name = "defaultPaymentAuthorizationMethod", EmitDefaultValue = false)]
    [JsonPropertyName("defaultPaymentAuthorizationMethod")]
    public string DefaultPaymentAuthorizationMethod { get; set; }

    /// <summary>
    ///     Gets or Sets ClientIdentificator
    /// </summary>
    [DataMember(Name = "clientIdentificator", EmitDefaultValue = false)]
    [JsonPropertyName("clientIdentificator")]
    public string ClientIdentificator { get; set; }

    /// <summary>
    ///     Gets or Sets DebtorAccountType
    /// </summary>
    [DataMember(Name = "debtorAccountType", EmitDefaultValue = false)]
    [JsonPropertyName("debtorAccountType")]
    public string DebtorAccountType { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class ClientAdaptedBankInfo {\n");
        sb.Append("  SupportsDecoupledSinglePayment: ").Append(SupportsDecoupledSinglePayment).Append("\n");
        sb.Append("  SupportsDecoupledSigningBasket: ").Append(SupportsDecoupledSigningBasket).Append("\n");
        sb.Append("  SupportsFutureDaySettlement: ").Append(SupportsFutureDaySettlement).Append("\n");
        sb.Append("  SupportsSigningBaskets: ").Append(SupportsSigningBaskets).Append("\n");
        sb.Append("  DefaultPaymentAuthorizationMethod: ").Append(DefaultPaymentAuthorizationMethod).Append("\n");
        sb.Append("  ClientIdentificator: ").Append(ClientIdentificator).Append("\n");
        sb.Append("  DebtorAccountType: ").Append(DebtorAccountType).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}