using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class PaymentRequestParam
{
    /// <summary>
    ///     Gets or Sets Amount
    /// </summary>
    [DataMember(Name = "amount", EmitDefaultValue = false)]
    [JsonPropertyName("amount")]
    public decimal? Amount { get; set; }

    /// <summary>
    ///     Gets or Sets ExecutionDate
    /// </summary>
    [DataMember(Name = "executionDate", EmitDefaultValue = false)]
    [JsonPropertyName("executionDate")]
    public DateTime? ExecutionDate { get; set; }

    /// <summary>
    ///     Gets or Sets InvoiceId
    /// </summary>
    [DataMember(Name = "invoiceId", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceId")]
    public long? InvoiceId { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class PaymentRequestParam {\n");
        sb.Append("  Amount: ").Append(Amount).Append("\n");
        sb.Append("  ExecutionDate: ").Append(ExecutionDate).Append("\n");
        sb.Append("  InvoiceId: ").Append(InvoiceId).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}