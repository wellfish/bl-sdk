using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class CustomerRotRut
{
    /// <summary>
    ///     Gets or Sets CustomerId
    /// </summary>
    [DataMember(Name = "customerId", EmitDefaultValue = false)]
    [JsonPropertyName("customerId")]
    public string CustomerId { get; set; }

    /// <summary>
    ///     Gets or Sets PropertyId
    /// </summary>
    [DataMember(Name = "propertyId", EmitDefaultValue = false)]
    [JsonPropertyName("propertyId")]
    public string PropertyId { get; set; }

    /// <summary>
    ///     Gets or Sets ApartmentId
    /// </summary>
    [DataMember(Name = "apartmentId", EmitDefaultValue = false)]
    [JsonPropertyName("apartmentId")]
    public string ApartmentId { get; set; }

    /// <summary>
    ///     Gets or Sets CooperativeId
    /// </summary>
    [DataMember(Name = "cooperativeId", EmitDefaultValue = false)]
    [JsonPropertyName("cooperativeId")]
    public string CooperativeId { get; set; }

    /// <summary>
    ///     Gets or Sets Address
    /// </summary>
    [DataMember(Name = "address", EmitDefaultValue = false)]
    [JsonPropertyName("address")]
    public string Address { get; set; }

    /// <summary>
    ///     Gets or Sets Zip
    /// </summary>
    [DataMember(Name = "zip", EmitDefaultValue = false)]
    [JsonPropertyName("zip")]
    public string Zip { get; set; }

    /// <summary>
    ///     Gets or Sets City
    /// </summary>
    [DataMember(Name = "city", EmitDefaultValue = false)]
    [JsonPropertyName("city")]
    public string City { get; set; }

    /// <summary>
    ///     Gets or Sets SocialSecurityNumber1
    /// </summary>
    [DataMember(Name = "socialSecurityNumber1", EmitDefaultValue = false)]
    [JsonPropertyName("socialSecurityNumber1")]
    public string SocialSecurityNumber1 { get; set; }

    /// <summary>
    ///     Gets or Sets SocialSecurityNumber2
    /// </summary>
    [DataMember(Name = "socialSecurityNumber2", EmitDefaultValue = false)]
    [JsonPropertyName("socialSecurityNumber2")]
    public string SocialSecurityNumber2 { get; set; }

    /// <summary>
    ///     Gets or Sets Percentage1
    /// </summary>
    [DataMember(Name = "percentage1", EmitDefaultValue = false)]
    [JsonPropertyName("percentage1")]
    public int? Percentage1 { get; set; }

    /// <summary>
    ///     Gets or Sets Percentage2
    /// </summary>
    [DataMember(Name = "percentage2", EmitDefaultValue = false)]
    [JsonPropertyName("percentage2")]
    public int? Percentage2 { get; set; }

    /// <summary>
    ///     Gets or Sets EntityId
    /// </summary>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class CustomerRotRut {\n");
        sb.Append("  CustomerId: ").Append(CustomerId).Append("\n");
        sb.Append("  PropertyId: ").Append(PropertyId).Append("\n");
        sb.Append("  ApartmentId: ").Append(ApartmentId).Append("\n");
        sb.Append("  CooperativeId: ").Append(CooperativeId).Append("\n");
        sb.Append("  Address: ").Append(Address).Append("\n");
        sb.Append("  Zip: ").Append(Zip).Append("\n");
        sb.Append("  City: ").Append(City).Append("\n");
        sb.Append("  SocialSecurityNumber1: ").Append(SocialSecurityNumber1).Append("\n");
        sb.Append("  SocialSecurityNumber2: ").Append(SocialSecurityNumber2).Append("\n");
        sb.Append("  Percentage1: ").Append(Percentage1).Append("\n");
        sb.Append("  Percentage2: ").Append(Percentage2).Append("\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}