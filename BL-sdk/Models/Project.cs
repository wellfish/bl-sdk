using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Project
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    /// <summary>
    ///     Gets or Sets Name
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     Gets or Sets Closed
    /// </summary>
    [DataMember(Name = "closed", EmitDefaultValue = false)]
    [JsonPropertyName("closed")]
    public bool? Closed { get; set; }

    /// <summary>
    ///     Gets or Sets Comment
    /// </summary>
    [DataMember(Name = "comment", EmitDefaultValue = false)]
    [JsonPropertyName("comment")]
    public string Comment { get; set; }

    /// <summary>
    ///     Gets or Sets Status
    /// </summary>
    [DataMember(Name = "status", EmitDefaultValue = false)]
    [JsonPropertyName("status")]
    public string Status { get; set; }

    /// <summary>
    ///     Gets or Sets Category
    /// </summary>
    [DataMember(Name = "category", EmitDefaultValue = false)]
    [JsonPropertyName("category")]
    public string Category { get; set; }

    /// <summary>
    ///     Gets or Sets CustomerId
    /// </summary>
    [DataMember(Name = "customerId", EmitDefaultValue = false)]
    [JsonPropertyName("customerId")]
    public string CustomerId { get; set; }

    /// <summary>
    ///     Gets or Sets ResponsiblePerson
    /// </summary>
    [DataMember(Name = "responsiblePerson", EmitDefaultValue = false)]
    [JsonPropertyName("responsiblePerson")]
    public string ResponsiblePerson { get; set; }

    /// <summary>
    ///     Gets or Sets ContactPerson
    /// </summary>
    [DataMember(Name = "contactPerson", EmitDefaultValue = false)]
    [JsonPropertyName("contactPerson")]
    public string ContactPerson { get; set; }

    /// <summary>
    ///     Gets or Sets StartDate
    /// </summary>
    [DataMember(Name = "startDate", EmitDefaultValue = false)]
    [JsonPropertyName("startDate")]
    public DateTime? StartDate { get; set; }

    /// <summary>
    ///     Gets or Sets StopDate
    /// </summary>
    [DataMember(Name = "stopDate", EmitDefaultValue = false)]
    [JsonPropertyName("stopDate")]
    public DateTime? StopDate { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Project {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  Closed: ").Append(Closed).Append("\n");
        sb.Append("  Comment: ").Append(Comment).Append("\n");
        sb.Append("  Status: ").Append(Status).Append("\n");
        sb.Append("  Category: ").Append(Category).Append("\n");
        sb.Append("  CustomerId: ").Append(CustomerId).Append("\n");
        sb.Append("  ResponsiblePerson: ").Append(ResponsiblePerson).Append("\n");
        sb.Append("  ContactPerson: ").Append(ContactPerson).Append("\n");
        sb.Append("  StartDate: ").Append(StartDate).Append("\n");
        sb.Append("  StopDate: ").Append(StopDate).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}