using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Account
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "unit", EmitDefaultValue = false)]
    [JsonPropertyName("unit")]
    public string Unit { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "declareAmount", EmitDefaultValue = false)]
    [JsonPropertyName("declareAmount")]
    public bool? DeclareAmount { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "vatCode", EmitDefaultValue = false)]
    [JsonPropertyName("vatCode")]
    public string VatCode { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "sruCode", EmitDefaultValue = false)]
    [JsonPropertyName("sruCode")]
    public string SruCode { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "sruCode2", EmitDefaultValue = false)]
    [JsonPropertyName("sruCode2")]
    public string SruCode2 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "sruCodeCredit", EmitDefaultValue = false)]
    [JsonPropertyName("sruCodeCredit")]
    public string SruCodeCredit { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "costCenterId", EmitDefaultValue = false)]
    [JsonPropertyName("costCenterId")]
    public string CostCenterId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "costBearerId", EmitDefaultValue = false)]
    [JsonPropertyName("costBearerId")]
    public string CostBearerId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "projectId", EmitDefaultValue = false)]
    [JsonPropertyName("projectId")]
    public string ProjectId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "comment", EmitDefaultValue = false)]
    [JsonPropertyName("comment")]
    public string Comment { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "closed", EmitDefaultValue = false)]
    [JsonPropertyName("closed")]
    public bool? Closed { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "credit", EmitDefaultValue = false)]
    [JsonPropertyName("credit")]
    public int? Credit { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "debit", EmitDefaultValue = false)]
    [JsonPropertyName("debit")]
    public int? Debit { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "field", EmitDefaultValue = false)]
    [JsonPropertyName("field")]
    public string Field { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "type", EmitDefaultValue = false)]
    [JsonPropertyName("type")]
    public string Type { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Account {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  Unit: ").Append(Unit).Append("\n");
        sb.Append("  DeclareAmount: ").Append(DeclareAmount).Append("\n");
        sb.Append("  VatCode: ").Append(VatCode).Append("\n");
        sb.Append("  SruCode: ").Append(SruCode).Append("\n");
        sb.Append("  SruCode2: ").Append(SruCode2).Append("\n");
        sb.Append("  SruCodeCredit: ").Append(SruCodeCredit).Append("\n");
        sb.Append("  CostCenterId: ").Append(CostCenterId).Append("\n");
        sb.Append("  CostBearerId: ").Append(CostBearerId).Append("\n");
        sb.Append("  ProjectId: ").Append(ProjectId).Append("\n");
        sb.Append("  Comment: ").Append(Comment).Append("\n");
        sb.Append("  Closed: ").Append(Closed).Append("\n");
        sb.Append("  Credit: ").Append(Credit).Append("\n");
        sb.Append("  Debit: ").Append(Debit).Append("\n");
        sb.Append("  Field: ").Append(Field).Append("\n");
        sb.Append("  Type: ").Append(Type).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}