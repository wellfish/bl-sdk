using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class BalanceResult
{
    /// <summary>
    ///     Gets or Sets Account
    /// </summary>
    [DataMember(Name = "account", EmitDefaultValue = false)]
    [JsonPropertyName("account")]
    public BankAccount Account { get; set; }

    /// <summary>
    ///     Gets or Sets Balances
    /// </summary>
    [DataMember(Name = "balances", EmitDefaultValue = false)]
    [JsonPropertyName("balances")]
    public List<Balance> Balances { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class BalanceResult {\n");
        sb.Append("  Account: ").Append(Account).Append("\n");
        sb.Append("  Balances: ").Append(Balances).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}