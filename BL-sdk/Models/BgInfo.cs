using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class BgInfo
{
    /// <summary>
    ///     Gets or Sets AggregateId
    /// </summary>
    [DataMember(Name = "aggregateId", EmitDefaultValue = false)]
    [JsonPropertyName("aggregateId")]
    public int? AggregateId { get; set; }

    /// <summary>
    ///     Gets or Sets AggregateAmount
    /// </summary>
    [DataMember(Name = "aggregateAmount", EmitDefaultValue = false)]
    [JsonPropertyName("aggregateAmount")]
    public string AggregateAmount { get; set; }

    /// <summary>
    ///     Gets or Sets DebtorAddress
    /// </summary>
    [DataMember(Name = "debtorAddress", EmitDefaultValue = false)]
    [JsonPropertyName("debtorAddress")]
    public Address DebtorAddress { get; set; }

    /// <summary>
    ///     Gets or Sets TransactionId
    /// </summary>
    [DataMember(Name = "transactionId", EmitDefaultValue = false)]
    [JsonPropertyName("transactionId")]
    public string TransactionId { get; set; }

    /// <summary>
    ///     Gets or Sets EntryReference
    /// </summary>
    [DataMember(Name = "entryReference", EmitDefaultValue = false)]
    [JsonPropertyName("entryReference")]
    public string EntryReference { get; set; }

    /// <summary>
    ///     Gets or Sets BookingDate
    /// </summary>
    [DataMember(Name = "bookingDate", EmitDefaultValue = false)]
    [JsonPropertyName("bookingDate")]
    public DateTime? BookingDate { get; set; }

    /// <summary>
    ///     Gets or Sets TransactionAmount
    /// </summary>
    [DataMember(Name = "transactionAmount", EmitDefaultValue = false)]
    [JsonPropertyName("transactionAmount")]
    public Amount TransactionAmount { get; set; }

    /// <summary>
    ///     Gets or Sets CreditorAccount
    /// </summary>
    [DataMember(Name = "creditorAccount", EmitDefaultValue = false)]
    [JsonPropertyName("creditorAccount")]
    public BankAccount CreditorAccount { get; set; }

    /// <summary>
    ///     Gets or Sets DebtorName
    /// </summary>
    [DataMember(Name = "debtorName", EmitDefaultValue = false)]
    [JsonPropertyName("debtorName")]
    public string DebtorName { get; set; }

    /// <summary>
    ///     Gets or Sets DebtorAccount
    /// </summary>
    [DataMember(Name = "debtorAccount", EmitDefaultValue = false)]
    [JsonPropertyName("debtorAccount")]
    public BankAccount DebtorAccount { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class BgInfo {\n");
        sb.Append("  AggregateId: ").Append(AggregateId).Append("\n");
        sb.Append("  AggregateAmount: ").Append(AggregateAmount).Append("\n");
        sb.Append("  DebtorAddress: ").Append(DebtorAddress).Append("\n");
        sb.Append("  TransactionId: ").Append(TransactionId).Append("\n");
        sb.Append("  EntryReference: ").Append(EntryReference).Append("\n");
        sb.Append("  BookingDate: ").Append(BookingDate).Append("\n");
        sb.Append("  TransactionAmount: ").Append(TransactionAmount).Append("\n");
        sb.Append("  CreditorAccount: ").Append(CreditorAccount).Append("\n");
        sb.Append("  DebtorName: ").Append(DebtorName).Append("\n");
        sb.Append("  DebtorAccount: ").Append(DebtorAccount).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}