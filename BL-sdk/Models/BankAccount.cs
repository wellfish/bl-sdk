using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class BankAccount
{
    /// <summary>
    ///     Gets or Sets CashAccountType
    /// </summary>
    [DataMember(Name = "cashAccountType", EmitDefaultValue = false)]
    [JsonPropertyName("cashAccountType")]
    public string CashAccountType { get; set; }

    /// <summary>
    ///     Gets or Sets Currency
    /// </summary>
    [DataMember(Name = "currency", EmitDefaultValue = false)]
    [JsonPropertyName("currency")]
    public string Currency { get; set; }

    /// <summary>
    ///     Gets or Sets Iban
    /// </summary>
    [DataMember(Name = "iban", EmitDefaultValue = false)]
    [JsonPropertyName("iban")]
    public string Iban { get; set; }

    /// <summary>
    ///     Gets or Sets Bban
    /// </summary>
    [DataMember(Name = "bban", EmitDefaultValue = false)]
    [JsonPropertyName("bban")]
    public string Bban { get; set; }

    /// <summary>
    ///     Gets or Sets Name
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     Gets or Sets Product
    /// </summary>
    [DataMember(Name = "product", EmitDefaultValue = false)]
    [JsonPropertyName("product")]
    public string Product { get; set; }

    /// <summary>
    ///     Gets or Sets ResourceId
    /// </summary>
    [DataMember(Name = "resourceId", EmitDefaultValue = false)]
    [JsonPropertyName("resourceId")]
    public string ResourceId { get; set; }

    /// <summary>
    ///     Gets or Sets ClearingNumber
    /// </summary>
    [DataMember(Name = "clearingNumber", EmitDefaultValue = false)]
    [JsonPropertyName("clearingNumber")]
    public string ClearingNumber { get; set; }

    /// <summary>
    ///     Gets or Sets BankgiroNumber
    /// </summary>
    [DataMember(Name = "bankgiroNumber", EmitDefaultValue = false)]
    [JsonPropertyName("bankgiroNumber")]
    public string BankgiroNumber { get; set; }

    /// <summary>
    ///     Gets or Sets PlusgiroNumber
    /// </summary>
    [DataMember(Name = "plusgiroNumber", EmitDefaultValue = false)]
    [JsonPropertyName("plusgiroNumber")]
    public string PlusgiroNumber { get; set; }

    /// <summary>
    ///     Gets or Sets Msisdn
    /// </summary>
    [DataMember(Name = "msisdn", EmitDefaultValue = false)]
    [JsonPropertyName("msisdn")]
    public string Msisdn { get; set; }

    /// <summary>
    ///     Gets or Sets Bic
    /// </summary>
    [DataMember(Name = "bic", EmitDefaultValue = false)]
    [JsonPropertyName("bic")]
    public string Bic { get; set; }

    /// <summary>
    ///     Gets or Sets Usage
    /// </summary>
    [DataMember(Name = "usage", EmitDefaultValue = false)]
    [JsonPropertyName("usage")]
    public string Usage { get; set; }

    /// <summary>
    ///     Gets or Sets Status
    /// </summary>
    [DataMember(Name = "status", EmitDefaultValue = false)]
    [JsonPropertyName("status")]
    public string Status { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class BankAccount {\n");
        sb.Append("  CashAccountType: ").Append(CashAccountType).Append("\n");
        sb.Append("  Currency: ").Append(Currency).Append("\n");
        sb.Append("  Iban: ").Append(Iban).Append("\n");
        sb.Append("  Bban: ").Append(Bban).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  Product: ").Append(Product).Append("\n");
        sb.Append("  ResourceId: ").Append(ResourceId).Append("\n");
        sb.Append("  ClearingNumber: ").Append(ClearingNumber).Append("\n");
        sb.Append("  BankgiroNumber: ").Append(BankgiroNumber).Append("\n");
        sb.Append("  PlusgiroNumber: ").Append(PlusgiroNumber).Append("\n");
        sb.Append("  Msisdn: ").Append(Msisdn).Append("\n");
        sb.Append("  Bic: ").Append(Bic).Append("\n");
        sb.Append("  Usage: ").Append(Usage).Append("\n");
        sb.Append("  Status: ").Append(Status).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}