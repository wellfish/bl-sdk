using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class EmployeeDeclaration
{
    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    /// <summary>
    ///     Gets or Sets Name
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     Gets or Sets SocialSecurityNumber
    /// </summary>
    [DataMember(Name = "socialSecurityNumber", EmitDefaultValue = false)]
    [JsonPropertyName("socialSecurityNumber")]
    public string SocialSecurityNumber { get; set; }

    /// <summary>
    ///     Gets or Sets SalaryId
    /// </summary>
    [DataMember(Name = "salaryId", EmitDefaultValue = false)]
    [JsonPropertyName("salaryId")]
    public long? SalaryId { get; set; }

    /// <summary>
    ///     Gets or Sets Fields
    /// </summary>
    [DataMember(Name = "fields", EmitDefaultValue = false)]
    [JsonPropertyName("fields")]
    public List<Field> Fields { get; set; }

    /// <summary>
    ///     Gets or Sets EntityId
    /// </summary>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class EmployeeDeclaration {\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  SocialSecurityNumber: ").Append(SocialSecurityNumber).Append("\n");
        sb.Append("  SalaryId: ").Append(SalaryId).Append("\n");
        sb.Append("  Fields: ").Append(Fields).Append("\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}