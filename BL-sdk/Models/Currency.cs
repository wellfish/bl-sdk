using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Currency
{
    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    /// <summary>
    ///     Gets or Sets Buy
    /// </summary>
    [DataMember(Name = "buy", EmitDefaultValue = false)]
    [JsonPropertyName("buy")]
    public double? Buy { get; set; }

    /// <summary>
    ///     Gets or Sets Sell
    /// </summary>
    [DataMember(Name = "sell", EmitDefaultValue = false)]
    [JsonPropertyName("sell")]
    public double? Sell { get; set; }

    /// <summary>
    ///     Gets or Sets EntityId
    /// </summary>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Currency {\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  Buy: ").Append(Buy).Append("\n");
        sb.Append("  Sell: ").Append(Sell).Append("\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}