using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Address
{
    /// <summary>
    ///     Gets or Sets Street
    /// </summary>
    [DataMember(Name = "street", EmitDefaultValue = false)]
    [JsonPropertyName("street")]
    public string Street { get; set; }

    /// <summary>
    ///     Gets or Sets Box
    /// </summary>
    [DataMember(Name = "box", EmitDefaultValue = false)]
    [JsonPropertyName("box")]
    public string Box { get; set; }

    /// <summary>
    ///     Gets or Sets Zip
    /// </summary>
    [DataMember(Name = "zip", EmitDefaultValue = false)]
    [JsonPropertyName("zip")]
    public string Zip { get; set; }

    /// <summary>
    ///     Gets or Sets City
    /// </summary>
    [DataMember(Name = "city", EmitDefaultValue = false)]
    [JsonPropertyName("city")]
    public string City { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Address {\n");
        sb.Append("  Street: ").Append(Street).Append("\n");
        sb.Append("  Box: ").Append(Box).Append("\n");
        sb.Append("  Zip: ").Append(Zip).Append("\n");
        sb.Append("  City: ").Append(City).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}