using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Customer
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets Bg
    /// </summary>
    [DataMember(Name = "bg", EmitDefaultValue = false)]
    [JsonPropertyName("bg")]
    public string Bg { get; set; }

    /// <summary>
    ///     Gets or Sets Box
    /// </summary>
    [DataMember(Name = "box", EmitDefaultValue = false)]
    [JsonPropertyName("box")]
    public string Box { get; set; }

    /// <summary>
    ///     Gets or Sets Category
    /// </summary>
    [DataMember(Name = "category", EmitDefaultValue = false)]
    [JsonPropertyName("category")]
    public string Category { get; set; }

    /// <summary>
    ///     Gets or Sets City
    /// </summary>
    [DataMember(Name = "city", EmitDefaultValue = false)]
    [JsonPropertyName("city")]
    public string City { get; set; }

    /// <summary>
    ///     Gets or Sets Closed
    /// </summary>
    [DataMember(Name = "closed", EmitDefaultValue = false)]
    [JsonPropertyName("closed")]
    public bool? Closed { get; set; }

    /// <summary>
    ///     Gets or Sets Comment
    /// </summary>
    [DataMember(Name = "comment", EmitDefaultValue = false)]
    [JsonPropertyName("comment")]
    public string Comment { get; set; }

    /// <summary>
    ///     Gets or Sets CompanyId
    /// </summary>
    [DataMember(Name = "companyId", EmitDefaultValue = false)]
    [JsonPropertyName("companyId")]
    public string CompanyId { get; set; }

    /// <summary>
    ///     Gets or Sets Country
    /// </summary>
    [DataMember(Name = "country", EmitDefaultValue = false)]
    [JsonPropertyName("country")]
    public string Country { get; set; }

    /// <summary>
    ///     Gets or Sets CountryCode
    /// </summary>
    [DataMember(Name = "countryCode", EmitDefaultValue = false)]
    [JsonPropertyName("countryCode")]
    public string CountryCode { get; set; }

    /// <summary>
    ///     Gets or Sets CreditLimit
    /// </summary>
    [DataMember(Name = "creditLimit", EmitDefaultValue = false)]
    [JsonPropertyName("creditLimit")]
    public double? CreditLimit { get; set; }

    /// <summary>
    ///     Gets or Sets Currency
    /// </summary>
    [DataMember(Name = "currency", EmitDefaultValue = false)]
    [JsonPropertyName("currency")]
    public string Currency { get; set; }

    /// <summary>
    ///     Gets or Sets DeliveryTerms
    /// </summary>
    [DataMember(Name = "deliveryTerms", EmitDefaultValue = false)]
    [JsonPropertyName("deliveryTerms")]
    public string DeliveryTerms { get; set; }

    /// <summary>
    ///     Gets or Sets Department
    /// </summary>
    [DataMember(Name = "department", EmitDefaultValue = false)]
    [JsonPropertyName("department")]
    public string Department { get; set; }

    /// <summary>
    ///     Gets or Sets Email
    /// </summary>
    [DataMember(Name = "email", EmitDefaultValue = false)]
    [JsonPropertyName("email")]
    public string Email { get; set; }

    /// <summary>
    ///     Gets or Sets Fax
    /// </summary>
    [DataMember(Name = "fax", EmitDefaultValue = false)]
    [JsonPropertyName("fax")]
    public string Fax { get; set; }

    /// <summary>
    ///     Gets or Sets Gln
    /// </summary>
    [DataMember(Name = "gln", EmitDefaultValue = false)]
    [JsonPropertyName("gln")]
    public string Gln { get; set; }

    /// <summary>
    ///     Gets or Sets GroupId
    /// </summary>
    [DataMember(Name = "groupId", EmitDefaultValue = false)]
    [JsonPropertyName("groupId")]
    public string GroupId { get; set; }

    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    /// <summary>
    ///     Gets or Sets InterestInvoicingAllowed
    /// </summary>
    [DataMember(Name = "interestInvoicingAllowed", EmitDefaultValue = false)]
    [JsonPropertyName("interestInvoicingAllowed")]
    public bool? InterestInvoicingAllowed { get; set; }

    /// <summary>
    ///     Gets or Sets InvoiceBy
    /// </summary>
    [DataMember(Name = "invoiceBy", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceBy")]
    public string InvoiceBy { get; set; }

    /// <summary>
    ///     Gets or Sets InvoiceTo
    /// </summary>
    [DataMember(Name = "invoiceTo", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceTo")]
    public string InvoiceTo { get; set; }

    /// <summary>
    ///     Gets or Sets InvoiceMethod
    /// </summary>
    [DataMember(Name = "invoiceMethod", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceMethod")]
    public string InvoiceMethod { get; set; }

    /// <summary>
    ///     Gets or Sets Mobile
    /// </summary>
    [DataMember(Name = "mobile", EmitDefaultValue = false)]
    [JsonPropertyName("mobile")]
    public string Mobile { get; set; }

    /// <summary>
    ///     Gets or Sets ModeOfDelivery
    /// </summary>
    [DataMember(Name = "modeOfDelivery", EmitDefaultValue = false)]
    [JsonPropertyName("modeOfDelivery")]
    public string ModeOfDelivery { get; set; }

    /// <summary>
    ///     Gets or Sets Name
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     Gets or Sets NoFreight
    /// </summary>
    [DataMember(Name = "noFreight", EmitDefaultValue = false)]
    [JsonPropertyName("noFreight")]
    public bool? NoFreight { get; set; }

    /// <summary>
    ///     Gets or Sets NoServiceFee
    /// </summary>
    [DataMember(Name = "noServiceFee", EmitDefaultValue = false)]
    [JsonPropertyName("noServiceFee")]
    public bool? NoServiceFee { get; set; }

    /// <summary>
    ///     Gets or Sets NoVat
    /// </summary>
    [DataMember(Name = "noVat", EmitDefaultValue = false)]
    [JsonPropertyName("noVat")]
    public bool? NoVat { get; set; }

    /// <summary>
    ///     Gets or Sets OrganisationNumber
    /// </summary>
    [DataMember(Name = "organisationNumber", EmitDefaultValue = false)]
    [JsonPropertyName("organisationNumber")]
    public string OrganisationNumber { get; set; }

    /// <summary>
    ///     Gets or Sets PaymentTerms
    /// </summary>
    [DataMember(Name = "paymentTerms", EmitDefaultValue = false)]
    [JsonPropertyName("paymentTerms")]
    public string PaymentTerms { get; set; }

    /// <summary>
    ///     Gets or Sets PeppolId
    /// </summary>
    [DataMember(Name = "peppolId", EmitDefaultValue = false)]
    [JsonPropertyName("peppolId")]
    public string PeppolId { get; set; }

    /// <summary>
    ///     Gets or Sets Pg
    /// </summary>
    [DataMember(Name = "pg", EmitDefaultValue = false)]
    [JsonPropertyName("pg")]
    public string Pg { get; set; }

    /// <summary>
    ///     Gets or Sets Phone
    /// </summary>
    [DataMember(Name = "phone", EmitDefaultValue = false)]
    [JsonPropertyName("phone")]
    public string Phone { get; set; }

    /// <summary>
    ///     Gets or Sets Pricelist
    /// </summary>
    [DataMember(Name = "pricelist", EmitDefaultValue = false)]
    [JsonPropertyName("pricelist")]
    public int? Pricelist { get; set; }

    /// <summary>
    ///     Gets or Sets ReceivableAccount
    /// </summary>
    [DataMember(Name = "receivableAccount", EmitDefaultValue = false)]
    [JsonPropertyName("receivableAccount")]
    public string ReceivableAccount { get; set; }

    /// <summary>
    ///     Gets or Sets RemindersAllowed
    /// </summary>
    [DataMember(Name = "remindersAllowed", EmitDefaultValue = false)]
    [JsonPropertyName("remindersAllowed")]
    public bool? RemindersAllowed { get; set; }

    /// <summary>
    ///     Gets or Sets RemindersFeeAllowed
    /// </summary>
    [DataMember(Name = "remindersFeeAllowed", EmitDefaultValue = false)]
    [JsonPropertyName("remindersFeeAllowed")]
    public bool? RemindersFeeAllowed { get; set; }

    /// <summary>
    ///     Gets or Sets ResponsiblePerson
    /// </summary>
    [DataMember(Name = "responsiblePerson", EmitDefaultValue = false)]
    [JsonPropertyName("responsiblePerson")]
    public string ResponsiblePerson { get; set; }

    /// <summary>
    ///     Gets or Sets RoundOff
    /// </summary>
    [DataMember(Name = "roundOff", EmitDefaultValue = false)]
    [JsonPropertyName("roundOff")]
    public bool? RoundOff { get; set; }

    /// <summary>
    ///     Gets or Sets SalesAccount
    /// </summary>
    [DataMember(Name = "salesAccount", EmitDefaultValue = false)]
    [JsonPropertyName("salesAccount")]
    public string SalesAccount { get; set; }

    /// <summary>
    ///     Gets or Sets Street
    /// </summary>
    [DataMember(Name = "street", EmitDefaultValue = false)]
    [JsonPropertyName("street")]
    public string Street { get; set; }

    /// <summary>
    ///     Gets or Sets TotalInvoicedAmount
    /// </summary>
    [DataMember(Name = "totalInvoicedAmount", EmitDefaultValue = false)]
    [JsonPropertyName("totalInvoicedAmount")]
    public double? TotalInvoicedAmount { get; set; }

    /// <summary>
    ///     Gets or Sets VatCode
    /// </summary>
    [DataMember(Name = "vatCode", EmitDefaultValue = false)]
    [JsonPropertyName("vatCode")]
    public int? VatCode { get; set; }

    /// <summary>
    ///     Gets or Sets VatNumber
    /// </summary>
    [DataMember(Name = "vatNumber", EmitDefaultValue = false)]
    [JsonPropertyName("vatNumber")]
    public string VatNumber { get; set; }

    /// <summary>
    ///     Gets or Sets Web
    /// </summary>
    [DataMember(Name = "web", EmitDefaultValue = false)]
    [JsonPropertyName("web")]
    public string Web { get; set; }

    /// <summary>
    ///     Gets or Sets YourReference
    /// </summary>
    [DataMember(Name = "yourReference", EmitDefaultValue = false)]
    [JsonPropertyName("yourReference")]
    public string YourReference { get; set; }

    /// <summary>
    ///     Gets or Sets Zip
    /// </summary>
    [DataMember(Name = "zip", EmitDefaultValue = false)]
    [JsonPropertyName("zip")]
    public string Zip { get; set; }

    /// <summary>
    ///     Gets or Sets CostBearer
    /// </summary>
    [DataMember(Name = "costBearer", EmitDefaultValue = false)]
    [JsonPropertyName("costBearer")]
    public string CostBearer { get; set; }

    /// <summary>
    ///     Gets or Sets CostCenter
    /// </summary>
    [DataMember(Name = "costCenter", EmitDefaultValue = false)]
    [JsonPropertyName("costCenter")]
    public string CostCenter { get; set; }

    /// <summary>
    ///     Gets or Sets Project
    /// </summary>
    [DataMember(Name = "project", EmitDefaultValue = false)]
    [JsonPropertyName("project")]
    public string Project { get; set; }

    /// <summary>
    ///     Gets or Sets Password
    /// </summary>
    [DataMember(Name = "password", EmitDefaultValue = false)]
    [JsonPropertyName("password")]
    public string Password { get; set; }

    /// <summary>
    ///     Gets or Sets AoToEmail
    /// </summary>
    [DataMember(Name = "aoToEmail", EmitDefaultValue = false)]
    [JsonPropertyName("aoToEmail")]
    public bool? AoToEmail { get; set; }

    /// <summary>
    ///     Gets or Sets AgreementInvoice
    /// </summary>
    [DataMember(Name = "agreementInvoice", EmitDefaultValue = false)]
    [JsonPropertyName("agreementInvoice")]
    public string AgreementInvoice { get; set; }

    /// <summary>
    ///     Gets or Sets SortName
    /// </summary>
    [DataMember(Name = "sortName", EmitDefaultValue = false)]
    [JsonPropertyName("sortName")]
    public string SortName { get; set; }

    /// <summary>
    ///     Gets or Sets Invoice
    /// </summary>
    [DataMember(Name = "invoice", EmitDefaultValue = false)]
    [JsonPropertyName("invoice")]
    public string Invoice { get; set; }

    /// <summary>
    ///     Gets or Sets OrderConfirmation
    /// </summary>
    [DataMember(Name = "orderConfirmation", EmitDefaultValue = false)]
    [JsonPropertyName("orderConfirmation")]
    public bool? OrderConfirmation { get; set; }

    /// <summary>
    ///     Gets or Sets CashInvoice
    /// </summary>
    [DataMember(Name = "cashInvoice", EmitDefaultValue = false)]
    [JsonPropertyName("cashInvoice")]
    public bool? CashInvoice { get; set; }

    /// <summary>
    ///     Gets or Sets ConnectedCustomerId
    /// </summary>
    [DataMember(Name = "connectedCustomerId", EmitDefaultValue = false)]
    [JsonPropertyName("connectedCustomerId")]
    public string ConnectedCustomerId { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Customer {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Bg: ").Append(Bg).Append("\n");
        sb.Append("  Box: ").Append(Box).Append("\n");
        sb.Append("  Category: ").Append(Category).Append("\n");
        sb.Append("  City: ").Append(City).Append("\n");
        sb.Append("  Closed: ").Append(Closed).Append("\n");
        sb.Append("  Comment: ").Append(Comment).Append("\n");
        sb.Append("  CompanyId: ").Append(CompanyId).Append("\n");
        sb.Append("  Country: ").Append(Country).Append("\n");
        sb.Append("  CountryCode: ").Append(CountryCode).Append("\n");
        sb.Append("  CreditLimit: ").Append(CreditLimit).Append("\n");
        sb.Append("  Currency: ").Append(Currency).Append("\n");
        sb.Append("  DeliveryTerms: ").Append(DeliveryTerms).Append("\n");
        sb.Append("  Department: ").Append(Department).Append("\n");
        sb.Append("  Email: ").Append(Email).Append("\n");
        sb.Append("  Fax: ").Append(Fax).Append("\n");
        sb.Append("  Gln: ").Append(Gln).Append("\n");
        sb.Append("  GroupId: ").Append(GroupId).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  InterestInvoicingAllowed: ").Append(InterestInvoicingAllowed).Append("\n");
        sb.Append("  InvoiceBy: ").Append(InvoiceBy).Append("\n");
        sb.Append("  InvoiceTo: ").Append(InvoiceTo).Append("\n");
        sb.Append("  InvoiceMethod: ").Append(InvoiceMethod).Append("\n");
        sb.Append("  Mobile: ").Append(Mobile).Append("\n");
        sb.Append("  ModeOfDelivery: ").Append(ModeOfDelivery).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  NoFreight: ").Append(NoFreight).Append("\n");
        sb.Append("  NoServiceFee: ").Append(NoServiceFee).Append("\n");
        sb.Append("  NoVat: ").Append(NoVat).Append("\n");
        sb.Append("  OrganisationNumber: ").Append(OrganisationNumber).Append("\n");
        sb.Append("  PaymentTerms: ").Append(PaymentTerms).Append("\n");
        sb.Append("  PeppolId: ").Append(PeppolId).Append("\n");
        sb.Append("  Pg: ").Append(Pg).Append("\n");
        sb.Append("  Phone: ").Append(Phone).Append("\n");
        sb.Append("  Pricelist: ").Append(Pricelist).Append("\n");
        sb.Append("  ReceivableAccount: ").Append(ReceivableAccount).Append("\n");
        sb.Append("  RemindersAllowed: ").Append(RemindersAllowed).Append("\n");
        sb.Append("  RemindersFeeAllowed: ").Append(RemindersFeeAllowed).Append("\n");
        sb.Append("  ResponsiblePerson: ").Append(ResponsiblePerson).Append("\n");
        sb.Append("  RoundOff: ").Append(RoundOff).Append("\n");
        sb.Append("  SalesAccount: ").Append(SalesAccount).Append("\n");
        sb.Append("  Street: ").Append(Street).Append("\n");
        sb.Append("  TotalInvoicedAmount: ").Append(TotalInvoicedAmount).Append("\n");
        sb.Append("  VatCode: ").Append(VatCode).Append("\n");
        sb.Append("  VatNumber: ").Append(VatNumber).Append("\n");
        sb.Append("  Web: ").Append(Web).Append("\n");
        sb.Append("  YourReference: ").Append(YourReference).Append("\n");
        sb.Append("  Zip: ").Append(Zip).Append("\n");
        sb.Append("  CostBearer: ").Append(CostBearer).Append("\n");
        sb.Append("  CostCenter: ").Append(CostCenter).Append("\n");
        sb.Append("  Project: ").Append(Project).Append("\n");
        sb.Append("  Password: ").Append(Password).Append("\n");
        sb.Append("  AoToEmail: ").Append(AoToEmail).Append("\n");
        sb.Append("  AgreementInvoice: ").Append(AgreementInvoice).Append("\n");
        sb.Append("  SortName: ").Append(SortName).Append("\n");
        sb.Append("  Invoice: ").Append(Invoice).Append("\n");
        sb.Append("  OrderConfirmation: ").Append(OrderConfirmation).Append("\n");
        sb.Append("  CashInvoice: ").Append(CashInvoice).Append("\n");
        sb.Append("  ConnectedCustomerId: ").Append(ConnectedCustomerId).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}