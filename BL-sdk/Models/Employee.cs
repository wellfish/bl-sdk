using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Employee
{
    /// <summary>
    ///     Gets or Sets EntityId
    /// </summary>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public string Id { get; set; }

    /// <summary>
    ///     Gets or Sets SocialSecurityNumber
    /// </summary>
    [DataMember(Name = "socialSecurityNumber", EmitDefaultValue = false)]
    [JsonPropertyName("socialSecurityNumber")]
    public string SocialSecurityNumber { get; set; }

    /// <summary>
    ///     Gets or Sets Comments
    /// </summary>
    [DataMember(Name = "comments", EmitDefaultValue = false)]
    [JsonPropertyName("comments")]
    public string Comments { get; set; }

    /// <summary>
    ///     Gets or Sets Name
    /// </summary>
    [DataMember(Name = "name", EmitDefaultValue = false)]
    [JsonPropertyName("name")]
    public string Name { get; set; }

    /// <summary>
    ///     Gets or Sets EmploymentStart
    /// </summary>
    [DataMember(Name = "employmentStart", EmitDefaultValue = false)]
    [JsonPropertyName("employmentStart")]
    public DateTime? EmploymentStart { get; set; }

    /// <summary>
    ///     Gets or Sets EmploymentEnd
    /// </summary>
    [DataMember(Name = "employmentEnd", EmitDefaultValue = false)]
    [JsonPropertyName("employmentEnd")]
    public DateTime? EmploymentEnd { get; set; }

    /// <summary>
    ///     Gets or Sets PaymentMethod
    /// </summary>
    [DataMember(Name = "paymentMethod", EmitDefaultValue = false)]
    [JsonPropertyName("paymentMethod")]
    public string PaymentMethod { get; set; }

    /// <summary>
    ///     Gets or Sets BankAccount
    /// </summary>
    [DataMember(Name = "bankAccount", EmitDefaultValue = false)]
    [JsonPropertyName("bankAccount")]
    public string BankAccount { get; set; }

    /// <summary>
    ///     Gets or Sets MonthlyWage
    /// </summary>
    [DataMember(Name = "monthlyWage", EmitDefaultValue = false)]
    [JsonPropertyName("monthlyWage")]
    public decimal? MonthlyWage { get; set; }

    /// <summary>
    ///     Gets or Sets HourlyWage
    /// </summary>
    [DataMember(Name = "hourlyWage", EmitDefaultValue = false)]
    [JsonPropertyName("hourlyWage")]
    public decimal? HourlyWage { get; set; }

    /// <summary>
    ///     Gets or Sets PaymentPeriod
    /// </summary>
    [DataMember(Name = "paymentPeriod", EmitDefaultValue = false)]
    [JsonPropertyName("paymentPeriod")]
    public int? PaymentPeriod { get; set; }

    /// <summary>
    ///     Gets or Sets TaxationTable
    /// </summary>
    [DataMember(Name = "taxationTable", EmitDefaultValue = false)]
    [JsonPropertyName("taxationTable")]
    public double? TaxationTable { get; set; }

    /// <summary>
    ///     Gets or Sets WeeklyWorkingHours
    /// </summary>
    [DataMember(Name = "weeklyWorkingHours", EmitDefaultValue = false)]
    [JsonPropertyName("weeklyWorkingHours")]
    public double? WeeklyWorkingHours { get; set; }

    /// <summary>
    ///     Gets or Sets TaxCalculation
    /// </summary>
    [DataMember(Name = "taxCalculation", EmitDefaultValue = false)]
    [JsonPropertyName("taxCalculation")]
    public bool? TaxCalculation { get; set; }

    /// <summary>
    ///     Gets or Sets UsePayrollTax
    /// </summary>
    [DataMember(Name = "usePayrollTax", EmitDefaultValue = false)]
    [JsonPropertyName("usePayrollTax")]
    public bool? UsePayrollTax { get; set; }

    /// <summary>
    ///     Gets or Sets EstimatedAnnualIncome
    /// </summary>
    [DataMember(Name = "estimatedAnnualIncome", EmitDefaultValue = false)]
    [JsonPropertyName("estimatedAnnualIncome")]
    public decimal? EstimatedAnnualIncome { get; set; }

    /// <summary>
    ///     Gets or Sets PrintToEmail
    /// </summary>
    [DataMember(Name = "printToEmail", EmitDefaultValue = false)]
    [JsonPropertyName("printToEmail")]
    public bool? PrintToEmail { get; set; }

    /// <summary>
    ///     Gets or Sets Email
    /// </summary>
    [DataMember(Name = "email", EmitDefaultValue = false)]
    [JsonPropertyName("email")]
    public string Email { get; set; }

    /// <summary>
    ///     Gets or Sets Email2
    /// </summary>
    [DataMember(Name = "email2", EmitDefaultValue = false)]
    [JsonPropertyName("email2")]
    public string Email2 { get; set; }

    /// <summary>
    ///     Gets or Sets YearlyWorkingHours
    /// </summary>
    [DataMember(Name = "yearlyWorkingHours", EmitDefaultValue = false)]
    [JsonPropertyName("yearlyWorkingHours")]
    public double? YearlyWorkingHours { get; set; }

    /// <summary>
    ///     Gets or Sets BornIn21stCentury
    /// </summary>
    [DataMember(Name = "bornIn21stCentury", EmitDefaultValue = false)]
    [JsonPropertyName("bornIn21stCentury")]
    public bool? BornIn21stCentury { get; set; }

    /// <summary>
    ///     Gets or Sets HolidayAgreementId
    /// </summary>
    [DataMember(Name = "holidayAgreementId", EmitDefaultValue = false)]
    [JsonPropertyName("holidayAgreementId")]
    public long? HolidayAgreementId { get; set; }

    /// <summary>
    ///     Gets or Sets SalaryAgreementId
    /// </summary>
    [DataMember(Name = "salaryAgreementId", EmitDefaultValue = false)]
    [JsonPropertyName("salaryAgreementId")]
    public long? SalaryAgreementId { get; set; }

    /// <summary>
    ///     Gets or Sets FullTimeSalary
    /// </summary>
    [DataMember(Name = "fullTimeSalary", EmitDefaultValue = false)]
    [JsonPropertyName("fullTimeSalary")]
    public decimal? FullTimeSalary { get; set; }

    /// <summary>
    ///     Gets or Sets Adjustment
    /// </summary>
    [DataMember(Name = "adjustment", EmitDefaultValue = false)]
    [JsonPropertyName("adjustment")]
    public Adjustment Adjustment { get; set; }

    /// <summary>
    ///     Gets or Sets Address
    /// </summary>
    [DataMember(Name = "address", EmitDefaultValue = false)]
    [JsonPropertyName("address")]
    public Address Address { get; set; }

    /// <summary>
    ///     Gets or Sets Phones
    /// </summary>
    [DataMember(Name = "phones", EmitDefaultValue = false)]
    [JsonPropertyName("phones")]
    public List<Phone> Phones { get; set; }

    /// <summary>
    ///     Gets or Sets Sink
    /// </summary>
    [DataMember(Name = "sink", EmitDefaultValue = false)]
    [JsonPropertyName("sink")]
    public bool? Sink { get; set; }

    /// <summary>
    ///     Gets or Sets CostBearer
    /// </summary>
    [DataMember(Name = "costBearer", EmitDefaultValue = false)]
    [JsonPropertyName("costBearer")]
    public string CostBearer { get; set; }

    /// <summary>
    ///     Gets or Sets CostCenter
    /// </summary>
    [DataMember(Name = "costCenter", EmitDefaultValue = false)]
    [JsonPropertyName("costCenter")]
    public string CostCenter { get; set; }

    /// <summary>
    ///     Gets or Sets Project
    /// </summary>
    [DataMember(Name = "project", EmitDefaultValue = false)]
    [JsonPropertyName("project")]
    public string Project { get; set; }

    /// <summary>
    ///     Gets or Sets Category
    /// </summary>
    [DataMember(Name = "category", EmitDefaultValue = false)]
    [JsonPropertyName("category")]
    public int? Category { get; set; }

    /// <summary>
    ///     Gets or Sets AccountForSalary
    /// </summary>
    [DataMember(Name = "accountForSalary", EmitDefaultValue = false)]
    [JsonPropertyName("accountForSalary")]
    public string AccountForSalary { get; set; }

    /// <summary>
    ///     Gets or Sets CompanyCarCode
    /// </summary>
    [DataMember(Name = "companyCarCode", EmitDefaultValue = false)]
    [JsonPropertyName("companyCarCode")]
    public string CompanyCarCode { get; set; }

    /// <summary>
    ///     Gets or Sets CompanyOwner
    /// </summary>
    [DataMember(Name = "companyOwner", EmitDefaultValue = false)]
    [JsonPropertyName("companyOwner")]
    public bool? CompanyOwner { get; set; }

    /// <summary>
    ///     Gets or Sets CompanySupportExcemption
    /// </summary>
    [DataMember(Name = "companySupportExcemption", EmitDefaultValue = false)]
    [JsonPropertyName("companySupportExcemption")]
    public bool? CompanySupportExcemption { get; set; }

    /// <summary>
    ///     Gets or Sets UnionDues
    /// </summary>
    [DataMember(Name = "unionDues", EmitDefaultValue = false)]
    [JsonPropertyName("unionDues")]
    public double? UnionDues { get; set; }

    /// <summary>
    ///     Gets or Sets HasUnionDues
    /// </summary>
    [DataMember(Name = "hasUnionDues", EmitDefaultValue = false)]
    [JsonPropertyName("hasUnionDues")]
    public bool? HasUnionDues { get; set; }

    /// <summary>
    ///     Gets or Sets Asink
    /// </summary>
    [DataMember(Name = "asink", EmitDefaultValue = false)]
    [JsonPropertyName("asink")]
    public bool? Asink { get; set; }

    /// <summary>
    ///     Gets or Sets AsinkDescription
    /// </summary>
    [DataMember(Name = "asinkDescription", EmitDefaultValue = false)]
    [JsonPropertyName("asinkDescription")]
    public string AsinkDescription { get; set; }

    /// <summary>
    ///     Gets or Sets ExcludeFromTaxDeductionInquiry
    /// </summary>
    [DataMember(Name = "excludeFromTaxDeductionInquiry", EmitDefaultValue = false)]
    [JsonPropertyName("excludeFromTaxDeductionInquiry")]
    public bool? ExcludeFromTaxDeductionInquiry { get; set; }

    /// <summary>
    ///     Gets or Sets SortName
    /// </summary>
    [DataMember(Name = "sortName", EmitDefaultValue = false)]
    [JsonPropertyName("sortName")]
    public string SortName { get; set; }

    /// <summary>
    ///     Gets or Sets TaxIdentificationNumberCountryCode
    /// </summary>
    [DataMember(Name = "taxIdentificationNumberCountryCode", EmitDefaultValue = false)]
    [JsonPropertyName("taxIdentificationNumberCountryCode")]
    public string TaxIdentificationNumberCountryCode { get; set; }

    /// <summary>
    ///     Gets or Sets TaxIdentificationNumber
    /// </summary>
    [DataMember(Name = "taxIdentificationNumber", EmitDefaultValue = false)]
    [JsonPropertyName("taxIdentificationNumber")]
    public string TaxIdentificationNumber { get; set; }

    /// <summary>
    ///     Gets or Sets SpecificEmployerTaxDeduction
    /// </summary>
    [DataMember(Name = "specificEmployerTaxDeduction", EmitDefaultValue = false)]
    [JsonPropertyName("specificEmployerTaxDeduction")]
    public bool? SpecificEmployerTaxDeduction { get; set; }

    /// <summary>
    ///     Gets or Sets WorkplaceNumber
    /// </summary>
    [DataMember(Name = "workplaceNumber", EmitDefaultValue = false)]
    [JsonPropertyName("workplaceNumber")]
    public string WorkplaceNumber { get; set; }

    /// <summary>
    ///     Gets or Sets Type
    /// </summary>
    [DataMember(Name = "type", EmitDefaultValue = false)]
    [JsonPropertyName("type")]
    public string Type { get; set; }

    /// <summary>
    ///     Gets or Sets PaymentMethodAsInteger
    /// </summary>
    [DataMember(Name = "paymentMethodAsInteger", EmitDefaultValue = false)]
    [JsonPropertyName("paymentMethodAsInteger")]
    public int? PaymentMethodAsInteger { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Employee {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  SocialSecurityNumber: ").Append(SocialSecurityNumber).Append("\n");
        sb.Append("  Comments: ").Append(Comments).Append("\n");
        sb.Append("  Name: ").Append(Name).Append("\n");
        sb.Append("  EmploymentStart: ").Append(EmploymentStart).Append("\n");
        sb.Append("  EmploymentEnd: ").Append(EmploymentEnd).Append("\n");
        sb.Append("  PaymentMethod: ").Append(PaymentMethod).Append("\n");
        sb.Append("  BankAccount: ").Append(BankAccount).Append("\n");
        sb.Append("  MonthlyWage: ").Append(MonthlyWage).Append("\n");
        sb.Append("  HourlyWage: ").Append(HourlyWage).Append("\n");
        sb.Append("  PaymentPeriod: ").Append(PaymentPeriod).Append("\n");
        sb.Append("  TaxationTable: ").Append(TaxationTable).Append("\n");
        sb.Append("  WeeklyWorkingHours: ").Append(WeeklyWorkingHours).Append("\n");
        sb.Append("  TaxCalculation: ").Append(TaxCalculation).Append("\n");
        sb.Append("  UsePayrollTax: ").Append(UsePayrollTax).Append("\n");
        sb.Append("  EstimatedAnnualIncome: ").Append(EstimatedAnnualIncome).Append("\n");
        sb.Append("  PrintToEmail: ").Append(PrintToEmail).Append("\n");
        sb.Append("  Email: ").Append(Email).Append("\n");
        sb.Append("  Email2: ").Append(Email2).Append("\n");
        sb.Append("  YearlyWorkingHours: ").Append(YearlyWorkingHours).Append("\n");
        sb.Append("  BornIn21stCentury: ").Append(BornIn21stCentury).Append("\n");
        sb.Append("  HolidayAgreementId: ").Append(HolidayAgreementId).Append("\n");
        sb.Append("  SalaryAgreementId: ").Append(SalaryAgreementId).Append("\n");
        sb.Append("  FullTimeSalary: ").Append(FullTimeSalary).Append("\n");
        sb.Append("  Adjustment: ").Append(Adjustment).Append("\n");
        sb.Append("  Address: ").Append(Address).Append("\n");
        sb.Append("  Phones: ").Append(Phones).Append("\n");
        sb.Append("  Sink: ").Append(Sink).Append("\n");
        sb.Append("  CostBearer: ").Append(CostBearer).Append("\n");
        sb.Append("  CostCenter: ").Append(CostCenter).Append("\n");
        sb.Append("  Project: ").Append(Project).Append("\n");
        sb.Append("  Category: ").Append(Category).Append("\n");
        sb.Append("  AccountForSalary: ").Append(AccountForSalary).Append("\n");
        sb.Append("  CompanyCarCode: ").Append(CompanyCarCode).Append("\n");
        sb.Append("  CompanyOwner: ").Append(CompanyOwner).Append("\n");
        sb.Append("  CompanySupportExcemption: ").Append(CompanySupportExcemption).Append("\n");
        sb.Append("  UnionDues: ").Append(UnionDues).Append("\n");
        sb.Append("  HasUnionDues: ").Append(HasUnionDues).Append("\n");
        sb.Append("  Asink: ").Append(Asink).Append("\n");
        sb.Append("  AsinkDescription: ").Append(AsinkDescription).Append("\n");
        sb.Append("  ExcludeFromTaxDeductionInquiry: ").Append(ExcludeFromTaxDeductionInquiry).Append("\n");
        sb.Append("  SortName: ").Append(SortName).Append("\n");
        sb.Append("  TaxIdentificationNumberCountryCode: ").Append(TaxIdentificationNumberCountryCode).Append("\n");
        sb.Append("  TaxIdentificationNumber: ").Append(TaxIdentificationNumber).Append("\n");
        sb.Append("  SpecificEmployerTaxDeduction: ").Append(SpecificEmployerTaxDeduction).Append("\n");
        sb.Append("  WorkplaceNumber: ").Append(WorkplaceNumber).Append("\n");
        sb.Append("  Type: ").Append(Type).Append("\n");
        sb.Append("  PaymentMethodAsInteger: ").Append(PaymentMethodAsInteger).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}