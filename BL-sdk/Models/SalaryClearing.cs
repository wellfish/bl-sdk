using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class SalaryClearing
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "entityId", EmitDefaultValue = false)]
    [JsonPropertyName("entityId")]
    public long? EntityId { get; set; }

    /// <summary>
    ///     Gets or Sets Id
    /// </summary>
    [DataMember(Name = "id", EmitDefaultValue = false)]
    [JsonPropertyName("id")]
    public long? Id { get; set; }

    /// <summary>
    ///     Gets or Sets EmployeeId
    /// </summary>
    [DataMember(Name = "employeeId", EmitDefaultValue = false)]
    [JsonPropertyName("employeeId")]
    public string EmployeeId { get; set; }

    /// <summary>
    ///     Gets or Sets PaymentTypeId
    /// </summary>
    [DataMember(Name = "paymentTypeId", EmitDefaultValue = false)]
    [JsonPropertyName("paymentTypeId")]
    public string PaymentTypeId { get; set; }

    /// <summary>
    ///     Gets or Sets Amount
    /// </summary>
    [DataMember(Name = "amount", EmitDefaultValue = false)]
    [JsonPropertyName("amount")]
    public double? Amount { get; set; }

    /// <summary>
    ///     Gets or Sets Updated
    /// </summary>
    [DataMember(Name = "updated", EmitDefaultValue = false)]
    [JsonPropertyName("updated")]
    public bool? Updated { get; set; }

    /// <summary>
    ///     Gets or Sets CostBearer
    /// </summary>
    [DataMember(Name = "costBearer", EmitDefaultValue = false)]
    [JsonPropertyName("costBearer")]
    public string CostBearer { get; set; }

    /// <summary>
    ///     Gets or Sets CostCenter
    /// </summary>
    [DataMember(Name = "costCenter", EmitDefaultValue = false)]
    [JsonPropertyName("costCenter")]
    public string CostCenter { get; set; }

    /// <summary>
    ///     Gets or Sets Project
    /// </summary>
    [DataMember(Name = "project", EmitDefaultValue = false)]
    [JsonPropertyName("project")]
    public string Project { get; set; }

    /// <summary>
    ///     Gets or Sets PricePerUnit
    /// </summary>
    [DataMember(Name = "pricePerUnit", EmitDefaultValue = false)]
    [JsonPropertyName("pricePerUnit")]
    public decimal? PricePerUnit { get; set; }

    /// <summary>
    ///     Gets or Sets Text
    /// </summary>
    [DataMember(Name = "text", EmitDefaultValue = false)]
    [JsonPropertyName("text")]
    public string Text { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class SalaryClearing {\n");
        sb.Append("  EntityId: ").Append(EntityId).Append("\n");
        sb.Append("  Id: ").Append(Id).Append("\n");
        sb.Append("  EmployeeId: ").Append(EmployeeId).Append("\n");
        sb.Append("  PaymentTypeId: ").Append(PaymentTypeId).Append("\n");
        sb.Append("  Amount: ").Append(Amount).Append("\n");
        sb.Append("  Updated: ").Append(Updated).Append("\n");
        sb.Append("  CostBearer: ").Append(CostBearer).Append("\n");
        sb.Append("  CostCenter: ").Append(CostCenter).Append("\n");
        sb.Append("  Project: ").Append(Project).Append("\n");
        sb.Append("  PricePerUnit: ").Append(PricePerUnit).Append("\n");
        sb.Append("  Text: ").Append(Text).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}