using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class DocumentProperty
{
    /// <summary>
    ///     Gets or Sets Key
    /// </summary>
    [DataMember(Name = "key", EmitDefaultValue = false)]
    [JsonPropertyName("key")]
    public string Key { get; set; }

    /// <summary>
    ///     Gets or Sets Value
    /// </summary>
    [DataMember(Name = "value", EmitDefaultValue = false)]
    [JsonPropertyName("value")]
    public string Value { get; set; }

    /// <summary>
    ///     Gets or Sets AsBoolean
    /// </summary>
    [DataMember(Name = "asBoolean", EmitDefaultValue = false)]
    [JsonPropertyName("asBoolean")]
    public bool? AsBoolean { get; set; }

    /// <summary>
    ///     Gets or Sets AsLocalTime
    /// </summary>
    [DataMember(Name = "asLocalTime", EmitDefaultValue = false)]
    [JsonPropertyName("asLocalTime")]
    public LocalTime AsLocalTime { get; set; }

    /// <summary>
    ///     Gets or Sets AsInteger
    /// </summary>
    [DataMember(Name = "asInteger", EmitDefaultValue = false)]
    [JsonPropertyName("asInteger")]
    public int? AsInteger { get; set; }

    /// <summary>
    ///     Gets or Sets AsLocalDate
    /// </summary>
    [DataMember(Name = "asLocalDate", EmitDefaultValue = false)]
    [JsonPropertyName("asLocalDate")]
    public DateTime? AsLocalDate { get; set; }

    /// <summary>
    ///     Gets or Sets AsLocalDateTime
    /// </summary>
    [DataMember(Name = "asLocalDateTime", EmitDefaultValue = false)]
    [JsonPropertyName("asLocalDateTime")]
    public DateTime? AsLocalDateTime { get; set; }

    /// <summary>
    ///     Gets or Sets AsDouble
    /// </summary>
    [DataMember(Name = "asDouble", EmitDefaultValue = false)]
    [JsonPropertyName("asDouble")]
    public double? AsDouble { get; set; }

    /// <summary>
    ///     Gets or Sets AsLong
    /// </summary>
    [DataMember(Name = "asLong", EmitDefaultValue = false)]
    [JsonPropertyName("asLong")]
    public long? AsLong { get; set; }

    /// <summary>
    ///     Gets or Sets AsBigDecimal
    /// </summary>
    [DataMember(Name = "asBigDecimal", EmitDefaultValue = false)]
    [JsonPropertyName("asBigDecimal")]
    public decimal? AsBigDecimal { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class DocumentProperty {\n");
        sb.Append("  Key: ").Append(Key).Append("\n");
        sb.Append("  Value: ").Append(Value).Append("\n");
        sb.Append("  AsBoolean: ").Append(AsBoolean).Append("\n");
        sb.Append("  AsLocalTime: ").Append(AsLocalTime).Append("\n");
        sb.Append("  AsInteger: ").Append(AsInteger).Append("\n");
        sb.Append("  AsLocalDate: ").Append(AsLocalDate).Append("\n");
        sb.Append("  AsLocalDateTime: ").Append(AsLocalDateTime).Append("\n");
        sb.Append("  AsDouble: ").Append(AsDouble).Append("\n");
        sb.Append("  AsLong: ").Append(AsLong).Append("\n");
        sb.Append("  AsBigDecimal: ").Append(AsBigDecimal).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}