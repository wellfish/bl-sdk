using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
///     New customer invoice
/// </summary>
[DataContract]
public sealed class NewCustomerInvoice
{
    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "amountInOriginalCurrency", EmitDefaultValue = false)]
    [JsonPropertyName("amountInOriginalCurrency")]
    public decimal? AmountInOriginalCurrency { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "amountInLocalCurrency", EmitDefaultValue = false)]
    [JsonPropertyName("amountInLocalCurrency")]
    public decimal? AmountInLocalCurrency { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "amountPaidInOriginalCurrency", EmitDefaultValue = false)]
    [JsonPropertyName("amountPaidInOriginalCurrency")]
    public decimal? AmountPaidInOriginalCurrency { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "amountPaidInLocalCurrency", EmitDefaultValue = false)]
    [JsonPropertyName("amountPaidInLocalCurrency")]
    public decimal? AmountPaidInLocalCurrency { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "connectedInvoices", EmitDefaultValue = false)]
    [JsonPropertyName("connectedInvoices")]
    public List<long?> ConnectedInvoices { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "costBearerId", EmitDefaultValue = false)]
    [JsonPropertyName("costBearerId")]
    public string CostBearerId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "costCenterId", EmitDefaultValue = false)]
    [JsonPropertyName("costCenterId")]
    public string CostCenterId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "createdWith", EmitDefaultValue = false)]
    [JsonPropertyName("createdWith")]
    public string CreatedWith { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "currency", EmitDefaultValue = false)]
    [JsonPropertyName("currency")]
    public string Currency { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "customerId", EmitDefaultValue = false)]
    [JsonPropertyName("customerId")]
    public string CustomerId { get; set; }

    /// <summary>
    ///     ___<BR /> aggregated property from associated order
    /// </summary>
    /// <value>___<BR /> aggregated property from associated order</value>
    [DataMember(Name = "customerName", EmitDefaultValue = false)]
    [JsonPropertyName("customerName")]
    public string CustomerName { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "dateOfLatestPayment", EmitDefaultValue = false)]
    [JsonPropertyName("dateOfLatestPayment")]
    public DateTime? DateOfLatestPayment { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "documentIds", EmitDefaultValue = false)]
    [JsonPropertyName("documentIds")]
    public List<long>? DocumentIds { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "dueDate", EmitDefaultValue = false)]
    [JsonPropertyName("dueDate")]
    public DateTime? DueDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "exchangeRate", EmitDefaultValue = false)]
    [JsonPropertyName("exchangeRate")]
    public double? ExchangeRate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "externalInvoiceId", EmitDefaultValue = false)]
    [JsonPropertyName("externalInvoiceId")]
    public long? ExternalInvoiceId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "invoiceNumber", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceNumber")]
    public long? InvoiceNumber { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "invoiceDate", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceDate")]
    public DateTime? InvoiceDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "invoiceDocuments", EmitDefaultValue = false)]
    [JsonPropertyName("invoiceDocuments")]
    public List<Dictionary<string, object>> InvoiceDocuments { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "journalId", EmitDefaultValue = false)]
    [JsonPropertyName("journalId")]
    public string JournalId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "journalEntryDate", EmitDefaultValue = false)]
    [JsonPropertyName("journalEntryDate")]
    public DateTime? JournalEntryDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "journalEntryId", EmitDefaultValue = false)]
    [JsonPropertyName("journalEntryId")]
    public long? JournalEntryId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "lastReminderDate", EmitDefaultValue = false)]
    [JsonPropertyName("lastReminderDate")]
    public DateTime? LastReminderDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "ledgerNumber", EmitDefaultValue = false)]
    [JsonPropertyName("ledgerNumber")]
    public int? LedgerNumber { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "numberOfRemindersSent", EmitDefaultValue = false)]
    [JsonPropertyName("numberOfRemindersSent")]
    public int? NumberOfRemindersSent { get; set; }

    /// <summary>
    ///     ___<BR /> aggregated property from associated order
    /// </summary>
    /// <value>___<BR /> aggregated property from associated order</value>
    [DataMember(Name = "ocrRef", EmitDefaultValue = false)]
    [JsonPropertyName("ocrRef")]
    public string OcrRef { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "orderNumber", EmitDefaultValue = false)]
    [JsonPropertyName("orderNumber")]
    public long? OrderNumber { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "paid", EmitDefaultValue = false)]
    [JsonPropertyName("paid")]
    public bool? Paid { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "paymentType", EmitDefaultValue = false)]
    [JsonPropertyName("paymentType")]
    public int? PaymentType { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "preliminary", EmitDefaultValue = false)]
    [JsonPropertyName("preliminary")]
    public bool? Preliminary { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "projectId", EmitDefaultValue = false)]
    [JsonPropertyName("projectId")]
    public string ProjectId { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "receivableAccount", EmitDefaultValue = false)]
    [JsonPropertyName("receivableAccount")]
    public string ReceivableAccount { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "registeredByUser", EmitDefaultValue = false)]
    [JsonPropertyName("registeredByUser")]
    public string RegisteredByUser { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "reminderFee1", EmitDefaultValue = false)]
    [JsonPropertyName("reminderFee1")]
    public decimal? ReminderFee1 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "reminderFee2", EmitDefaultValue = false)]
    [JsonPropertyName("reminderFee2")]
    public decimal? ReminderFee2 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "reminderFee3", EmitDefaultValue = false)]
    [JsonPropertyName("reminderFee3")]
    public decimal? ReminderFee3 { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "reminderOneDate", EmitDefaultValue = false)]
    [JsonPropertyName("reminderOneDate")]
    public DateTime? ReminderOneDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "reminderTwoDate", EmitDefaultValue = false)]
    [JsonPropertyName("reminderTwoDate")]
    public DateTime? ReminderTwoDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "reminderThreeDate", EmitDefaultValue = false)]
    [JsonPropertyName("reminderThreeDate")]
    public DateTime? ReminderThreeDate { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "rotRutAmount", EmitDefaultValue = false)]
    [JsonPropertyName("rotRutAmount")]
    public double? RotRutAmount { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "subLedgerEntries", EmitDefaultValue = false)]
    [JsonPropertyName("subLedgerEntries")]
    public List<SubLedgerEntry> SubLedgerEntries { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "type", EmitDefaultValue = false)]
    [JsonPropertyName("type")]
    public string Type { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "typeOrigin", EmitDefaultValue = false)]
    [JsonPropertyName("typeOrigin")]
    public string TypeOrigin { get; set; }

    /// <summary>
    ///     ___
    /// </summary>
    /// <value>___</value>
    [DataMember(Name = "updatedDate", EmitDefaultValue = false)]
    [JsonPropertyName("updatedDate")]
    public DateTime? UpdatedDate { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class NewCustomerInvoice {\n");
        sb.Append("  AmountInOriginalCurrency: ").Append(AmountInOriginalCurrency).Append("\n");
        sb.Append("  AmountInLocalCurrency: ").Append(AmountInLocalCurrency).Append("\n");
        sb.Append("  AmountPaidInOriginalCurrency: ").Append(AmountPaidInOriginalCurrency).Append("\n");
        sb.Append("  AmountPaidInLocalCurrency: ").Append(AmountPaidInLocalCurrency).Append("\n");
        sb.Append("  ConnectedInvoices: ").Append(ConnectedInvoices).Append("\n");
        sb.Append("  CostBearerId: ").Append(CostBearerId).Append("\n");
        sb.Append("  CostCenterId: ").Append(CostCenterId).Append("\n");
        sb.Append("  CreatedWith: ").Append(CreatedWith).Append("\n");
        sb.Append("  Currency: ").Append(Currency).Append("\n");
        sb.Append("  CustomerId: ").Append(CustomerId).Append("\n");
        sb.Append("  CustomerName: ").Append(CustomerName).Append("\n");
        sb.Append("  DateOfLatestPayment: ").Append(DateOfLatestPayment).Append("\n");
        sb.Append("  DocumentIds: ").Append(DocumentIds).Append("\n");
        sb.Append("  DueDate: ").Append(DueDate).Append("\n");
        sb.Append("  ExchangeRate: ").Append(ExchangeRate).Append("\n");
        sb.Append("  ExternalInvoiceId: ").Append(ExternalInvoiceId).Append("\n");
        sb.Append("  InvoiceNumber: ").Append(InvoiceNumber).Append("\n");
        sb.Append("  InvoiceDate: ").Append(InvoiceDate).Append("\n");
        sb.Append("  InvoiceDocuments: ").Append(InvoiceDocuments).Append("\n");
        sb.Append("  JournalId: ").Append(JournalId).Append("\n");
        sb.Append("  JournalEntryDate: ").Append(JournalEntryDate).Append("\n");
        sb.Append("  JournalEntryId: ").Append(JournalEntryId).Append("\n");
        sb.Append("  LastReminderDate: ").Append(LastReminderDate).Append("\n");
        sb.Append("  LedgerNumber: ").Append(LedgerNumber).Append("\n");
        sb.Append("  NumberOfRemindersSent: ").Append(NumberOfRemindersSent).Append("\n");
        sb.Append("  OcrRef: ").Append(OcrRef).Append("\n");
        sb.Append("  OrderNumber: ").Append(OrderNumber).Append("\n");
        sb.Append("  Paid: ").Append(Paid).Append("\n");
        sb.Append("  PaymentType: ").Append(PaymentType).Append("\n");
        sb.Append("  Preliminary: ").Append(Preliminary).Append("\n");
        sb.Append("  ProjectId: ").Append(ProjectId).Append("\n");
        sb.Append("  ReceivableAccount: ").Append(ReceivableAccount).Append("\n");
        sb.Append("  RegisteredByUser: ").Append(RegisteredByUser).Append("\n");
        sb.Append("  ReminderFee1: ").Append(ReminderFee1).Append("\n");
        sb.Append("  ReminderFee2: ").Append(ReminderFee2).Append("\n");
        sb.Append("  ReminderFee3: ").Append(ReminderFee3).Append("\n");
        sb.Append("  ReminderOneDate: ").Append(ReminderOneDate).Append("\n");
        sb.Append("  ReminderTwoDate: ").Append(ReminderTwoDate).Append("\n");
        sb.Append("  ReminderThreeDate: ").Append(ReminderThreeDate).Append("\n");
        sb.Append("  RotRutAmount: ").Append(RotRutAmount).Append("\n");
        sb.Append("  SubLedgerEntries: ").Append(SubLedgerEntries).Append("\n");
        sb.Append("  Type: ").Append(Type).Append("\n");
        sb.Append("  TypeOrigin: ").Append(TypeOrigin).Append("\n");
        sb.Append("  UpdatedDate: ").Append(UpdatedDate).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}