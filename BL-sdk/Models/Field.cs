using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Bl_sdk.Models;

/// <summary>
/// </summary>
[DataContract]
public sealed class Field
{
    /// <summary>
    ///     Gets or Sets Code
    /// </summary>
    [DataMember(Name = "code", EmitDefaultValue = false)]
    [JsonPropertyName("code")]
    public string Code { get; set; }

    /// <summary>
    ///     Gets or Sets Label
    /// </summary>
    [DataMember(Name = "label", EmitDefaultValue = false)]
    [JsonPropertyName("label")]
    public string Label { get; set; }

    /// <summary>
    ///     Gets or Sets Value
    /// </summary>
    [DataMember(Name = "value", EmitDefaultValue = false)]
    [JsonPropertyName("value")]
    public object Value { get; set; }

    /// <summary>
    ///     Gets or Sets DeductValue
    /// </summary>
    [DataMember(Name = "deductValue", EmitDefaultValue = false)]
    [JsonPropertyName("deductValue")]
    public decimal? DeductValue { get; set; }


    /// <summary>
    ///     Get the string presentation of the object
    /// </summary>
    /// <returns>String presentation of the object</returns>
    public override string ToString()
    {
        var sb = new StringBuilder();
        sb.Append("class Field {\n");
        sb.Append("  Code: ").Append(Code).Append("\n");
        sb.Append("  Label: ").Append(Label).Append("\n");
        sb.Append("  Value: ").Append(Value).Append("\n");
        sb.Append("  DeductValue: ").Append(DeductValue).Append("\n");
        sb.Append("}\n");
        return sb.ToString();
    }

    /// <summary>
    ///     Get the JSON string presentation of the object
    /// </summary>
    /// <returns>JSON string presentation of the object</returns>
    public string ToJson()
    {
        return JsonSerializer.Serialize(this);
    }
}