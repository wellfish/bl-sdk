using Bl_sdk.Client;
using Bl_sdk.Connector;
using RestSharp;

namespace Bl_sdk.Service;

public static class BlDocumentService
{
    public static async Task<byte[]> GetDocumentById(BlConnector connector, int documentId)
    {
        var response = await BlDocumentApiClient.ExecuteRequest(Method.Get, $"document/{documentId}", connector);

        return response;
    }
    public static async Task<byte[]> GetDocumentAsPdfById(BlConnector connector, long documentId)
    {
        var response = await BlDocumentApiClient.ExecuteRequest(Method.Get, $"document/asPdf/{documentId}", connector);

        return response;
    }
}