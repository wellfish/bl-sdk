using Bl_sdk.Client;
using Bl_sdk.Models.Response;

namespace Bl_sdk.Service;

public static class BlAuthService
{
    public static async Task<AuthResponse> GetAuth(string clientId, string clientSecret)
    {
        return await GetAuth(new Guid(clientId), new Guid(clientSecret));
    }
    public static async Task<AuthResponse> GetAuth(Guid clientId, Guid clientSecret)
    {
        var authResponse = await BlApiAuthClient.GetToken(clientId, clientSecret);
        return authResponse;
    }
}