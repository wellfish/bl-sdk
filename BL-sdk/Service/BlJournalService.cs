using Bl_sdk.Client;
using Bl_sdk.Connector;
using Bl_sdk.Models;
using RestSharp;

namespace Bl_sdk.Service;

public static class BlJournalService
{
    public static async Task<ICollection<JournalEntry>> GetJournalEntries(BlConnector connector, DateTime start, DateTime end)
    {
        var response = await BlApiClient.ExecuteRequest<List<JournalEntry>>(Method.Get, $"journal/entry/{start:yyyy-MM-dd}/{end:yyyy-MM-dd}", connector);

        return response;
    }
    public static async Task<JournalEntry> GetJournalEntryById(BlConnector connector, int id)
    {
        var response = await BlApiClient.ExecuteRequest<JournalEntry>(Method.Get, $"journal/entry?entityId={id}", connector);

        return response;
    }
}