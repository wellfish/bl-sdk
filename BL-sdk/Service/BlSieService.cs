using Bl_sdk.Client;
using Bl_sdk.Connector;
using jsiSIE;
using RestSharp;

namespace Bl_sdk.Service;

public static class BlSieService
{
    public static async Task<SieDocument> GetSieData(BlConnector connector, DateTime fromDate, DateTime toDate)
    {
        var resource = $"/sie/export/{fromDate:yyyy-MM-dd}/{toDate:yyyy-MM-dd}";

        var response = await BlSieClient.ExecuteRequest<string>(Method.Get, resource, connector).ConfigureAwait(false);
        if (string.IsNullOrEmpty(response))
        {
            throw new Exception("Could not get SIE data, response is empty");
        }
        var document = new SieDocument
        {
            IgnoreBTRANS = true,
            IgnoreRTRANS = true,
        };

        using var stream = new MemoryStream();
        await using var writer = new StreamWriter(stream);
        await writer.WriteAsync(response).ConfigureAwait(false);
        await writer.FlushAsync().ConfigureAwait(false); // Ensure all data is written to the underlying stream
        stream.Position = 0;
        document.ReadDocument(stream);

        return document;
    }
}