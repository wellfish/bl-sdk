using Bl_sdk.Client;
using Bl_sdk.Connector;
using Bl_sdk.Models;
using RestSharp;

namespace Bl_sdk.Service;

public static class BlAccountService
{
    public static async Task<ICollection<Account>> GetAccounts(BlConnector connector)
    {
        var response = await BlApiClient.ExecuteRequest<List<Account>>(Method.Get, "account", connector);
       
        return response;
    }
    public static async Task<ICollection<Account>> GetAccounts(BlConnector connector, DateTime startDate, DateTime endDate)
    {
        var response = await BlApiClient.ExecuteRequest<List<Account>>(Method.Get, $"account/{startDate:yyyy-MM-dd}/{endDate:yyyy-MM-dd}", connector);
       
        return response;
    }
}