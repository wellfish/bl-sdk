using Bl_sdk.Client;
using Bl_sdk.Connector;
using Bl_sdk.Models;
using RestSharp;

namespace Bl_sdk.Service;

public static class BlFinancialYearService
{
    public static async Task<ICollection<FinancialYear>> GetFinancialYears(BlConnector connector)
    {
        var response = await BlApiClient.ExecuteRequest<List<FinancialYear>>(Method.Get, "financialyear", connector);
       
        return response;
    }
}