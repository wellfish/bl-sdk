using Bl_sdk.Client;
using Bl_sdk.Connector;
using Bl_sdk.Models;
using RestSharp;

namespace Bl_sdk.Service;

public static class BlSupplierInvoiceService
{
    public static async Task<ICollection<SupplierInvoice>> GetSupplierInvoices(BlConnector connector)
    {
        var response = await BlApiClient.ExecuteRequest<List<SupplierInvoice>>(Method.Get, "supplierinvoice", connector);
       
        return response;
    }
    public static async Task<ICollection<SupplierInvoice>> GetSupplierInvoices(BlConnector connector, DateTime startDate, DateTime endDate)
    {
        var response = await BlApiClient.ExecuteRequest<List<SupplierInvoice>>(Method.Get, $"supplierinvoice/date/{startDate:yyyy-MM-dd}/{endDate:yyyy-MM-dd}", connector);
       
        return response;
    }
    public static async Task<SupplierInvoice> GetSupplierInvoiceById(BlConnector connector, int id)
    {
        var response = await BlApiClient.ExecuteRequest<SupplierInvoice>(Method.Get, $"supplierinvoice?entityId={id}", connector);
       
        return response;
    }
}