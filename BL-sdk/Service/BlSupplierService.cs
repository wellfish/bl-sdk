using Bl_sdk.Client;
using Bl_sdk.Connector;
using Bl_sdk.Models;
using RestSharp;

namespace Bl_sdk.Service;

public static class BlSupplierService
{
    public static async Task<ICollection<Supplier>> GetSuppliers(BlConnector connector)
    {
        var response = await BlApiClient.ExecuteRequest<List<Supplier>>(Method.Get, "supplier", connector);
       
        return response;
    }
    //public static async Task<FinancialYear> GetFinicalYear(string token, int id)
   // {
        //var client = new BlApiClient();
        //var response = await client.ExecuteRequest<FinancialYear>(Method.Get, $"financialyear/{id}", token);
       
        //return response;
    //}
}