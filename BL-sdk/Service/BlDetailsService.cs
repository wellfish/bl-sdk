using Bl_sdk.Client;
using Bl_sdk.Connector;
using Bl_sdk.Models;
using RestSharp;

namespace Bl_sdk.Service;

/// <summary>
/// Get and update the company information
/// </summary>
public static class BlDetailsService
{
    public static async Task<Details> GetDetails(BlConnector connector)
    {
        var response = await BlApiClient.ExecuteRequest<Details>(Method.Get, "details", connector);
       
        return response;
    }
    // public static async Task<Details> UpdateDetails(BlConnector connector, Details details)
    // {
    //     var client = new BlApiClient();
    //     var response = await client.ExecuteRequest<Details>(Method.Get, "details", connector);
    //    
    //     return response;
    // }
}