using System.Net;
using Bl_sdk.Connector;
using RestSharp;

namespace Bl_sdk.Client;

public static class BlSieClient
{
    public static async Task<string> ExecuteRequest<T>(Method method, string path, BlConnector connector)
    {
        RestClient restClient = new("https://apigateway.blinfo.se/bla-api/v1/sp/");
        var request = new RestRequest(path, method);
        request.AddHeader("Authorization", $"Bearer {connector.AccessToken}");
        request.AddHeader("User-Key", connector.UserKey);
        request.AddHeader("Content-Type", "application/json");
        var response = await restClient.ExecuteAsync<T>(request);

        if (response.StatusCode != HttpStatusCode.OK || response.Content == null)
        {
            throw new Exception($"Error: {response.StatusCode} - {response.Content}", response.ErrorException);
        }
        return response.Content;
    }
}