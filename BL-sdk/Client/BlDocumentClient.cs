using Bl_sdk.Connector;
using RestSharp;

namespace Bl_sdk.Client;

public static class BlDocumentApiClient
{
    public static async Task<byte[]> ExecuteRequest(Method method, string path, BlConnector connector)
    {
        RestClient restClient = new("https://apigateway.blinfo.se/bla-api/v1/sp/");
        var request = new RestRequest(path, method);
        request.AddHeader("Authorization", $"Bearer {connector.AccessToken}");
        request.AddHeader("User-Key", connector.UserKey);
        // request.AddHeader("Content-Type", "application/json");

        // Execute the request and get the response object
        var response = await restClient.ExecuteAsync(request).ConfigureAwait(false);

        // Check if the response is successful
        if (response.IsSuccessful)
        {
            return response.RawBytes ?? throw new Exception("Response is empty");
        }

        throw response.ErrorException ?? new HttpRequestException(response.ErrorMessage);
    }
}