using Bl_sdk.Connector;
using RestSharp;

namespace Bl_sdk.Client;

public static class BlApiClient
{
    public static async Task<T> ExecuteRequest<T>(Method method, string path, BlConnector connector)
    {
        RestClient restClient = new("https://apigateway.blinfo.se/bla-api/v1/sp/");
        var request = new RestRequest(path, method);
        request.AddHeader("Authorization", $"Bearer {connector.AccessToken}");
        request.AddHeader("User-Key", connector.UserKey);
        request.AddHeader("Content-Type", "application/json");

        var response = await restClient.ExecuteAsync<T>(request).ConfigureAwait(false);

        if (response.ResponseStatus != ResponseStatus.Completed)
        {
            throw new HttpRequestException(response.Data?.ToString(), response.ErrorException);
        }

        if (!response.IsSuccessful || response.Data == null)
        {
            var error = response.Data?.ToString();
            throw new HttpRequestException(error);
        }

        return response.Data;
    }
}