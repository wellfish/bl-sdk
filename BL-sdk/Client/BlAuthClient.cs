using Bl_sdk.Models.Response;
using RestSharp;

namespace Bl_sdk.Client;

public static class BlApiAuthClient
{
    public static async Task<AuthResponse> GetToken(Guid clientId, Guid clientSecret)
    {
        RestClient restClient = new("https://apigateway.blinfo.se");
        const string resource = "auth/oauth/v2/token";

        var request = new RestRequest(resource, Method.Post);

        request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
        request.AddParameter("grant_type", "client_credentials");
        request.AddParameter("scope", "");
        request.AddParameter("client_id", clientId);
        request.AddParameter("client_secret", clientSecret);

        var response = await restClient.ExecuteAsync<AuthResponse>(request).ConfigureAwait(false);

        if (!response.IsSuccessful || response.Data == null)
        {
            throw new Exception(response.ErrorMessage, response.ErrorException);
        }

        return response.Data;
    }
}